//
//  FilePreviewNoActionbarViewController.swift
//  Runner
//
//  Created by FancyLou on 2024/1/18.
//

import UIKit

class FilePreviewNoActionbarViewController: UIViewController {
    
    var doneTitle: String = "关闭"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let doneBtn = UIButton(type: .system)
        doneBtn.frame = CGRect(x: UIScreen.main.bounds.width - 40, y: 20, width: 40, height: 40)
        doneBtn.setTitle( self.doneTitle, for: .normal)
        doneBtn.addTarget(self, action: #selector(closePreviewOutVc), for: .touchUpInside)
        self.view.addSubview(doneBtn);
    }
    
 
    @objc private func closePreviewOutVc() {
        self.dismiss(animated: true, completion: nil)
        print("closePreviewOutVc ===== dismiss")
    }
}
