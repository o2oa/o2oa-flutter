//
//  BaiduMapForFlutter.swift
//  Runner
//
//  Created by FancyLou on 2022/8/30.
//

import UIKit
import Foundation
import Flutter



class BdiuMapForFlutterViewFactory : NSObject, FlutterPlatformViewFactory {
    
    public func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return BdiuMapForFlutterView(frame, viewId:viewId, args:args)
    }
}


class BdiuMapForFlutterView : NSObject, FlutterPlatformView {
    
    let frame : CGRect
    let viewId : Int64
    
    init(_ frame:CGRect, viewId:Int64, args: Any?){
        print("BdiuMapForFlutterView init frame: \(frame) viewId: \(viewId)")
        self.frame = frame
        self.viewId = viewId
    }
    
    func view() -> UIView {
        print("BdiuMapForFlutterView view" )
//        let mapView = BMKMapView(frame: self.frame)
//        mapView.zoomLevel = 17
//        mapView.showMapPoi = true
//        mapView.showIndoorMapPoi = true
//        return mapView
        let view : UIView = UIView(frame: self.frame)
        view.backgroundColor = UIColor.lightGray
        return view
    }
    
    
}
