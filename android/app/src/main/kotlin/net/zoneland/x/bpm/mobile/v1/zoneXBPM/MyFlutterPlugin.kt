package net.zoneland.x.bpm.mobile.v1.zoneXBPM

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.PluginRegistry
import net.zoneland.x.bpm.mobile.v1.zoneXBPM.view.TestBaiduMapViewFactory

/**
 * Created by fancyLou on 2022-08-25.
 * Copyright © 2022 android. All rights reserved.
 */
class MyFlutterPlugin: FlutterPlugin {



    companion object {
        const val pluginName = "plugins.o2oa.net/test_baidu_map_view"
        @JvmStatic
        fun registerWith(registrar: PluginRegistry.Registrar) {
            registrar
                .platformViewRegistry()
                .registerViewFactory(
                    pluginName,
                    TestBaiduMapViewFactory(registrar.messenger()))
        }
    }

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        val messenger: BinaryMessenger = binding.binaryMessenger
        binding
            .platformViewRegistry
            .registerViewFactory(
                pluginName, TestBaiduMapViewFactory(messenger)
            )

    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
    }
}