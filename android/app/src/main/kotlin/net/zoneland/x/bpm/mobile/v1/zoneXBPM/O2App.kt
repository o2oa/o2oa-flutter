package net.zoneland.x.bpm.mobile.v1.zoneXBPM

import android.app.Application
import android.content.Context
import android.util.Log

// [baidumap19]
// [baidumap20]
// [baidumap21]
// import com.baidu.mapapi.CoordType
// import com.baidu.mapapi.SDKInitializer
// import com.baidu.mapapi.common.BaiduMapSDKException




/**
 * Created by fancyLou on 2022-06-30.
 * Copyright © 2022 android. All rights reserved.
 */
class O2App : Application() {


    companion object {
        lateinit var instance: O2App
    }



    var mContext: Context? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        mContext = applicationContext
    }

    /**
     * 初始化百度 sdk
     */
    fun initBaiduSdk() {
        try {
            // [baidumap22]
            // [baidumap23]
            // [baidumap24]
            // SDKInitializer.setAgreePrivacy(this, true)
            // SDKInitializer.initialize(this)
            // SDKInitializer.setCoordType(CoordType.BD09LL)
        } catch (e: Exception) { // [baidumap25]
            Log.i("O2APP"," 百度地图初始化失败")
        }
    }


}