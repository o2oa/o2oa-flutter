import 'dart:math';

/// 百度 db09 坐标转化工具
class BaiduLocationTransformHelper {
  static const double pi = 3.1415926535897932384626;
  static const double ee = 0.00669342162296594323;
  static const double a = 6378245.0;

  static List<double> bd09towgs84(double bdLng, double bdLat) {
    var gcj02 = bd09togcj02(bdLng, bdLat);
    var result = gcj02towgs84(gcj02[0], gcj02[1]);
    return result;
  }

//百度坐标转火星坐标
  static List<double> bd09togcj02(double bdLng, double bdLat) {
    var xPi = 3.14159265358979324 * 3000.0 / 180.0;
    var x = bdLng - 0.0065;
    var y = bdLat - 0.006;
    var z = sqrt(x * x + y * y) - 0.00002 * sin(y * xPi);
    var theta = atan2(y, x) - 0.000003 * cos(x * xPi);
    var ggLng = z * cos(theta);
    var ggLat = z * sin(theta);
    return [ggLng, ggLat];
  }

//火星转GPS
  static List<double> gcj02towgs84(double lng, double lat) {
    if (_outOfChina(lng, lat)) {
      return [lng, lat];
    } else {
      var dlat = _transformlat(lng - 105.0, lat - 35.0);
      var dlng = _transformlng(lng - 105.0, lat - 35.0);
      var radlat = lat / 180.0 * pi;
      var magic = sin(radlat);
      magic = 1 - ee * magic * magic;
      var sqrtmagic = sqrt(magic);
      dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi);
      dlng = (dlng * 180.0) / (a / sqrtmagic * cos(radlat) * pi);
      var mglat = lat + dlat;
      var mglng = lng + dlng;
      return [lng * 2 - mglng, lat * 2 - mglat];
    }
  }

  static bool _outOfChina(double lng, double lat) {
    return (lng < 72.004 ||
        lng > 137.8347 ||
        ((lat < 0.8293 || lat > 55.8271) || false));
  }

  static double _transformlat(double lng, double lat) {
    var ret = -100.0 +
        2.0 * lng +
        3.0 * lat +
        0.2 * lat * lat +
        0.1 * lng * lat +
        0.2 * sqrt(lng.abs());
    ret +=
        (20.0 * sin(6.0 * lng * pi) + 20.0 * sin(2.0 * lng * pi)) * 2.0 / 3.0;
    ret += (20.0 * sin(lat * pi) + 40.0 * sin(lat / 3.0 * pi)) * 2.0 / 3.0;
    ret +=
        (160.0 * sin(lat / 12.0 * pi) + 320 * sin(lat * pi / 30.0)) * 2.0 / 3.0;
    return ret;
  }

  static double _transformlng(double lng, double lat) {
    var ret = 300.0 +
        lng +
        2.0 * lat +
        0.1 * lng * lng +
        0.1 * lng * lat +
        0.1 * sqrt(lng.abs());
    ret +=
        (20.0 * sin(6.0 * lng * pi) + 20.0 * sin(2.0 * lng * pi)) * 2.0 / 3.0;
    ret += (20.0 * sin(lng * pi) + 40.0 * sin(lng / 3.0 * pi)) * 2.0 / 3.0;
    ret += (150.0 * sin(lng / 12.0 * pi) + 300.0 * sin(lng / 30.0 * pi)) *
        2.0 /
        3.0;
    return ret;
  }
}
