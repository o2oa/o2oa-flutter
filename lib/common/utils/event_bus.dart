//订阅者回调签名
typedef EventCallback = void Function(dynamic arg);


class EventBus {
  // 刷新门户事件
  static const refreshPortalMsg = "refreshPortalMsg";
  // js api 刷新页面事件
  static const refreshWebviewByJsAPIMsg = "refreshWebviewByJsAPIMsg";
  // im消息
  static const websocketCreateImMsg = "websocketCreateImMsg";
  static const websocketRevokeImMsg = "websocketRevokeImMsg";
  static const websocketImConversationUpdateMsg = "websocketImConversationUpdateMsg";
  static const websocketImConversationDeleteMsg = "websocketImConversationDeleteMsg";
  // 刷新未读消息数量的消息
  static const imUnReadNumberMsg = "imUnReadNumberMsg";
  // 语音消息播放动画
  static const imSoundMessagePlayMsg  = 'imSoundMessagePlayMsg';
  static const imSoundMessageStopPlayMsg  = 'imSoundMessageStopPlayMsg';
  /// 选择器数据同步用
  static const contactPickerAddPersonMsg = "contactPickerAddPersonMsg";
  static const contactPickerAddIdentityMsg = "contactPickerAddIdentityMsg";
  static const contactPickerAddUnitMsg = "contactPickerAddUnitMsg";
  static const contactPickerAddGroupMsg = "contactPickerAddGroupMsg";
  static const contactPickerRemovePersonMsg = "contactPickerRemovePersonMsg";
  static const contactPickerRemoveIdentityMsg = "contactPickerRemoveIdentityMsg";
  static const contactPickerRemoveUnitMsg = "contactPickerRemoveUnitMsg";
  static const contactPickerRemoveGroupMsg = "contactPickerRemoveGroupMsg";
  static const contactPickerChangeAllPersonMsg = "contactPickerChangeAllPersonMsg";
  static const contactPickerChangeAllIdentityMsg = "contactPickerChangeAllIdentityMsg";
  static const contactPickerChangeAllUnitMsg = "contactPickerChangeAllUnitMsg";

  static const personPickerRemovePersonMsg = "personPickerRemovePersonMsg";
  static const identityPickerRemoveIdentityMsg = "identityPickerRemoveIdentityMsg";
  static const unitPickerRemoveUnitMsg = "unitPickerRemoveUnitMsg";
  static const groupPickerRemoveGroupMsg = "groupPickerRemoveGroupMsg";

  /// 刷新头像
  static const avatarUpdateMsg = "avatarUpdateMsg";
  /// 签名修改刷新
  static const  mySignatureUpdateMsg = "mySignatureUpdateMsg";

  /// work 工作文档关闭通知
  static const processWorkCloseMsg = "processWorkCloseMsg";
  /// cms文档关闭通知 刷新列表使用
  static const cmsDocumentCloseMsg = "cmsDocumentCloseMsg";
  /// cms 分类文档列表刷新通知
  static const cmsCategoryDocListRefreshMsg = "cmsCategoryDocListRefreshMsg";
  /// portal 门户页面关闭通知
  static const portalCloseMsg = "portalCloseMsg";

  /// 网盘 上传文件成功刷新通知
  static const clouddiskFileUploadedMsg = "clouddiskFileUploadedMsg";

  /// 刷新黑白
  static const greyColorChangeGlobalMsg = 'greyColorChangeGlobalMsg';
  /// 全局水印开关
  static const globalWaterMarkEnableMsg = 'globalWaterMarkEnableMsg';

  ///  我的应用编辑后通知
  static const myAppChangeMsg = 'myAppChangeMsg';

  //私有构造函数
  EventBus._internal();

  //保存单例
  static final EventBus _singleton = EventBus._internal();

  //工厂构造函数
  factory EventBus()=> _singleton;

  //保存事件订阅者队列，key:事件名(id)，value: 对应事件的订阅者
  // final _emap =  <String, EventCallback?>{};
  final _emap =  <String, Map<String, EventCallback?>?>{};

  //添加订阅者
  void on(eventName, id, EventCallback? f) {
    if (eventName == null || id == null || f == null) return;
    final subscribers =  _emap[eventName];
    if (subscribers == null) {
      _emap[eventName]= <String, EventCallback?>{id: f};
    } else {
      subscribers[id] = f;
    }
    
  }

  //移除订阅者
  void off(eventName, id) {
    // _emap[eventName] = null;
    final subscribers =  _emap[eventName];
    if (subscribers != null) {
      subscribers[id] = null;
    }
  }

  //触发事件，事件触发后该事件所有订阅者会被调用
  void emit(eventName, [arg]) {
    var subscribers = _emap[eventName];
    if (subscribers != null) {
      for(var f in subscribers.values) {
        if (f != null) {
          f(arg);
        }
      }
    }
  }
}