import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get_utils/get_utils.dart';

import '../../pages/common/preview_image/index.dart';
import '../../pages/common/video_player/index.dart';
import '../models/enums/o2_skin.dart';
import '../services/shared_preference_service.dart';
import 'index.dart';

/// webview  工具类
///
/// 主要处理 jspai 注入方面的工作
class WebviewHelper {
  final channel = O2FlutterMethodChannelUtils();
  // js api 插件
  final jsHandler = O2JavascriptHandler();
  // 打印控制器
  PrintJobController? printJobController;

  /// 加载接口实现类
  ///
  /// 处理窗口相关事件的
  void setupJsNavigationInterface(JsNavigationInterface inf) {
    jsHandler.jsNavigationInterface = inf;
  }

  ///
  /// 加载js通道
  /// 在 webview onWebViewCreated 的时候加载
  ///
  void setupWebviewJsHandler(InAppWebViewController webviewController) async {
    // 添加 webviewController
    jsHandler.webviewController = webviewController;
    // 添加 js handler
    webviewController.addJavaScriptHandler(
        handlerName: jsHandler.o2mNotification().key,
        callback: jsHandler.o2mNotification().callback);
    webviewController.addJavaScriptHandler(
        handlerName: jsHandler.o2mBiz().key,
        callback: jsHandler.o2mBiz().callback);
    webviewController.addJavaScriptHandler(
        handlerName: jsHandler.o2mUtil().key,
        callback: jsHandler.o2mUtil().callback);
    const js = '''
        window.o2f = {};
        window.o2f.readys = []; // 延迟执行函数
        window.o2f.readyOptions = {};
        window.o2f._loaded = false;
        window.o2f.addReady = function (fn) {
            if (window.o2f._loaded) {
                if (fn) fn.call(window, window.o2f.readyOptions);
            } else {
                if (fn) window.o2f.readys.push(fn);
            }
        };
        window.o2f.onReady = function () {
            for (let i = 0; i < window.o2f.readys.length; i++) {
              window.o2f.readys[i].call(window, window.o2f.readyOptions);
            }
            window.o2f._loaded = true;
        };
      ''';
    webviewController.evaluateJavascript(source: js);
  }

  void dispose() {
     printJobController?.dispose();
     OLogger.d('Webview Helper dispose   ======= ');
  }

  void printJob(InAppWebViewController webviewController) async {
    printJobController?.dispose();
    final jobSettings = PrintJobSettings(
        handledByClient: true,
        jobName: await webviewController.getTitle() ?? '',
        // colorMode: PrintJobColorMode.MONOCHROME,
        // outputType: PrintJobOutputType.GRAYSCALE,
        // orientation: PrintJobOrientation.LANDSCAPE,
        // numberOfPages: 1
        );

    printJobController = await webviewController.printCurrentPage(settings: jobSettings);

    if (defaultTargetPlatform == TargetPlatform.iOS) {
      printJobController?.onComplete = (completed, error) async {
        if (completed) {
          OLogger.d("Print Job Completed");
        } else {
          OLogger.d("Print Job Failed $error");
        }
        printJobController?.dispose();
      };
    }

    final jobInfo = await printJobController?.getInfo();
    OLogger.d('${jobInfo?.toMap()}');
  }

  ///
  /// webview 加载进度
  /// progressChanged == 100的时候加载
  ///
  void changeJsHandlerFunName(InAppWebViewController c) {
    var js = '''
          if (window.flutter_inappwebview && window.flutter_inappwebview.callHandler) {
            window.o2mNotification = {};
            window.o2mNotification.postMessage = function(message){
              window.flutter_inappwebview.callHandler('o2mNotification', message);
            };
            window.o2mBiz = {};
            window.o2mBiz.postMessage = function(message){
              window.flutter_inappwebview.callHandler('o2mBiz', message);
            };
            window.o2mUtil = {};
            window.o2mUtil.postMessage = function(message){
              window.flutter_inappwebview.callHandler('o2mUtil', message);
            };
          }
        ''';
    c.evaluateJavascript(source: js);
    OLogger.d("执行o2m jsapi 转化完成。。。");
    // 执行 onReady
    final mode = SharedPreferenceService.to.getThemeMode();
    final skin = SharedPreferenceService.to.getThemeSkin();
    OLogger.d(
        '执行 window.o2f.onReady：theme mode ${mode.name} skin ${skin.name}');
    final readyJs = '''
      if (window.o2f) {
        window.o2f.readyOptions = {
            theme: {
                mode: '${mode.name}',
                color: '${skin.getColor().value.toRadixString(16)}',
            }
        };
        window.o2f.onReady();
      }
    ''';
    c.evaluateJavascript(source: readyJs);
  }

  ///
  /// webview 下载文件 支持
  ///
  void onFileDownloadStart(DownloadStartRequest request) async {
    String downloadurl = request.url.toString();
    OLogger.d(
        "开始下载文件，${request.suggestedFilename} , ${request.contentDisposition} , ${request.contentLength} , ${request.mimeType}  ");
    OLogger.d("开始下载文件，url : $downloadurl");
    String fileName = '';
    var contentDisposition = request.contentDisposition;
    if (contentDisposition != null && contentDisposition.isNotEmpty) {
        // RegExp filenameRegExp = RegExp(r'filename="([^"]+)"');
        RegExp filenameRegExp = RegExp(r"filename\*=UTF-8''([^;]+)");
        Match? filenameMatch = filenameRegExp.firstMatch(contentDisposition);
        if (filenameMatch != null) {
          fileName = filenameMatch.group(1) ?? '';
          fileName = Uri.decodeComponent(fileName);
        }
    }
    if (fileName.isEmpty) {
      fileName = request.suggestedFilename ?? '';
    }
    OLogger.d("下载文件名： $fileName");
    if (fileName.isEmpty) {
      OLogger.e("文件名称为空，无法下载");
      return;
    }
    String md5 = Md5Util.encode(downloadurl);
    String? filePath =
        await O2FilePathUtil.getProcessFileDownloadLocalPath(md5, fileName);
    if (filePath == null || filePath.isEmpty) {
      OLogger.e("文件本地存储路径为空，无法下载");
      return;
    }
    try {
      var file = File(filePath);
      if (!file.existsSync()) {
        Loading.show();
        await O2HttpClient.instance.downloadFile(downloadurl, filePath);
        Loading.dismiss();
      }
      _openAttachmentFile(filePath, fileName);
    } on Exception catch (e) {
      Loading.showError(e.toString());
    }
  }

  ///
  /// 打开附件
  ///
  void _openAttachmentFile(String filePath, String fileName) {
    if (filePath.isImageFileName) {
      PreviewImagePage.open(filePath, fileName);
    } else if (filePath.isVideoFileName) {
      VideoPlayerPage.openLocalVideo(filePath, title: fileName);
    } else {
      channel.openLocalFile(filePath);
    }
  }
}
