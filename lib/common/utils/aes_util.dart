import 'package:encrypt/encrypt.dart' as en;

/// AES加密工具类
class AESUtil {
  /// 单例模式
  static final AESUtil _instance = AESUtil._internal();
  AESUtil._internal();
  factory AESUtil() => _instance;

  final _key = en.Key.fromUtf8('Og6k#Fv@H9HSELVVgQzFuQVWmQ8^FfMQ');
  final _iv = en.IV.fromLength(16);

  /// AES加密
  String encrypt(String data) {
    final encrypter = en.Encrypter(en.AES(_key));
    final encrypted = encrypter.encrypt(data, iv: _iv);
    return encrypted.base64;
  }

  /// AES解密
  String decrypt(String data) {
    final encrypter = en.Encrypter(en.AES(_key));
    final decrypted = encrypter.decrypt64(data, iv: _iv);
    return decrypted;
  }
}
