

import 'dart:convert';
import 'dart:typed_data';

import 'package:pointycastle/pointycastle.dart';

class Md5Util {

  static String encode(String content) {
    final md5 = Digest("MD5");
    Uint8List output = md5.process(utf8.encode(content) as Uint8List);
    var base64EncodedText = base64UrlEncode(output);
    return base64EncodedText;
  }
}

// void main(List<String> args) {
//   String md = Md5Util.encode("http://www.o2oa.net");
//   debugPrint("md: $md");
//   //md: SGRUBVpjZpE8Un7Ot3WyMw==
// }