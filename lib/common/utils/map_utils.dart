import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets/index.dart';
import 'loading.dart';
import 'log_util.dart';
 

class MapUtils {
  static List<String> buttons = [];
  static Future<void> showMapListMenu(String longitude, String latitude) async {
    final context = Get.context;
    if (context == null) {
      OLogger.e('没有 context！');
      return;
    }
    if (Platform.isAndroid) {
      buttons = [
        'bmap_open_menu_amap'.tr,
        'bmap_open_menu_bmap'.tr,
        'bmap_open_menu_tmap'.tr
      ];
    } else {
      buttons = [
        'bmap_open_menu_amap'.tr,
        'bmap_open_menu_bmap'.tr,
        'bmap_open_menu_tmap'.tr,
        'bmap_open_menu_iosmap'.tr
      ];
    }
    O2UI.showBottomSheetWithCancel(
        context,
        buttons.map((e) {
          return ListTile(
            onTap: () {
              Navigator.pop(context);
              openMap(e, longitude, latitude);
            },
            title: Align(
              alignment: Alignment.center,
              child: Text(e, style: Theme.of(context).textTheme.bodyMedium),
            ),
          );
        }).toList());
  }

  static void openMap(String name, String longitude, String latitude) {
    if (name == 'bmap_open_menu_amap'.tr) {
      _gotoAMap(longitude, latitude);
    } else if (name == 'bmap_open_menu_bmap'.tr) {
      _gotoBaiduMap(longitude, latitude);
    } else if (name == 'bmap_open_menu_tmap'.tr) {
      _gotoTencentMap(longitude, latitude);
    } else if (name == 'bmap_open_menu_iosmap'.tr) {
      _gotoAppleMap(longitude, latitude);
    }
  }

  /// 高德地图
  static Future<bool> _gotoAMap(String longitude, String latitude) async {
    final url =
        '${Platform.isAndroid ? 'android' : 'ios'}amap://navi?sourceApplication=amap&lat=$latitude&lon=$longitude&dev=0&style=2';
    final uri = Uri.parse(url);
    bool can = await canLaunchUrl(uri);
    if (!can) {
      Loading.showError('bmap_open_amap_nofound'.tr);
      return false;
    }
    await launchUrl(uri, mode: LaunchMode.externalApplication);

    return true;
  }

  /// 腾讯地图
  static Future<bool> _gotoTencentMap(longitude, latitude) async {
    final url =
        'qqmap://map/routeplan?type=drive&fromcoord=CurrentLocation&tocoord=$latitude,$longitude&referer=IXHBZ-QIZE4-ZQ6UP-DJYEO-HC2K2-EZBXJ';
    final uri = Uri.parse(url);
    bool can = await canLaunchUrl(uri);
    if (!can) {
      Loading.showError('bmap_open_tmap_nofound'.tr);
      return false;
    }
    await launchUrl(uri, mode: LaunchMode.externalApplication);
    return true;
  }

  /// 百度地图
  static Future<bool> _gotoBaiduMap(longitude, latitude) async {
    var url =
        'baidumap://map/direction?destination=$latitude,$longitude&coord_type=bd09ll&mode=driving';

    final uri = Uri.parse(url);
    bool can = await canLaunchUrl(uri);
    if (!can) {
      Loading.showError('bmap_open_bmap_nofound'.tr);
      return false;
    }
    await launchUrl(uri, mode: LaunchMode.externalApplication);
    return true;
  }

  /// 苹果地图
  static Future<bool> _gotoAppleMap(longitude, latitude) async {
    var url = 'http://maps.apple.com/?&daddr=$latitude,$longitude';
    final uri = Uri.parse(url);
    bool can = await canLaunchUrl(uri);
    if (!can) {
      Loading.showError('bmap_open_iosmap_nofound'.tr);
      return false;
    }
    await launchUrl(uri, mode: LaunchMode.externalApplication);
    return true;
  }
}
