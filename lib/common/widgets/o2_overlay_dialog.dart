import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../style/index.dart';
import 'o2_elevated_button.dart';

class O2OverlayEntryDialog {
  factory O2OverlayEntryDialog() => _instance;
  static O2OverlayEntryDialog get instance => _instance;

  static final O2OverlayEntryDialog _instance =
      O2OverlayEntryDialog._internal();
  O2OverlayEntryDialog._internal();

  OverlayEntry? _overlayEntry;
  VoidCallback? _callback;
  bool isOpening = false; // 是否有弹窗显示中

  /// 考勤 极速打卡 弹出提示
  void openFastCheckInDialog(String title, String content) {
    final context = Get.context;
    if (context == null) {
      return;
    }
    if (isOpening) {
      return;
    }
    isOpening = true;
    _openDialog(context, _buildAlertDialog(context, title, content, showCheckIcon: true, showFooter: true));
  }

  /// 提示弹窗
  void openAlertDialog(String title, String content, {VoidCallback? callback}) {
    final context = Get.context;
    if (context == null) {
      return;
    }
    if (isOpening) {
      return;
    }
    isOpening = true;
    bool showFooter = false;
    if (callback != null) {
      _callback = callback;
      showFooter = true;
    }
    _openDialog(context, _buildAlertDialog(context, title, content, showFooter: showFooter));
  }

  void _openDialog(BuildContext context, Widget dialog) {
    _overlayEntry = OverlayEntry(builder: ((context) => dialog));
    //插入到 Overlay中显示 OverlayEntry
    Overlay.of(context).insert(_overlayEntry!);
  }

  void closeDiloag() {
    if (_overlayEntry != null) {
      _overlayEntry!.remove();
      _overlayEntry = null;
    }
    _callback = null;
    isOpening = false;
  }


  Widget _buildAlertDialog(
      BuildContext context, String title, String content, {bool showCheckIcon = false, bool showFooter = false}) {
    return _dialogBox(
      Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Padding(
            padding:
                const EdgeInsets.only(left: 14, right: 14, bottom: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    if (showCheckIcon)const Icon(O2IconFont.ok ,size: 24, color: Colors.green),
                    const Spacer(),
                    IconButton(
                        onPressed: () => closeDiloag(),
                        icon: const Icon(Icons.close),
                        padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0))
                  ],
                ),
                Text(title,
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge
                        ?.copyWith(fontWeight: FontWeight.bold)),
                const SizedBox(height: 10),
                Expanded(
                    flex: 1,
                    child: Padding(
                        padding:
                            const EdgeInsets.only(left: 10, right: 10),
                        child: Text(content,
                            style: Theme.of(context).textTheme.bodyMedium))),
                const SizedBox(height: 10),
                if(showFooter) SizedBox(
                    width: double.infinity,
                    height: 36,
                    child: O2ElevatedButton(() {
                      if (_callback != null) {
                        _callback!();
                      }
                      closeDiloag();
                    },
                        Text(
                          'positive'.tr,
                          style: const TextStyle(fontSize: 18),
                        ))),
              ],
            )),
      ),
    );
  }

  

  Widget _dialogBox(Widget child) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.black.withOpacity(0.6),
        ),
        Positioned(
            top: 44, left: 10, right: 10, height: 200, child: child)
      ],
    );
  }
}
