import 'package:flutter/widgets.dart';

import 'water_mark_painter.dart';

class WaterMarkWidget extends StatelessWidget {
  const WaterMarkWidget({
    super.key,
    required this.child,
    this.content = '水印',
    this.isShow = true,
  });

  final Widget child;
  final bool isShow;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,  // 设置文本方向为从左到右
      child: Stack(
        children: [
        child, 
        if (isShow) waterMark()
    ]));
  }

  Widget waterMark() {
    return IgnorePointer(
        child: CustomPaint(
            size: Size.infinite, painter: WatermarkPainter(content: content)));
  }
}
