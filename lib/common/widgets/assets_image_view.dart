import 'package:flutter/material.dart';

class AssetsImageView extends StatelessWidget {
  
  const AssetsImageView(this.image, {
    Key? key,
    this.width,
    this.height, 
    this.cacheWidth,
    this.cacheHeight,
    this.fit,
    this.color
  }): super(key: key);

  final String image;
  final double? width;
  final double? height;
  final int? cacheWidth;
  final int? cacheHeight;
  final BoxFit? fit;
  final Color? color;
  
  @override
  Widget build(BuildContext context) {

    return Image.asset(
      'assets/images/$image',
      height: height,
      width: width,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
      fit: fit,
      color: color,
      /// 忽略图片语义
      excludeFromSemantics: true,
    );
  }
}