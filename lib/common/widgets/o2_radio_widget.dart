import 'package:flutter/material.dart';

class O2RadioView extends StatelessWidget {
  const O2RadioView(
      {super.key,
      required this.isSelected,
      this.padding = 10,
      this.radioSize = 22});
  final double padding;
  final bool isSelected;
  final double radioSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: isSelected
          ? Icon(Icons.radio_button_checked_outlined, size: radioSize)
          : Icon(Icons.radio_button_off_outlined, size: radioSize),
    );
  }
}
