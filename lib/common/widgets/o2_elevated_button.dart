import 'package:flutter/material.dart';

class O2ElevatedButton extends StatelessWidget {
  const O2ElevatedButton(
    this.onPressed,
    this.child, {
    Key? key,
    this.radius = 20,
    this.isPrimaryBackgroundColor = true,
    this.onLongPress,
    this.focusNode,
    this.autofocus = false,
    this.clipBehavior = Clip.none,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final Widget? child;
  final VoidCallback? onLongPress;
  final FocusNode? focusNode;
  final bool autofocus;
  final Clip clipBehavior;
  final double radius;
  final bool isPrimaryBackgroundColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        onLongPress: onLongPress,
        style: ButtonStyle(
          backgroundColor: buttonBackgroundColor(context),
          shape: WidgetStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20))), //圆角弧度
        ),
        focusNode: focusNode,
        autofocus: autofocus,
        clipBehavior: clipBehavior,
        child: child);
  }

  WidgetStateProperty<Color?> buttonBackgroundColor(BuildContext context) {
    if (!isPrimaryBackgroundColor) {
      return WidgetStateProperty.resolveWith((states) {
        if (states.contains(WidgetState.disabled)) {
          return null;
        } else {
          return Theme.of(context).colorScheme.surface;
        }
      });
    }
    return WidgetStateProperty.resolveWith((states) {
      if (states.contains(WidgetState.disabled)) {
        return null;
      } else {
        return Theme.of(context).colorScheme.primary;
      }
    });
  }
}
