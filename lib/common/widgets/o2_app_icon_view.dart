import 'dart:typed_data';

import 'package:flutter/material.dart';

import '../api/x_processplatform_assemble_surface.dart';

/// 应用 app 的 logo 图标控件
class O2AppIconWidget extends StatefulWidget {
  const O2AppIconWidget(this.appId, this.size, {super.key});

  final String appId;
  final double size;

  @override
  State<O2AppIconWidget> createState() => _O2AppIconWidgetState();
}

class _O2AppIconWidgetState extends State<O2AppIconWidget> {
  Uint8List? iconData;

  @override
  void initState() {
    loadAppIcon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return iconData == null
        ? Icon(Icons.image, size: widget.size)
        : Image.memory(
            iconData!,
            width: widget.size,
            height: widget.size,
            fit: BoxFit.cover,
          );
  }

  Future<void> loadAppIcon() async {
    final icon = await ProcessSurfaceService.to.applicationIcon(widget.appId);
    setState(() {
      iconData = icon;
    });
  }
}
