


import 'package:flutter/material.dart';

class O2IconFont {
    // iconName: setup 设置
    static const settings = IconData(
      0xe90a,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );

    // iconName: addressList 通讯录
    static const addressList = IconData(
      0xe909,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );
    // iconName: news 消息
    static const message = IconData(
      0xe900,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );
    // iconName: apps 应用
    static const apps = IconData(
      0xe90b,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );

    // iconName: search 搜索
    static const search = IconData(
      0xe923,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    ); 
    
    // iconName: alert 提示 
    static const alert = IconData(
      0xe912,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );

     // iconName: alert ok 提示 
    static const ok = IconData(
      0xe918,
      fontFamily: 'o2IconFont',
      matchTextDirection: true,
    );
}