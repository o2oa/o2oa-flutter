import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/enums/index.dart';
import 'color.dart';

class AppTheme {
  static const horizontalMargin = 16.0;
  static const radius = 10.0;

  static final defaultLightTheme = ThemeData(
    useMaterial3: false,
    scaffoldBackgroundColor: AppColor.scaffoldBackgroundColorLight,
      primaryColor: AppColor.o2SkinBlue,
      appBarTheme: const AppBarTheme().copyWith(centerTitle: true),
      colorScheme: const ColorScheme.light(
          primary: AppColor.o2SkinBlue, onPrimary: Colors.white, secondary: AppColor.o2SkinBlueSecondary, surface: AppColor.colorSchemeBackgroundLight));

  static final defaultDarkTheme = ThemeData(
    useMaterial3: false,
      appBarTheme: const AppBarTheme().copyWith(centerTitle: true),
      // bottomNavigationBarTheme: const BottomNavigationBarThemeData().copyWith(
      //   selectedLabelStyle:
      //       const TextStyle(fontSize: 14, color: AppColor.primaryColor),
      //   selectedItemColor: AppColor.primaryColor,
      // ),
      colorScheme: const ColorScheme.dark(
          primary: AppColor.o2SkinBlue, secondary: AppColor.o2SkinBlueSecondary));


  static TextStyle whitePrimaryTextStyle =
      const TextStyle(color: Colors.white, fontSize: 14);


  /// 根据 name  获取皮肤 enum
  static O2Skin getSkinByName(String name) {
    return O2Skin.values.firstWhereOrNull((element) => element.name == name) ?? O2Skin.blue;
  }

  /// 根据皮肤颜色 获取亮色主题对象
  static ThemeData getLightThemeBySkin(O2Skin skin) {
    final skinColor = skin.getColor();
    final secondary = skin.getSecondaryColor();
    return ThemeData(
      useMaterial3: false,
      scaffoldBackgroundColor: AppColor.scaffoldBackgroundColorLight,
      primaryColor: skinColor,
      appBarTheme: const AppBarTheme().copyWith(centerTitle: true),
      colorScheme: ColorScheme.light(
          primary: skinColor, onPrimary: Colors.white, secondary: secondary, surface: AppColor.colorSchemeBackgroundLight),
    );
  }

  /// 根据皮肤颜色 获取暗黑主题对象
  static ThemeData getDarkThemeBySkin(O2Skin skin) {
    final skinColor = skin.getColor();
    final secondary = skin.getSecondaryColor();
    return ThemeData(
      useMaterial3: false,
      appBarTheme: const AppBarTheme().copyWith(centerTitle: true),
      // bottomNavigationBarTheme: const BottomNavigationBarThemeData().copyWith(
      //   selectedLabelStyle:
      //       TextStyle(fontSize: 14, color: skinColor),
      //   selectedItemColor: skinColor,
      // ),
      colorScheme: ColorScheme.dark(
          primary: skinColor, onPrimary: Colors.white, secondary: secondary));
  }

  // static TextStyle? textBodyLarge(BuildContext context) {
  //   return  Theme.of(context).textTheme.bodyLarge?.copyWith(fontSize: 16.sp);
  // }

  // static TextStyle? textBodyMedium(BuildContext context) {
  //   return  Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 14.sp);
  // }
  
  // static TextStyle? textBodySmall(BuildContext context) {
  //   return  Theme.of(context).textTheme.bodySmall?.copyWith(fontSize: 12.sp);
  // }
  
  // static TextStyle? textTitleSmall(BuildContext context) {
  //   return Theme.of(context).textTheme.titleSmall?.copyWith(fontSize: 14.sp);
  // }

}
