import 'package:flutter/material.dart';

extension O2StringUtils on String {

  /// 颜色字符串 转  Color
  Color hexToColor() {
    String hex = replaceAll("#", "");
    if (hex.length == 6) {
      hex = "FF$hex";
    }
    int colorValue = int.parse(hex, radix: 16);
    return Color(colorValue);
  }

  ///
  /// 获取当前url中的域名
  /// 如： http://dd.o2oa.net/x_desktop/index.html => dd.o2oa.net
  ///
  String getHostFromUrl() {
    RegExp domainReg = RegExp(r"(?<=://)(www.)?(\w+(\.)?)+");
    return domainReg.stringMatch(this) ?? '';
  }

  ///
  /// 获取文件的扩展名
  ///
  String fileNameExtension() {
    var index = lastIndexOf(".");
    if (index >= 0) {
      return substring(index + 1, length).toLowerCase();
    }
    return "";
  }

  ///
  /// 获取文件的扩展名
  ///
  String fileName() {
    var index = lastIndexOf("/");
    if (index >= 0) {
      return substring(index + 1, length);
    }
    return "";
  }

  /// 字符串格式化 把单引号 回车换行符 替换成 unicode 编码
  String o2SimpleString() {
    final text = this;
    return text.replaceAllMapped(RegExp(r"['\r\n]"), (match) {
      int charCode = match.group(0)!.codeUnitAt(0);
      return '\\u${charCode.toRadixString(16).padLeft(4, '0')}';
    });
  }

  /// 截取名称
  ///
  /// 包含@的 取第一段
  String o2NameCut() {
    final text = this;
    if (text.contains('@')) {
      return text.split('@').first;
    }
    return text;
  }
}
