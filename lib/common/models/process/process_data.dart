
import 'application_data.dart';

class ProcessData {
    ProcessData({
        this.id,
        this.updateTableEnable,
        this.name,
        this.alias,
        this.description,
        this.creatorPerson,
        this.lastUpdatePerson,
        this.lastUpdateTime,
        this.application,
        this.controllerList,
        this.startableIdentityList,
        this.startableUnitList,
        this.startableGroupList,
        this.serialTexture,
        this.serialPhase,
        this.expireType,
        this.expireDay,
        this.expireHour,
        this.expireWorkTime,
        this.checkDraft,
        this.startableTerminal,
        this.projectionFully,
        this.routeNameAsOpinion,
        this.beforeArriveScript,
        this.beforeArriveScriptText,
        this.afterArriveScript,
        this.afterArriveScriptText,
        this.beforeExecuteScript,
        this.beforeExecuteScriptText,
        this.afterExecuteScript,
        this.afterExecuteScriptText,
        this.beforeInquireScript,
        this.beforeInquireScriptText,
        this.afterInquireScript,
        this.afterInquireScriptText,
        this.edition,
        this.editionName,
        this.editionEnable,
        this.editionNumber,
        this.defaultStartMode,
        this.properties,
        this.createTime,
        this.updateTime,
        this.withApp
    });


    ProcessApplicationData? withApp; // 所属应用，分组List显示的时候使用

    String? id;
    bool? updateTableEnable;
    String? name;
    String? alias;
    String? description;
    String? creatorPerson;
    String? lastUpdatePerson;
    String? lastUpdateTime;
    String? application;
    List<String>? controllerList;
    List<String>? startableIdentityList;
    List<String>? startableUnitList;
    List<String>? startableGroupList;
    String? serialTexture;
    String? serialPhase;
    String? expireType;
    int? expireDay;
    int? expireHour;
    bool? expireWorkTime;
    bool? checkDraft;
    String? startableTerminal; // 可启动流程终端类型,可选值 client,mobile,all,none
    bool? projectionFully;
    bool? routeNameAsOpinion;
    String? beforeArriveScript;
    String? beforeArriveScriptText;
    String? afterArriveScript;
    String? afterArriveScriptText;
    String? beforeExecuteScript;
    String? beforeExecuteScriptText;
    String? afterExecuteScript;
    String? afterExecuteScriptText;
    String? beforeInquireScript;
    String? beforeInquireScriptText;
    String? afterInquireScript;
    String? afterInquireScriptText;
    String? edition;
    String? editionName;
    bool? editionEnable;
    double? editionNumber;
    String? defaultStartMode;
    Map<String, dynamic>? properties;
    String? createTime;
    String? updateTime;

    factory ProcessData.fromJson(Map<String, dynamic> json) => ProcessData(
        id: json["id"],
        updateTableEnable: json["updateTableEnable"],
        name: json["name"],
        alias: json["alias"],
        description: json["description"],
        creatorPerson: json["creatorPerson"],
        lastUpdatePerson: json["lastUpdatePerson"],
        lastUpdateTime: json["lastUpdateTime"] ,
        application: json["application"],
        controllerList: json["controllerList"] == null ? null : List<String>.from(json["controllerList"].map((x) => x)),
        startableIdentityList: json["startableIdentityList"] == null ? null : List<String>.from(json["startableIdentityList"].map((x) => x)),
        startableUnitList: json["startableUnitList"] == null ? null : List<String>.from(json["startableUnitList"].map((x) => x)),
        startableGroupList: json["startableGroupList"] == null ? null : List<String>.from(json["startableGroupList"].map((x) => x)),
        serialTexture: json["serialTexture"] ,
        serialPhase: json["serialPhase"] ,
        expireType: json["expireType"] ,
        expireDay: json["expireDay"],
        expireHour: json["expireHour"],
        expireWorkTime: json["expireWorkTime"] ,
        checkDraft: json["checkDraft"] ,
        startableTerminal: json["startableTerminal"],
        projectionFully: json["projectionFully"],
        routeNameAsOpinion: json["routeNameAsOpinion"] ,
        beforeArriveScript: json["beforeArriveScript"] ,
        beforeArriveScriptText: json["beforeArriveScriptText"] ,
        afterArriveScript: json["afterArriveScript"] ,
        afterArriveScriptText: json["afterArriveScriptText"] ,
        beforeExecuteScript: json["beforeExecuteScript"] ,
        beforeExecuteScriptText: json["beforeExecuteScriptText"] ,
        afterExecuteScript: json["afterExecuteScript"] ,
        afterExecuteScriptText: json["afterExecuteScriptText"] ,
        beforeInquireScript: json["beforeInquireScript"],
        beforeInquireScriptText: json["beforeInquireScriptText"] ,
        afterInquireScript: json["afterInquireScript"] ,
        afterInquireScriptText: json["afterInquireScriptText"] ,
        edition: json["edition"],
        editionName: json["editionName"] ,
        editionEnable: json["editionEnable"],
        editionNumber: json["editionNumber"],
        defaultStartMode: json["defaultStartMode"] ,
        properties: json["properties"],
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
        withApp: json["withApp"] ,
    );

    Map<String, dynamic> toJson() => {
        "id": id ,
        "updateTableEnable": updateTableEnable ,
        "name": name ,
        "alias": alias,
        "description": description ,
        "creatorPerson": creatorPerson,
        "lastUpdatePerson": lastUpdatePerson ,
        "lastUpdateTime": lastUpdateTime ,
        "application": application,
        "controllerList": controllerList == null ? null : List<String>.from(controllerList!.map((x) => x)),
        "startableIdentityList": startableIdentityList == null ? null : List<String>.from(startableIdentityList!.map((x) => x)),
        "startableUnitList": startableUnitList == null ? null : List<String>.from(startableUnitList!.map((x) => x)),
        "startableGroupList": startableGroupList == null ? null : List<String>.from(startableGroupList!.map((x) => x)),
        "serialTexture": serialTexture == null ,
        "serialPhase": serialPhase == null ,
        "expireType": expireType == null ,
        "expireDay": expireDay == null,
        "expireHour": expireHour ,
        "expireWorkTime": expireWorkTime ,
        "checkDraft": checkDraft,
        "startableTerminal": startableTerminal ,
        "projectionFully": projectionFully ,
        "routeNameAsOpinion": routeNameAsOpinion,
        "beforeArriveScript": beforeArriveScript ,
        "beforeArriveScriptText": beforeArriveScriptText ,
        "afterArriveScript": afterArriveScript ,
        "afterArriveScriptText": afterArriveScriptText ,
        "beforeExecuteScript": beforeExecuteScript ,
        "beforeExecuteScriptText": beforeExecuteScriptText ,
        "afterExecuteScript": afterExecuteScript ,
        "afterExecuteScriptText": afterExecuteScriptText ,
        "beforeInquireScript": beforeInquireScript ,
        "beforeInquireScriptText": beforeInquireScriptText ,
        "afterInquireScript": afterInquireScript ,
        "afterInquireScriptText": afterInquireScriptText,
        "edition": edition ,
        "editionName": editionName ,
        "editionEnable": editionEnable ,
        "editionNumber": editionNumber ,
        "defaultStartMode": defaultStartMode ,
        "properties": properties ,
        "createTime": createTime ,
        "updateTime": updateTime ,
        "withApp": withApp ,
    };
}