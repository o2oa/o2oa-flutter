

class ApplicationIconData {
  ApplicationIconData({
    this.icon,
    this.iconHue
  });
  String? icon;
  String? iconHue;

   factory ApplicationIconData.fromJson(Map<String, dynamic> json) => ApplicationIconData(
      icon: json["icon"],
      iconHue: json["iconHue"],
   );
    Map<String, dynamic> toJson() => {
      "icon": icon,
      "iconHue": iconHue
    };
}