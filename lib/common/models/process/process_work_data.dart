import 'task_data.dart';

class ProcessWorkData {
  String? createTime;
  String? updateTime;
  String? id;
  String? fromActivity;
  String? fromActivityType;
  String? fromActivityName;
  String? fromActivityToken;
  String? fromTime;
  String? job;
  List<TaskData>? taskList;
  // private List<TaskCompleteData> taskCompletedList;

  ProcessWorkData(
      {this.createTime,
      this.updateTime,
      this.id,
      this.fromActivity,
      this.fromActivityName,
      this.fromActivityToken,
      this.fromActivityType,
      this.fromTime,
      this.job,
      this.taskList});

  factory ProcessWorkData.fromJson(Map<String, dynamic> json) =>
      ProcessWorkData(
        id: json["id"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        fromActivity: json["fromActivity"],
        fromActivityType: json["fromActivityType"],
        fromActivityName: json["fromActivityName"],
        fromActivityToken: json["fromActivityToken"],
        fromTime: json["fromTime"],
        job: json["job"],
        taskList: json["taskList"] == null
            ? null
            : List<TaskData>.from(
                json["taskList"].map((x) => TaskData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createTime": createTime,
        "updateTime": updateTime,
        "fromActivity": fromActivity,
        "fromActivityName": fromActivityName,
        "fromActivityToken": fromActivityToken,
        "fromActivityType": fromActivityType,
        "fromTime": fromTime,
        "job": job,
        "taskList": taskList == null
            ? null
            : List<dynamic>.from(taskList!.map((x) => x.toJson())),
      };
}
