// To parse this JSON data, do
//
//     final workOrWorkcompletedInfo = workOrWorkcompletedInfoFromJson(jsonString);


class WorkOrWorkcompletedInfo {
    WorkOrWorkcompletedInfo({
        this.work,
    });

    Work? work;

    factory WorkOrWorkcompletedInfo.fromJson(Map<String, dynamic> json) => WorkOrWorkcompletedInfo(
        work: json["work"] == null ? null : Work.fromJson(json["work"]),
    );

    Map<String, dynamic> toJson() => {
        "work": work?.toJson()
    };
}

class Work {
    Work({
        this.id,
        this.job,
        this.title,
        this.startTime,
        this.startTimeMonth,
        this.completedTime,
        this.completedTimeMonth,
        this.creatorPerson,
        this.creatorIdentity,
        this.creatorUnit,
        this.creatorUnitLevelName,
        this.application,
        this.applicationName,
        this.applicationAlias,
        this.process,
        this.processName,
        this.processAlias,
        this.serial,
        this.form,
        this.work,
        this.createTime,
        this.updateTime,
        this.activityAlias,
        this.activityArrivedTime,
        this.activityName,
        this.manualTaskIdentityText
    });

    String? id;
    String? job;
    String? title;
    String? startTime;
    String? startTimeMonth;
    String? completedTime;
    String? completedTimeMonth;
    String? creatorPerson;
    String? creatorIdentity;
    String? creatorUnit;
    String? creatorUnitLevelName;
    String? application;
    String? applicationName;
    String? applicationAlias;
    String? process;
    String? processName;
    String? processAlias;
    String? serial;
    String? form;
    String? work;
    String? createTime;
    String? updateTime;

    String? activityAlias;
    String? activityArrivedTime;
    String? activityName;
    String? manualTaskIdentityText; // 处理人


    factory Work.fromJson(Map<String, dynamic> json) => Work(
        id: json["id"],
        job: json["job"],
        title: json["title"],
        startTime: json["startTime"],
        startTimeMonth: json["startTimeMonth"],
        completedTime: json["completedTime"],
        completedTimeMonth: json["completedTimeMonth"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnit: json["creatorUnit"],
        creatorUnitLevelName: json["creatorUnitLevelName"],
        application: json["application"],
        applicationName: json["applicationName"],
        applicationAlias: json["applicationAlias"],
        process: json["process"],
        processName: json["processName"],
        processAlias: json["processAlias"],
        serial: json["serial"],
        form: json["form"],
        work: json["work"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        activityAlias: json["activityAlias"],
        activityArrivedTime: json["activityArrivedTime"],
        activityName: json["activityName"],
        manualTaskIdentityText: json["manualTaskIdentityText"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "job": job,
        "title": title,
        "startTime": startTime,
        "startTimeMonth": startTimeMonth,
        "completedTime": completedTime,
        "completedTimeMonth": completedTimeMonth,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnit": creatorUnit,
        "creatorUnitLevelName": creatorUnitLevelName,
        "application": application,
        "applicationName": applicationName,
        "applicationAlias": applicationAlias,
        "process": process,
        "processName": processName,
        "processAlias": processAlias,
        "serial": serial,
        "form": form,
        "work": work,
        "createTime": createTime,
        "updateTime": updateTime,
        "activityAlias": activityAlias,
        "activityArrivedTime": activityArrivedTime,
        "activityName": activityName,
        "manualTaskIdentityText": manualTaskIdentityText,
    };

    ///
    /// 是否是已结束的工作
    /// 
    bool isWorkCompleted()  {
      return (completedTime != null && completedTime?.isNotEmpty == true);
    }
}



class WorkCompleted {
    WorkCompleted({
        this.id,
        this.job,
        this.title,
        this.startTime,
        this.startTimeMonth,
        this.completedTime,
        this.completedTimeMonth,
        this.creatorPerson,
        this.creatorIdentity,
        this.creatorUnit,
        this.creatorUnitLevelName,
        this.application,
        this.applicationName,
        this.applicationAlias,
        this.process,
        this.processName,
        this.processAlias,
        this.serial,
        this.form,
        this.work,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? job;
    String? title;
    String? startTime;
    String? startTimeMonth;
    String? completedTime;
    String? completedTimeMonth;
    String? creatorPerson;
    String? creatorIdentity;
    String? creatorUnit;
    String? creatorUnitLevelName;
    String? application;
    String? applicationName;
    String? applicationAlias;
    String? process;
    String? processName;
    String? processAlias;
    String? serial;
    String? form;
    String? work;
    String? createTime;
    String? updateTime;

    factory WorkCompleted.fromJson(Map<String, dynamic> json) => WorkCompleted(
        id: json["id"],
        job: json["job"],
        title: json["title"],
        startTime: json["startTime"],
        startTimeMonth: json["startTimeMonth"],
        completedTime: json["completedTime"],
        completedTimeMonth: json["completedTimeMonth"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnit: json["creatorUnit"],
        creatorUnitLevelName: json["creatorUnitLevelName"],
        application: json["application"],
        applicationName: json["applicationName"],
        applicationAlias: json["applicationAlias"],
        process: json["process"],
        processName: json["processName"],
        processAlias: json["processAlias"],
        serial: json["serial"],
        form: json["form"],
        work: json["work"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "job": job,
        "title": title,
        "startTime": startTime,
        "startTimeMonth": startTimeMonth,
        "completedTime": completedTime,
        "completedTimeMonth": completedTimeMonth,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnit": creatorUnit,
        "creatorUnitLevelName": creatorUnitLevelName,
        "application": application,
        "applicationName": applicationName,
        "applicationAlias": applicationAlias,
        "process": process,
        "processName": processName,
        "processAlias": processAlias,
        "serial": serial,
        "form": form,
        "work": work,
        "createTime": createTime,
        "updateTime": updateTime,
    };
 
}