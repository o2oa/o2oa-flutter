class JPushDeviceData {
  String? deviceName;
  String? pushType;
  String? deviceType;

  JPushDeviceData({this.deviceName, this.pushType, this.deviceType});

  JPushDeviceData.fromJson(Map<String, dynamic> jsonMap) {
    deviceName = jsonMap['deviceName'];
    pushType = jsonMap['pushType'];
    deviceType = jsonMap['deviceType'];
  }
  Map<String, dynamic> toJson() => {
        "deviceName": deviceName,
        "pushType": pushType,
        "deviceType": deviceType,
      };
}
