
class EmpowerTypeData{
  String name;
  String key;

  EmpowerTypeData({
    required this.name,
    required this.key
  });

}


class EmpowerData {
  String? id;
  String? fromPerson;
  String? fromIdentity;
  String? toPerson;
  String? toIdentity;
  String? type; // all application process
  String? startTime;
  String? completedTime;
  bool? enable;

  // 流程
  String? edition; // 流程版本号
  String? process;
  String? processName;
  String? processAlias;

  // 应用
  String? application;
  String? applicationName;
  String? applicationAlias;
  EmpowerData(
      {this.id,
      this.fromPerson,
      this.fromIdentity,
      this.toPerson,
      this.toIdentity,
      this.type,
      this.startTime,
      this.completedTime,
      this.enable,
      this.edition,
      this.process,
      this.processAlias,
      this.processName,
      this.application,
      this.applicationAlias,
      this.applicationName});

      factory EmpowerData.fromJson(Map<String, dynamic> map) => EmpowerData(
        id: map['id'],
        fromPerson: map['fromPerson'],
        fromIdentity: map['fromIdentity'],
        toPerson: map['toPerson'],
        toIdentity: map['toIdentity'],
        type: map['type'],
        startTime: map['startTime'],
        completedTime: map['completedTime'],
        enable: map['enable'],
        edition: map['edition'],
        process: map['process'],
        processAlias: map['processAlias'],
        processName: map['processName'],
        application: map['application'],
        applicationAlias: map['applicationAlias'],
        applicationName: map['applicationName'],
      );

      Map<String, dynamic> toJson() => {
        "id": id,
        "fromPerson": fromPerson,
        "fromIdentity": fromIdentity,
        "toPerson": toPerson,
        "toIdentity": toIdentity,
        "type": type,
        "startTime": startTime,
        "completedTime": completedTime,
        "enable": enable,
        "edition": edition,
        "process": process,
        "processAlias": processAlias,
        "processName": processName,
        "application": application,
        "applicationAlias": applicationAlias,
        "applicationName": applicationName,
      };
}
