class ZoneFileSaveToPerson{
  List<String>? attIdList;
  List<String>? folderIdList;

  ZoneFileSaveToPerson({
    this.attIdList,
    this.folderIdList
  });

  Map<String, dynamic> toJson() => {
    "attIdList": attIdList,
    "folderIdList": folderIdList
  };
}