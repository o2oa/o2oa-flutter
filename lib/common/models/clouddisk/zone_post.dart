class CloudDiskZonePost{
    String? name;
    String? description;

  CloudDiskZonePost({this.name, this.description});


  Map<String, dynamic> toJson() => {
    "name": name,
    "description": description
  };
}