import 'index.dart';

class CloudDiskItem {
  FolderInfo? folder;
  FileInfo? file;

  String displayName;
  bool isFolder;

  CloudDiskItem(this.displayName, {this.file, this.folder, this.isFolder = false});

  CloudDiskItem.file(FileInfo file) : this(file.name ?? '', file: file, isFolder: false);
  CloudDiskItem.folder(FolderInfo folder) : this(folder.name ?? '', folder: folder, isFolder: true);



  @override
  bool operator ==(other) {
    if (other is CloudDiskItem) {
      if (other.isFolder && isFolder) {
        return other.folder!.id == folder!.id;
      }
      if (!other.isFolder && !isFolder) {
        return other.file!.id == file!.id;
      }
    }
    return false;
  }

   @override
  int get hashCode => isFolder ? folder?.id.hashCode ?? 0 : file?.id.hashCode ?? 0;
  
}
