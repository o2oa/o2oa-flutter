
class FileInfo {
    FileInfo({
        this.id,
        this.person,
        this.name,
        this.extension,
        this.length,
        this.type,
        this.folder,
        this.originFile,
        this.status,
        this.lastUpdateTime,
        this.lastUpdatePerson,
        this.createTime,
        this.updateTime,
         this.isAdmin, 
        this.isEditor,
        this.isZone,
        this.zoneId
    });

    String? id;
    String? person;
    String? name;
    String? extension;
    int? length;
    String? type;
    String? folder;
    String? originFile;
    String? status;
    String? lastUpdateTime;
    String? lastUpdatePerson;
    String? createTime;
    String? updateTime;

    // v3 版本字段
    bool? isAdmin;
    bool? isEditor;
    bool? isZone;
    String? zoneId;

    // 不同的接口
    bool isV3 = false;

    String fileNamePlusExtension() {
      return '$name.$extension';
    }

    factory FileInfo.fromJson(Map<String, dynamic> json) => FileInfo(
        id: json["id"],
        person: json["person"],
        name: json["name"],
        extension: json["extension"],
        length: json["length"],
        type: json["type"],
        folder: json["folder"],
        originFile: json["originFile"],
        status: json["status"],
        lastUpdateTime: json["lastUpdateTime"],
        lastUpdatePerson: json["lastUpdatePerson"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
         isAdmin: json["isAdmin"],
        isEditor: json["isEditor"],
        isZone: json["isZone"],
        zoneId: json["zoneId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "person": person,
        "name": name,
        "extension": extension,
        "length": length,
        "type": type,
        "folder": folder,
        "originFile": originFile,
        "status": status,
        "lastUpdateTime": lastUpdateTime,
        "lastUpdatePerson": lastUpdatePerson,
        "createTime": createTime,
        "updateTime": updateTime,
        "isAdmin": isAdmin,
        "isEditor": isEditor,
        "isZone": isZone,
        "zoneId": zoneId,
    };
}