class FolderPost {
  String? name;
  String? superior; // superior上级id (为空就是顶级)

  FolderPost({this.name, this.superior});

  Map<String, dynamic> toJson() => {
    "name": name,
    "superior": superior,
  };
}