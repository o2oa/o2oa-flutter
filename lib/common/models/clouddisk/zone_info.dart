 

class CloudDiskZoneInfo {
    bool? isAdmin;
    bool? isEditor;
    bool? isZone;
    String? id;
    String? person;
    String? name;
    String? superior;
    String? zoneId;
    String? status;
    String? lastUpdatePerson;
    String? lastUpdateTime;
    String? description;
    bool? hasSetPermission;
    String? createTime;
    String? updateTime;

    String? groupTag; // 分组使用

    CloudDiskZoneInfo({
        this.isAdmin,
        this.isEditor,
        this.isZone,
        this.id,
        this.person,
        this.name,
        this.superior,
        this.zoneId,
        this.status,
        this.lastUpdatePerson,
        this.lastUpdateTime,
        this.description,
        this.hasSetPermission,
        this.createTime,
        this.updateTime,
        this.groupTag
    });

    factory CloudDiskZoneInfo.fromJson(Map<String, dynamic> json) => CloudDiskZoneInfo(
        isAdmin: json["isAdmin"],
        isEditor: json["isEditor"],
        isZone: json["isZone"],
        id: json["id"],
        person: json["person"],
        name: json["name"],
        superior: json["superior"],
        zoneId: json["zoneId"],
        status: json["status"],
        lastUpdatePerson: json["lastUpdatePerson"],
        lastUpdateTime: json["lastUpdateTime"] ,
        description: json["description"],
        hasSetPermission: json["hasSetPermission"],
        createTime: json["createTime"]  ,
        updateTime: json["updateTime"] ,
        groupTag: json["groupTag"] ,
    );

    Map<String, dynamic> toJson() => {
        "isAdmin": isAdmin,
        "isEditor": isEditor,
        "isZone": isZone,
        "id": id,
        "person": person,
        "name": name,
        "superior": superior,
        "zoneId": zoneId,
        "status": status,
        "lastUpdatePerson": lastUpdatePerson,
        "lastUpdateTime": lastUpdateTime ,
        "description": description,
        "hasSetPermission": hasSetPermission,
        "createTime": createTime ,
        "updateTime": updateTime ,
        "groupTag": groupTag ,
    };
}
