enum IMChatMenuType { report, showGroupInfo, updateTitle, updateMembers, deleteConv }

class IMChatMenu {
  IMChatMenu(
    this.name,
    this.type,
  );

  String name;
  IMChatMenuType type;

 
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'type': type.name,
    };
  }
}
