class ImSpeechAssistantCommand {
  String? name; // 命令代码
  String? answer; // 直接回答的参数，如果有这个值，直接返回这个值的内容给用户
  String? processId; // 流程， 特殊处理，比如请假流程命令的时候，传入请假流程的 id，这边可以查询对应的流程进行启动
  String? portalId; // 门户 id ，根据这个 portalId打开门户，如果pageId也有值就打开门户对应的页面。
  String? pageId; //门户的页面 id

  ImSpeechAssistantCommand({this.name, this.processId, this.answer, this.portalId, this.pageId});

  factory ImSpeechAssistantCommand.fromJson(Map<String, dynamic> json) =>
      ImSpeechAssistantCommand(
        name: json["name"],
        processId: json["processId"],
        answer: json["answer"],
        portalId: json["portalId"],
        pageId: json["pageId"],
      );
  Map<String, dynamic> toJson() => {
        "name": name,
        "processId": processId,
        "answer": answer,
        "portalId": portalId,
        "pageId": pageId,
      };
}

class ImSpeechAssistantResponse {
  static String leftSide = 'left';
  static String rightSide = 'right';

  String? msg; // 语音识别后的文字内容，或者 side=left 的时候提示文字内容
  ImSpeechAssistantCommand? command;
  String? err; // 如果没有命令，提示错误信息

  String? side; // right (用户输入的) left （系统生成的)
  DateTime? createTime; // 消息时间


  ImSpeechAssistantResponse(
      {this.msg, this.command, this.err, this.side, this.createTime});

  factory ImSpeechAssistantResponse.fromJson(Map<String, dynamic> json) =>
      ImSpeechAssistantResponse(
        command: json["command"] == null
            ? null
            : ImSpeechAssistantCommand.fromJson(json["command"]),
        msg: json["msg"],
        err: json["err"],
        side: json["side"],
        createTime: json["createTime"],
      );

  Map<String, dynamic> toJson() => {
        "command": command == null ? null : command!.toJson(),
        "msg": msg,
        "err": err,
        "side": side,
        "createTime": createTime,
      };
}
