
import '../../utils/index.dart';
import 'im_msg_body.dart';

class IMMessagePageData {
  int page = 1; //当前页面
  int totalPage = 1; //总页数
  List<IMMessage> list = [];

  IMMessagePageData({
    this.page = 1,
    this.totalPage = 1,
    this.list = const <IMMessage>[],
  });

}

class IMMessage {
  IMMessage(
      {this.id,
      this.conversationId,
      this.quoteMessageId,
      this.body,
      this.createPerson,
      this.createTime,
      this.sendStatus,
      this.quoteMessage});

  String? id;
  String? conversationId;
  String? quoteMessageId; // 引用消息id
  String? body;
  String? createPerson;
  String? createTime;

  //消息发送状态 0正常 1发送中 2发送失败要重试
  int? sendStatus;

  IMMessage? quoteMessage; // 引用消息


  /// 
  /// 重写 == 界面上remove对象的时候使用
  @override
  bool operator ==(other) {
    return other is IMMessage && other.id == id;
  }

   @override
  int get hashCode => id.hashCode;

  factory IMMessage.fromJson(Map<String, dynamic> json) => IMMessage(
        id: json['id'],
        conversationId: json['conversationId'],
        quoteMessageId: json['quoteMessageId'],
        body: json['body'],
        createPerson: json['createPerson'],
        createTime: json['createTime'],
        sendStatus: json['sendStatus'],
        quoteMessage: json['quoteMessage'] == null ? null : IMMessage.fromJson(json['quoteMessage']),
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "conversationId": conversationId,
        "quoteMessageId": quoteMessageId,
        "body": body,
        "createPerson": createPerson,
        "createTime": createTime,
        "sendStatus": sendStatus,
        "quoteMessage": quoteMessage?.toJson()
      };

  IMMessageBody? toBody() {
    if (body != null && body!.isNotEmpty) {
      return IMMessageBody.fromJson(O2Utils.parseStringToJson(body!));
    }
    return null;
  }
}

class IMMessageCollection {
  String? id;
  String? createTime;
  String? messageId;
  String? createPerson;
  IMMessage? message;

  IMMessageCollection({
    this.id,
    this.createTime,
    this.messageId,
    this.createPerson,
    this.message,
  });

  factory IMMessageCollection.fromJson(Map<String, dynamic> json) => IMMessageCollection(
        id: json['id'],
        createTime: json['createTime'],
        messageId: json['messageId'],
        createPerson: json['createPerson'],
        message: json['message'] == null ? null : IMMessage.fromJson(json['message']),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createTime": createTime,
        "messageId": messageId,
        "createPerson": createPerson,
        "message": message?.toJson()
  };
}