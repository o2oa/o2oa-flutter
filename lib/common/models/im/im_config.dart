class IMConfigData {
  IMConfigData(
      {this.enableClearMsg,
      this.enableRevokeMsg,
      this.enableOnlyOfficePreview,
      this.revokeOutMinute,
      this.versionNo,
      this.changelog});

  bool? enableClearMsg;
  bool? enableRevokeMsg;
  bool? enableOnlyOfficePreview;
  int? revokeOutMinute;
  int? versionNo;
  String? changelog;

  factory IMConfigData.fromJson(Map<String, dynamic> json) => IMConfigData(
        enableClearMsg: json['enableClearMsg'],
        enableRevokeMsg: json['enableRevokeMsg'],
        enableOnlyOfficePreview: json['enableOnlyOfficePreview'],
        versionNo: json['versionNo'],
        revokeOutMinute: json['revokeOutMinute'],
        changelog: json['changelog'],
      );
  Map<String, dynamic> toJson() => {
        "enableClearMsg": enableClearMsg,
        "enableRevokeMsg": enableRevokeMsg,
        "enableOnlyOfficePreview": enableOnlyOfficePreview,
        'versionNo': versionNo,
        'changelog': changelog,
        'revokeOutMinute': revokeOutMinute
      };

  // 新增的功能，服务端版本是否支持
  bool enableForwardAndCollect() {
    return versionNo != null && versionNo! >= 300;
  }
}
