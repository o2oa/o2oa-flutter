///
/// 系统消息实例
///
class InstantMsg {
  String? id;
  String? title;
  String? body;
  String? type;
  String? person;
  bool? consumed;
  String? createTime;
  String? updateTime;

  InstantMsg({
    this.id,
    this.title,
    this.body,
    this.type,
    this.person,
    this.consumed,
    this.createTime,
    this.updateTime,
  });

  factory InstantMsg.fromJson(Map<String, dynamic> json) => InstantMsg(
        id: json["id"],
        title: json["title"],
        body: json["body"],
        type: json["type"],
        person: json["person"],
        consumed: json["consumed"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "body": body,
        "type": type,
        "person": person,
        "consumed": consumed,
        "createTime": createTime,
        "updateTime": updateTime,
      };

  /// 是否 custom 消息
  bool isCustomType() {
    return (type != null && type!.startsWith("custom"));
  }
}

///custom 文本消息
class InstantCustomO2AppTextMsg {
  String? content;
  String? url;
  bool? openExternally;
  InstantCustomO2AppTextMsg({this.content, this.url, this.openExternally});
  factory InstantCustomO2AppTextMsg.fromJson(Map<String, dynamic> json) =>
      InstantCustomO2AppTextMsg(content: json['content'], url: json['url'], openExternally: json['openExternally']);
}

///custom 图片消息
class InstantCustomO2AppImageMsg {
  String? url;
  InstantCustomO2AppImageMsg({this.url});
  factory InstantCustomO2AppImageMsg.fromJson(Map<String, dynamic> json) =>
      InstantCustomO2AppImageMsg(url: json['url']);
}

///custom 卡片消息
class InstantCustomO2AppCardMsg {
  String? title;
  String? desc;
  String? url;
  bool? openExternally;
  InstantCustomO2AppCardMsg({this.title, this.desc, this.url, this.openExternally});
  factory InstantCustomO2AppCardMsg.fromJson(Map<String, dynamic> json) =>
      InstantCustomO2AppCardMsg(
          title: json['title'], desc: json['desc'], url: json['url'], openExternally: json['openExternally']);
}

class InstantCustomO2AppMsg {
  String? msgtype; // text image textcard
  InstantCustomO2AppTextMsg? text;
  InstantCustomO2AppImageMsg? image;
  InstantCustomO2AppCardMsg? textcard;
  InstantCustomO2AppMsg({this.msgtype, this.text, this.image, this.textcard});
  factory InstantCustomO2AppMsg.fromJson(Map<String, dynamic> json) =>
      InstantCustomO2AppMsg(
        msgtype: json["msgtype"],
        text: json["text"] == null
            ? null
            : InstantCustomO2AppTextMsg.fromJson(json['text']),
        image: json["image"] == null
            ? null
            : InstantCustomO2AppImageMsg.fromJson(json['image']),
        textcard: json["textcard"] == null
            ? null
            : InstantCustomO2AppCardMsg.fromJson(json['textcard']),
      );
}

class InstantCustomO2AppMsgBody {
  InstantCustomO2AppMsg? o2AppMsg;
  InstantCustomO2AppMsgBody({this.o2AppMsg});

  factory InstantCustomO2AppMsgBody.fromJson(Map<String, dynamic> json) =>
      InstantCustomO2AppMsgBody(
        o2AppMsg: json["o2AppMsg"] == null
            ? null
            : InstantCustomO2AppMsg.fromJson(json["o2AppMsg"]),
      );
}
