class MeetingCheckInModel {
  List<String>? checkinPersonList; //已签到的人员列表

  MeetingCheckInModel({this.checkinPersonList});

  MeetingCheckInModel.fromJson(Map<String, dynamic> json) {
     checkinPersonList = json["checkinPersonList"] == null ? null : List<String>.from(json["checkinPersonList"].map((x) => x));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['checkinPersonList'] = checkinPersonList;
    return data;
  }
}
