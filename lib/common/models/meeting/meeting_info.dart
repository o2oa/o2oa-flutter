// To parse this JSON data, do
//
//     final meetingInfoModel = meetingInfoModelFromJson(jsonString);


import 'meeting_attachment.dart';
import 'meeting_room.dart';

class MeetingInfoModel {
    MeetingInfoModel({
        this.status,
        this.myApply,
        this.myWaitConfirm,
        this.myWaitAccept,
        this.myAccept,
        this.myReject,
        this.woRoom,
        this.attachmentList,
        this.id,
        this.subject,
        this.pinyin,
        this.pinyinInitial,
        this.summary,
        this.room,
        this.startTime,
        this.completedTime,
        this.inviteMemberList,
        this.invitePersonList,
        this.inviteDelPersonList,
        this.acceptPersonList,
        this.rejectPersonList,
        this.checkinPersonList,
        this.confirmStatus,
        this.manualCompleted,
        this.applicant,
        this.hostUnit,
        this.hostPerson,
        this.type,
        this.createTime,
        this.updateTime,
        this.mode,
        this.roomLink,
        this.roomId
    });

    String? status; // wait  processing completed
    bool? myApply;
    bool? myWaitConfirm;
    bool? myWaitAccept;
    bool? myAccept;
    bool? myReject;
    MeetingRoom? woRoom;
    List<MeetingAttachmentModel>? attachmentList;
    String? id;
    String? subject;
    String? pinyin;
    String? pinyinInitial;
    String? summary;
    String? room;
    String? startTime;
    String? completedTime;
    List<String>? inviteMemberList;
    List<String>? invitePersonList;
    List<String>? inviteDelPersonList;
    List<String>? acceptPersonList;
    List<String>? rejectPersonList;
    List<String>? checkinPersonList;
    String? confirmStatus;
    bool? manualCompleted;
    String? applicant; // 会议申请人
    String? hostUnit;
    String? hostPerson;
    String? type;
    String? createTime;
    String? updateTime;
     // 2023-05 新增字段 在线会议
    String? mode; // online|线上会议;offline|线下会议	
    String? roomId; // 在线会议房间号
    String? roomLink; // 在线会议打开链接

    factory MeetingInfoModel.fromJson(Map<String, dynamic> json) => MeetingInfoModel(
        status: json["status"],
        myApply: json["myApply"],
        myWaitConfirm: json["myWaitConfirm"] ,
        myWaitAccept: json["myWaitAccept"],
        myAccept: json["myAccept"] ,
        myReject: json["myReject"] ,
        woRoom: json["woRoom"] == null ? null : MeetingRoom.fromJson(json["woRoom"]),
        attachmentList: json["attachmentList"] == null ? null : List<MeetingAttachmentModel>.from(json["attachmentList"].map((x) => MeetingAttachmentModel.fromJson(x))),
        id: json["id"] ,
        subject: json["subject"] ,
        pinyin: json["pinyin"],
        pinyinInitial: json["pinyinInitial"] ,
        summary: json["summary"] ,
        room: json["room"] ,
        startTime: json["startTime"] ,
        completedTime: json["completedTime"],
        inviteMemberList: json["inviteMemberList"] == null ? null : List<String>.from(json["inviteMemberList"].map((x) => x)),
        invitePersonList: json["invitePersonList"] == null ? null : List<String>.from(json["invitePersonList"].map((x) => x)),
        inviteDelPersonList: json["inviteDelPersonList"] == null ? null : List<String>.from(json["inviteDelPersonList"].map((x) => x)),
        acceptPersonList: json["acceptPersonList"] == null ? null : List<String>.from(json["acceptPersonList"].map((x) => x)),
        rejectPersonList: json["rejectPersonList"] == null ? null : List<String>.from(json["rejectPersonList"].map((x) => x)),
        checkinPersonList: json["checkinPersonList"] == null ? null : List<String>.from(json["checkinPersonList"].map((x) => x)),
        confirmStatus: json["confirmStatus"],
        manualCompleted: json["manualCompleted"],
        applicant: json["applicant"],
        hostUnit: json["hostUnit"],
        hostPerson: json["hostPerson"],
        type: json["type"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        mode: json["mode"],
        roomId: json["roomId"],
        roomLink: json["roomLink"],
    );

    Map<String, dynamic> toJson() => {
        "status": status ,
        "myApply": myApply,
        "myWaitConfirm": myWaitConfirm ,
        "myWaitAccept": myWaitAccept ,
        "myAccept": myAccept ,
        "myReject": myReject,
        "woRoom": woRoom,
        "attachmentList": attachmentList == null ? null : attachmentList!.map((e) => e.toJson()).toList() ,
        "id": id,
        "subject": subject ,
        "pinyin": pinyin,
        "pinyinInitial": pinyinInitial ,
        "summary": summary ,
        "room": room,
        "startTime": startTime ,
        "completedTime": completedTime ,
        "inviteMemberList": inviteMemberList ,
        "invitePersonList": invitePersonList ,
        "inviteDelPersonList": inviteDelPersonList ,
        "acceptPersonList": acceptPersonList ,
        "rejectPersonList": rejectPersonList ,
        "checkinPersonList": checkinPersonList,
        "confirmStatus": confirmStatus ,
        "manualCompleted": manualCompleted ,
        "applicant": applicant ,
        "hostUnit": hostUnit,
        "hostPerson": hostPerson ,
        "type": type ,
        "createTime": createTime ,
        "updateTime": updateTime,
        "mode": mode,
        "roomId": roomId,
        "roomLink": roomLink,
    };

    MeetingInfoModel newInstanceCopyValue() => MeetingInfoModel(
      status: status ,
      myApply: myApply,
      myWaitConfirm: myWaitConfirm ,
      myWaitAccept: myWaitAccept ,
      myAccept: myAccept ,
      myReject: myReject,
      woRoom: woRoom,
      attachmentList: attachmentList ,
      id: id,
      subject: subject ,
      pinyin: pinyin,
      pinyinInitial: pinyinInitial ,
      summary: summary ,
      room: room,
      startTime: startTime ,
      completedTime: completedTime ,
      inviteMemberList: inviteMemberList ,
      invitePersonList: invitePersonList ,
      inviteDelPersonList: inviteDelPersonList ,
      acceptPersonList: acceptPersonList ,
      rejectPersonList: rejectPersonList ,
      checkinPersonList: checkinPersonList,
      confirmStatus: confirmStatus ,
      manualCompleted: manualCompleted ,
      applicant: applicant ,
      hostUnit: hostUnit,
      hostPerson: hostPerson ,
      type: type ,
      createTime: createTime ,
      updateTime: updateTime,
      mode: mode,
      roomId: roomId,
      roomLink: roomLink,
    );
}

