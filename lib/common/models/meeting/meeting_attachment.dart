
/// 会议材料
class MeetingAttachmentModel {

  String? id;
  String? name;
  String? meeting;
  String? extension;
  int? length;
  bool? summary; //是否是会议纪要附件.	 
 




  MeetingAttachmentModel({ this.id, this.name, this.meeting, this.extension, this.length, this.summary});

  MeetingAttachmentModel.fromJson(Map<String, dynamic> json){
      id = json['id'];
      name = json['name'];
      meeting = json['meeting'];
      extension = json['extension'];
      length = json['length'];
      summary = json['summary'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['meeting'] = meeting;
    data['extension'] = extension;
    data['length'] = length;
    data['summary'] = summary;
    return data;
  }

  @override
  bool operator ==(other) {
    return other is MeetingAttachmentModel && other.id == id;
  }

   @override
  int get hashCode => id.hashCode;

}