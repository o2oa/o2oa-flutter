
import 'o2_identity.dart';
import 'o2_person_attribute.dart';

class O2Person {
  String? token;
  String? tokenType; //（anonymous cihper manager user ） cipher代表是服务器之间的连接
  String? id;
  String? distinguishedName;
  String? unique;
  String? createTime;
  String? updateTime;
  String? genderType;
  String? pinyin;
  String? pinyinInitial;
  String? name;
  String? employee;
  String? display;
  String? mail;
  String? qq;
  String? weixin;
  String? mobile;
  String? showMobile;
  String? officePhone;
  String? signature;
  String? boardDate;  // 入职时间
  String? superior; //汇报关系
  String? language; //语言 zh-CN en es
  List<String>? roleList;

  List<O2Identity>? identityList; // 身份列表 当前登录用户只有这个属性

  /// 下面两个属性是查询用户对象的时候才有
  List<O2Identity>? woIdentityList; // 身份列表
  List<O2PersonAttribute>? woPersonAttributeList; //  人员属性列表


  @override
  bool operator ==(other) {
    return other is O2Person && other.id == id;
  }

   @override
  int get hashCode => id.hashCode;

  O2Person.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      token = jsonMap['token'];
      tokenType = jsonMap['tokenType'];
      id = jsonMap['id'];
      distinguishedName = jsonMap['distinguishedName'];
      unique = jsonMap['unique'];
      createTime = jsonMap['createTime'];
      updateTime = jsonMap['updateTime'];
      genderType = jsonMap['genderType'];
      pinyin = jsonMap['pinyin'];
      pinyinInitial = jsonMap['pinyinInitial'];
      name = jsonMap['name'];
      employee = jsonMap['employee'];
      display = jsonMap['display'];
      mail = jsonMap['mail'];
      qq = jsonMap['qq'];
      weixin = jsonMap['weixin'];
      mobile = jsonMap['mobile'];
      showMobile = jsonMap['mobile'];
      officePhone = jsonMap['officePhone'];
      signature = jsonMap['signature'];
      superior = jsonMap['superior'];
      boardDate = jsonMap['boardDate'];
      language = jsonMap['language'];
      roleList = jsonMap["roleList"] == null? null : List<String>.from(jsonMap["roleList"].map((x) => x));
      identityList = jsonMap["identityList"] == null? null : List<O2Identity>.from(jsonMap["identityList"].map((x) => O2Identity.fromJson(x)));
      woIdentityList = jsonMap["woIdentityList"] == null? null : List<O2Identity>.from(jsonMap["woIdentityList"].map((x) => O2Identity.fromJson(x)));
      woPersonAttributeList = jsonMap["woPersonAttributeList"] == null? null : List<O2PersonAttribute>.from(jsonMap["woPersonAttributeList"].map((x) => O2PersonAttribute.fromJson(x)));
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "pinyin": pinyin,
        "pinyinInitial": pinyinInitial,
        "token": token,
        "tokenType": tokenType,
        "distinguishedName": distinguishedName,
        "unique": unique,
        "createTime": createTime,
        "updateTime": updateTime,
        "genderType": genderType,
        "employee": employee,
        "display": display,
        "mail": mail,
        "qq": qq,
        "signature": signature,
        "superior": superior,
        "weixin": weixin,
        "mobile": mobile,
        "showMobile": showMobile,
        "officePhone": officePhone,
        "roleList": roleList,
        "identityList": identityList,
        "woIdentityList": woIdentityList,
        "woPersonAttributeList": woPersonAttributeList,
        "boardDate": boardDate,
        "language": language
      };
}
