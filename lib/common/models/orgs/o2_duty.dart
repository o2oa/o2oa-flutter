
class O2Duty {
    String? id;
    String? pinyin;
    String? pinyinInitial;
    String? description;
    String? name;
    String? unique;
    String? distinguishedName;
    String? unit;
    int? orderNumber;
    List<String>? identityList;
    String? createTime;
    String? updateTime;

    O2Duty({
        this.id,
        this.pinyin,
        this.pinyinInitial,
        this.description,
        this.name,
        this.unique,
        this.distinguishedName,
        this.unit,
        this.orderNumber,
        this.identityList,
        this.createTime,
        this.updateTime,
    });

    factory O2Duty.fromJson(Map<String, dynamic> json) => O2Duty(
        id: json["id"],
        pinyin: json["pinyin"],
        pinyinInitial: json["pinyinInitial"],
        description: json["description"],
        name: json["name"],
        unique: json["unique"],
        distinguishedName: json["distinguishedName"],
        unit: json["unit"],
        orderNumber: json["orderNumber"],
        identityList: json["identityList"] == null ? [] : List<String>.from(json["identityList"]!.map((x) => x)),
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "pinyin": pinyin,
        "pinyinInitial": pinyinInitial,
        "description": description,
        "name": name,
        "unique": unique,
        "distinguishedName": distinguishedName,
        "unit": unit,
        "orderNumber": orderNumber,
        "identityList": identityList == null ? [] : List<dynamic>.from(identityList!.map((x) => x)),
        "createTime": createTime,
        "updateTime": updateTime,
    };
}