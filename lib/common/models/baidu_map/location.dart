


class LocationData{

  static int locationDataModePicker = 0;
  static int locationDataModeShow = 1;

  LocationData(this.mode, {
    this.address,
    this.addressDetail,
    this.latitude,
    this.longitude
  });

  int? mode; //0: 选择位置 1: 显示地图位置
  String? address; //type=location的时候位置信息
  String? addressDetail;
  double? latitude;//type=location的时候位置信息
  double? longitude;//type=location的时候位置信息

LocationData.fromJson(Map<String, dynamic> jsonMap) {
    mode = jsonMap['mode'];
    address = jsonMap['address'];
    addressDetail = jsonMap['addressDetail'];
    latitude = jsonMap['latitude'];
    longitude = jsonMap['longitude'];
  }
}