/// 板块权限对象
class SectionPermissionInfo {
  SectionPermissionInfo({
    this.subjectPublishAble,
    this.subjectAuditAble,
    this.subjectManageAble,
    this.subjectRecommendAble,
    this.subjectStickAble,
    this.subjectCreamAble,
    this.replyPublishAble,
    this.replyAuditAble,
    this.replyManageAble,
    this.sectionManageAble,
    this.sectionPermissoinManageAble,
    this.sectionConfigManageAble,
  });

  bool? subjectPublishAble; // 发帖权限
  bool? subjectAuditAble;
  bool? subjectManageAble;
  bool? subjectRecommendAble;
  bool? subjectStickAble;
  bool? subjectCreamAble;
  bool? replyPublishAble;
  bool? replyAuditAble;
  bool? replyManageAble;
  bool? sectionManageAble;
  bool? sectionPermissoinManageAble;
  bool? sectionConfigManageAble;

  factory SectionPermissionInfo.fromJson(Map<String, dynamic> json) =>
      SectionPermissionInfo(
        subjectPublishAble: json["subjectPublishAble"],
        subjectAuditAble: json["subjectAuditAble"],
        subjectManageAble: json["subjectManageAble"],
        subjectRecommendAble: json["subjectRecommendAble"],
        subjectStickAble: json["subjectStickAble"],
        subjectCreamAble: json["subjectCreamAble"],
        replyPublishAble: json["replyPublishAble"],
        replyAuditAble: json["replyAuditAble"],
        replyManageAble: json["replyManageAble"],
        sectionManageAble: json["sectionManageAble"],
        sectionPermissoinManageAble: json["sectionPermissoinManageAble"],
        sectionConfigManageAble: json["sectionConfigManageAble"],
      );

  Map<String, dynamic> toJson() => {
        "subjectPublishAble": subjectPublishAble,
        "subjectAuditAble": subjectAuditAble,
        "subjectManageAble": subjectManageAble,
        "subjectRecommendAble": subjectRecommendAble,
        "subjectStickAble": subjectStickAble,
        "subjectCreamAble": subjectCreamAble,
        "replyPublishAble": replyPublishAble,
        "replyAuditAble": replyAuditAble,
        "replyManageAble": replyManageAble,
        "sectionManageAble": sectionManageAble,
        "sectionPermissoinManageAble": sectionPermissoinManageAble,
        "sectionConfigManageAble": sectionConfigManageAble,
      };
}
