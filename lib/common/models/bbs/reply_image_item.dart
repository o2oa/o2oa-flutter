
/// 图片上传处理用的数据对象
/// 发帖和回帖都用到了
class ReplyImageItem {
  String? fileId; // 上传后的id
  String? fileLocalPath; // 没有上次的本地地址
  int? width; //图片实际宽度
  int? height; //图片实际高度
  int? showWidth; //图片展现宽度
  int? showHeight; //图片展现高度

  ReplyImageItem(
      {this.fileId,
      this.fileLocalPath,
      this.width,
      this.height,
      this.showHeight,
      this.showWidth});

  Map<String, dynamic> toJson() => {
        "fileId": fileId,
        "fileLocalPath": fileLocalPath,
        "width": width,
        "height": height,
        "showHeight": showHeight,
        "showWidth": showWidth,
      };
}
