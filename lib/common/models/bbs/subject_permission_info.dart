
///
/// 帖子权限对象
///
class SubjectPermissionInfo {

SubjectPermissionInfo({
  this.auditAble,
  this.editAble,
  this.manageAble,
  this.recommendAble,
  this.stickAble,
  this.creamAble,
  this.replyPublishAble,
  this.replyAuditAble,
  this.replyManageAble
});
                          
                            
                           bool?  auditAble; // 用户是否可以审核该主题.
                           bool?  editAble; // 用户是否可以编辑该主题.
                           bool?  manageAble; // 用户是否可以管理该主题.
                           bool?  recommendAble; // 用户是否可以推荐该主题.
                           bool?  stickAble; // 用户是否可以置顶该主题.
                           bool?  creamAble; // 用户是否可以对该主题进行精华主题设置操作.	
                           bool?  replyPublishAble; // 用户是否可以在该主题中进行回复操作.
                           bool?  replyAuditAble; // 用户是否可以在版块中对回复进行审核.
                           bool?  replyManageAble; // 用户是否可以在主题中对回复进行查询或者删除.	
                           


    factory SubjectPermissionInfo.fromJson(Map<String, dynamic> json) => SubjectPermissionInfo(
        auditAble: json["auditAble"] ,
        editAble: json["editAble"] ,
        manageAble: json["manageAble"] ,
        recommendAble: json["recommendAble"] ,
        stickAble: json["stickAble"] ,
        creamAble: json["creamAble"] ,
        replyPublishAble: json["replyPublishAble"] ,
        replyAuditAble: json["replyAuditAble"] ,
        replyManageAble: json["replyManageAble"] 
    );

    Map<String, dynamic> toJson() => {
        "auditAble": auditAble  ,
        "editAble": editAble  ,
        "manageAble": manageAble ,
        "recommendAble": recommendAble ,
        "stickAble": stickAble ,
        "creamAble": creamAble ,
        "replyPublishAble": replyPublishAble ,
        "replyAuditAble": replyAuditAble ,
        "replyManageAble": replyManageAble ,
    };
}