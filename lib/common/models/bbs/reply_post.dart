class ReplyFormJson {
  String? id;
  String? content;
  String? subjectId;
  String? parentId;
  String? replyMachineName;
  String? replySystemName;

  ReplyFormJson(
      {this.id,
      this.content,
      this.subjectId,
      this.parentId,
      this.replyMachineName,
      this.replySystemName});

  Map<String, dynamic> toJson() => {
        "id": id,
        "content": content,
        "subjectId": subjectId,
        "parentId": parentId,
        "replyMachineName": replyMachineName,
        "replySystemName": replySystemName,
      };
}
