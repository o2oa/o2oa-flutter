 
/// app  更新对象 外网的
class O2AppUpdateInfo {
    O2AppUpdateAndroid? android;
     

    O2AppUpdateInfo({
        this.android,
    });

    factory O2AppUpdateInfo.fromJson(Map<String, dynamic> json) => O2AppUpdateInfo(
        android: json["android"] == null ? null : O2AppUpdateAndroid.fromJson(json["android"]),
    );

    Map<String, dynamic> toJson() => {
        "android": android?.toJson(),
    };
}

class O2AppUpdateAndroid {
    String? versionName;
    String? buildNo;
    String? downloadUrl;
    String? content;

    O2AppUpdateAndroid({
        this.versionName,
        this.buildNo,
        this.downloadUrl,
        this.content,
    });

    factory O2AppUpdateAndroid.fromJson(Map<String, dynamic> json) => O2AppUpdateAndroid(
        versionName: json["versionName"],
        buildNo: json["buildNo"],
        downloadUrl: json["downloadUrl"],
        content: json["content"],
    );

    Map<String, dynamic> toJson() => {
        "versionName": versionName,
        "buildNo": buildNo,
        "downloadUrl": downloadUrl,
        "content": content,
    };
}

/// 内部 app  打包结果对象
class O2AppInnerUpdateInfo {
    String? id;
    String? name;
    String? storage;
    String? extension;
    String? lastUpdateTime;
    int? length;
    String? packInfoId;
    String? appVersionName;
    String? appVersionNo;
    int? status; // 状态，异步下载文件所以需要这个状态，0开启，1下载完成， 2过程有异常.
    String? apkDownloadUrl; // 外部 apk 下载地址

  O2AppInnerUpdateInfo({
        this.id,
        this.name,
        this.storage,
        this.extension,
        this.lastUpdateTime,
        this.length,
        this.packInfoId,
        this.appVersionName,
        this.appVersionNo,
        this.status,
        this.apkDownloadUrl,
    });

    factory O2AppInnerUpdateInfo.fromJson(Map<String, dynamic> json) => O2AppInnerUpdateInfo(
        id: json["id"],
        name: json["name"],
        storage: json["storage"],
        extension: json["extension"],
        lastUpdateTime: json["lastUpdateTime"],
        length: json["length"],
        packInfoId: json["packInfoId"],
        appVersionName: json["appVersionName"],
        appVersionNo: json["appVersionNo"],
        status: json["status"],
        apkDownloadUrl: json["apkDownloadUrl"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "storage": storage,
        "extension": extension,
        "lastUpdateTime": lastUpdateTime,
        "length": length,
        "packInfoId": packInfoId,
        "appVersionName": appVersionName,
        "appVersionNo": appVersionNo,
        "status": status,
        "apkDownloadUrl": apkDownloadUrl,
         
    };
}
