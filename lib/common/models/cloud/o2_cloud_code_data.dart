
class CollectCodeData {
  String? mobile; 
  bool? value;
  String? meta;


  CollectCodeData(
      {this.mobile,
      this.value,
      this.meta,
      });

  CollectCodeData.fromJson(Map<String, dynamic> jsonMap) {
    mobile = jsonMap['mobile'];
    value = jsonMap['value'];
    meta = jsonMap['meta'];
     
  }

  Map<String, dynamic> toJson() => {
        "mobile": mobile,
        "value": value,
        "meta": meta,
      };
}