class EchoData {
  String? servletContextName;
  String? serverTime;

  EchoData({this.serverTime, this.servletContextName});

  EchoData.fromJson(Map<String, dynamic> jsonMap) {
    serverTime = jsonMap['serverTime'];
    servletContextName = jsonMap['servletContextName'];
  }
  Map<String, dynamic> toJson() => {
        "servletContextName": servletContextName,
        "serverTime": serverTime,
      };
}
