
class DocAttachmentInfo{
  DocAttachmentInfo({
        this.id,
        this.createTime,
        this.updateTime,
        this.name,
        this.extension,
        this.storage,
        this.length,
        this.appId,
        this.categoryId,
        this.documentId,
        this.fileType,
        this.creatorUid
    });
    String? id;
    String? createTime;
    String? updateTime;
    String? name;
    String? extension;
    String? storage;
    int? length;
    String? appId;
    String? categoryId;
    String? documentId;
    String? fileType;
    String? creatorUid;
      
    factory DocAttachmentInfo.fromJson(Map<String, dynamic> json) => DocAttachmentInfo(
        id: json["id"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        name: json["name"],
        extension: json["extension"],
        storage: json["storage"],
        length: json["length"],
        appId: json["appId"],
        categoryId: json["categoryId"],
        documentId: json["documentId"],
        fileType: json["fileType"],
        creatorUid: json["creatorUid"],
    );
}