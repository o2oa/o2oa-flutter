// To parse this JSON data, do
//
//     final documentData = documentDataFromJson(jsonString);
 

class DocumentData {
    DocumentData({
        this.id,
        this.createTime,
        this.publishTime,
        this.updateTime,
        this.summary,
        this.title,
        this.categoryAlias,
        this.categoryName,
        this.appId,
        this.categoryId,
        this.creatorPerson,
        this.creatorUnitName,
        this.creatorTopUnitName,
        this.docStatus,
        this.viewCount,
        this.commentCount,
        this.commendCount,
        this.hasIndexPic,
        this.pictureList,
        this.isTop,
        this.isAllRead,
        this.creatorPersonShort,
        this.creatorUnitNameShort,
        this.creatorTopUnitNameShort,
    });

    String? id;
    String? createTime;
    String? publishTime;
    String? updateTime;
    String? summary;
    String? title;
    String? categoryAlias;
    String? categoryName;
    String? appId;
    String? categoryId;
    String? creatorPerson;
    String? creatorUnitName;
    String? creatorTopUnitName;
    String? docStatus;
    int? viewCount;
    int? commentCount;
    int? commendCount;
    bool? hasIndexPic;
    List<String>? pictureList;
    bool? isTop;
    bool? isAllRead;
    String? creatorPersonShort;
    String? creatorUnitNameShort;
    String? creatorTopUnitNameShort;

    factory DocumentData.fromJson(Map<String, dynamic> json) => DocumentData(
        id: json["id"],
        createTime: json["createTime"],
        publishTime: json["publishTime"],
        updateTime: json["updateTime"],
        summary: json["summary"],
        title: json["title"],
        categoryAlias: json["categoryAlias"],
        categoryName: json["categoryName"],
        appId: json["appId"],
        categoryId: json["categoryId"],
        creatorPerson: json["creatorPerson"],
        creatorUnitName: json["creatorUnitName"],
        creatorTopUnitName: json["creatorTopUnitName"],
        docStatus: json["docStatus"],
        viewCount: json["viewCount"],
        commentCount: json["commentCount"],
        commendCount: json["commendCount"],
        hasIndexPic: json["hasIndexPic"],
        pictureList: json["pictureList"] == null? null : List<String>.from(json["pictureList"].map((x) => x)),
        isTop: json["isTop"],
        isAllRead: json["isAllRead"],
        creatorPersonShort: json["creatorPersonShort"],
        creatorUnitNameShort: json["creatorUnitNameShort"],
        creatorTopUnitNameShort: json["creatorTopUnitNameShort"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "createTime": createTime,
        "publishTime": publishTime,
        "updateTime": updateTime,
        "summary": summary,
        "title": title,
        "categoryAlias": categoryAlias,
        "categoryName": categoryName,
        "appId": appId,
        "categoryId": categoryId,
        "creatorPerson": creatorPerson,
        "creatorUnitName": creatorUnitName,
        "creatorTopUnitName": creatorTopUnitName,
        "docStatus": docStatus,
        "viewCount": viewCount,
        "commentCount": commentCount,
        "commendCount": commendCount,
        "hasIndexPic": hasIndexPic,
        "pictureList": pictureList == null ? null : List<String>.from(pictureList!.map((x) => x)),
        "isTop": isTop,
        "isAllRead": isAllRead,
        "creatorPersonShort": creatorPersonShort,
        "creatorUnitNameShort": creatorUnitNameShort,
        "creatorTopUnitNameShort": creatorTopUnitNameShort,
    };
}
