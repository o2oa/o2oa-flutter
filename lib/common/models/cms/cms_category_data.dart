import 'cms_app_data.dart';

class CmsCategoryData {
  CmsCategoryData({
    this.id,
    this.categoryName,
    this.appId,
    this.appName,
    this.documentType,
    this.categoryAlias,
    this.workflowType,
    this.workflowAppId,
    this.workflowAppName,
    this.workflowName,
    this.workflowFlag,
    this.formId,
    this.formName,
    this.readFormId,
    this.readFormName,
    this.defaultViewName,
    this.categorySeq,
    this.creatorPerson,
    this.creatorIdentity,
    this.creatorUnitName,
    this.creatorTopUnitName,
    this.allPeopleView,
    this.allPeoplePublish,
    this.importViewAppId,
    this.importViewId,
    this.importViewName,
    this.anonymousAble,
    this.sendNotify,
    this.blankToAllNotify,
    this.viewablePersonList,
    this.viewableUnitList,
    this.viewableGroupList,
    this.publishablePersonList,
    this.publishableUnitList,
    this.publishableGroupList,
    this.manageablePersonList,
    this.manageableUnitList,
    this.manageableGroupList,
    this.createTime,
    this.updateTime,
  });

  String? id;
  String? categoryName;
  String? appId;
  String? appName;
  String? documentType;
  String? categoryAlias;
  String? workflowType;
  String? workflowAppId;
  String? workflowAppName;
  String? workflowName;
  String? workflowFlag;
  String? formId;
  String? formName;
  String? readFormId;
  String? readFormName;
  String? defaultViewName;
  String? categorySeq;
  String? creatorPerson;
  String? creatorIdentity;
  String? creatorUnitName;
  String? creatorTopUnitName;
  bool? allPeopleView;
  bool? allPeoplePublish;
  String? importViewAppId;
  String? importViewId;
  String? importViewName;
  bool? anonymousAble;
  bool? sendNotify;
  bool? blankToAllNotify;
  List<String>? viewablePersonList;
  List<String>? viewableUnitList;
  List<String>? viewableGroupList;
  List<String>? publishablePersonList;
  List<String>? publishableUnitList;
  List<String>? publishableGroupList;
  List<String>? manageablePersonList;
  List<String>? manageableUnitList;
  List<String>? manageableGroupList;
  String? createTime;
  String? updateTime;

  CmsAppData? withApp;

  factory CmsCategoryData.fromJson(Map<String, dynamic> json) => CmsCategoryData(
        id: json["id"],
        categoryName: json["categoryName"],
        appId: json["appId"],
        appName: json["appName"],
        documentType: json["documentType"],
        categoryAlias: json["categoryAlias"],
        workflowType: json["workflowType"],
        workflowAppId: json["workflowAppId"],
        workflowAppName: json["workflowAppName"],
        workflowName: json["workflowName"],
        workflowFlag: json["workflowFlag"],
        formId: json["formId"],
        formName: json["formName"],
        readFormId: json["readFormId"],
        readFormName: json["readFormName"],
        defaultViewName: json["defaultViewName"],
        categorySeq: json["categorySeq"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnitName: json["creatorUnitName"],
        creatorTopUnitName: json["creatorTopUnitName"],
        allPeopleView: json["allPeopleView"],
        allPeoplePublish: json["allPeoplePublish"],
        importViewAppId: json["importViewAppId"],
        importViewId: json["importViewId"],
        importViewName: json["importViewName"],
        anonymousAble: json["anonymousAble"],
        sendNotify: json["sendNotify"],
        blankToAllNotify: json["blankToAllNotify"],
        viewablePersonList: List<String>.from(json["viewablePersonList"].map((x) => x)),
        viewableUnitList: List<String>.from(json["viewableUnitList"].map((x) => x)),
        viewableGroupList: List<String>.from(json["viewableGroupList"].map((x) => x)),
        publishablePersonList: List<String>.from(json["publishablePersonList"].map((x) => x)),
        publishableUnitList: List<String>.from(json["publishableUnitList"].map((x) => x)),
        publishableGroupList: List<String>.from(json["publishableGroupList"].map((x) => x)),
        manageablePersonList: List<String>.from(json["manageablePersonList"].map((x) => x)),
        manageableUnitList: List<String>.from(json["manageableUnitList"].map((x) => x)),
        manageableGroupList: List<String>.from(json["manageableGroupList"].map((x) => x)),
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "categoryName": categoryName,
        "appId": appId,
        "appName": appName,
        "documentType": documentType,
        "categoryAlias": categoryAlias,
        "workflowType": workflowType,
        "workflowAppId": workflowAppId,
        "workflowAppName": workflowAppName,
        "workflowName": workflowName,
        "workflowFlag": workflowFlag,
        "formId": formId,
        "formName": formName,
        "readFormId": readFormId,
        "readFormName": readFormName,
        "defaultViewName": defaultViewName,
        "categorySeq": categorySeq,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnitName": creatorUnitName,
        "creatorTopUnitName": creatorTopUnitName,
        "allPeopleView": allPeopleView,
        "allPeoplePublish": allPeoplePublish,
        "importViewAppId": importViewAppId,
        "importViewId": importViewId,
        "importViewName": importViewName,
        "anonymousAble": anonymousAble,
        "sendNotify": sendNotify,
        "blankToAllNotify": blankToAllNotify,
        "viewablePersonList": viewablePersonList,
        "viewableUnitList": viewableUnitList,
        "viewableGroupList": viewableGroupList,
        "publishablePersonList": publishablePersonList,
        "publishableUnitList": publishableUnitList,
        "publishableGroupList": publishableGroupList,
        "manageablePersonList": manageablePersonList,
        "manageableUnitList": manageableUnitList,
        "manageableGroupList": manageableGroupList,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}
