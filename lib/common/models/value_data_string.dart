
class ValueStringData {
  String? value;

   ValueStringData({this.value});
   ValueStringData.fromJson(Map<String, dynamic> jsonMap) {
     value = jsonMap['value'];
   }
   Map<String, dynamic> toJson() => {
        "value": value,
      };
}