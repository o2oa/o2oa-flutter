import 'package:get/get.dart';

import '../../../utils/index.dart';
import '../../enums/index.dart';

class PreCheckInData {
  PreCheckInData({
    this.allowFieldWork,
    this.requiredFieldWorkRemarks,
    this.canCheckIn,
    this.checkItemList,
    this.workPlaceList,
  });

  bool? allowFieldWork;
  bool? requiredFieldWorkRemarks;
  bool? canCheckIn;
  List<AttendanceV2Record>? checkItemList;
  List<WorkPlaceList>? workPlaceList;

  factory PreCheckInData.fromJson(Map<String, dynamic> json) => PreCheckInData(
        allowFieldWork: json["allowFieldWork"],
        requiredFieldWorkRemarks: json["requiredFieldWorkRemarks"],
        canCheckIn: json["canCheckIn"],
        checkItemList: json["checkItemList"] == null
            ? []
            : List<AttendanceV2Record>.from(json["checkItemList"]!
                .map((x) => AttendanceV2Record.fromJson(x))),
        workPlaceList: json["workPlaceList"] == null
            ? []
            : List<WorkPlaceList>.from(
                json["workPlaceList"]!.map((x) => WorkPlaceList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "allowFieldWork": allowFieldWork,
        "requiredFieldWorkRemarks": requiredFieldWorkRemarks,
        "canCheckIn": canCheckIn,
        "checkItemList": checkItemList == null
            ? []
            : List<dynamic>.from(checkItemList!.map((x) => x.toJson())),
        "workPlaceList": workPlaceList == null
            ? []
            : List<dynamic>.from(workPlaceList!.map((x) => x.toJson())),
      };
}

class AttendanceV2Record {
  AttendanceV2Record(
      {this.id,
      this.userId,
      this.recordDateString,
      this.recordDate,
      this.preDutyTime,
      this.preDutyTimeBeforeLimit,
      this.preDutyTimeAfterLimit,
      this.sourceType,
      this.checkInResult,
      this.checkInType,
      this.sourceDevice,
      this.description,
      this.groupId,
      this.groupCheckType,
      this.groupName,
      this.shiftId,
      this.shiftName,
      this.createTime,
      this.updateTime,
      this.sequence,
      this.fieldWork});

  String? id;
  String? userId;
  String? recordDateString;
  String? recordDate;
  String? preDutyTime;
  String? preDutyTimeBeforeLimit;
  String? preDutyTimeAfterLimit;
  String? sourceType;
  String? checkInResult;
  String? checkInType;
  String? sourceDevice;
  String? description;
  String? groupId;
  String? groupName;
  String? groupCheckType; // 1 固定班制 2 自由打卡 3 排班制
  String? shiftId;
  String? shiftName;
  String? createTime;
  String? updateTime;
  String? sequence;

  bool? fieldWork; // 是否外勤

  // 是否最后一条已经打卡过的数据
  bool isLastRecord = false;

  String checkInTypeText() {
    if (checkInType == 'OffDuty') {
      return 'attendance_offDuty'.tr;
    }
    return 'attendance_onDuty'.tr;
  }

  String resultText() {
    if (checkInResult == AttendanceV2RecordResultEnum.Normal.key) {
      return AttendanceV2RecordResultEnum.Normal.name;
    } else if (checkInResult == AttendanceV2RecordResultEnum.Early.key) {
      return AttendanceV2RecordResultEnum.Early.name;
    } else if (checkInResult == AttendanceV2RecordResultEnum.Late.key) {
      return AttendanceV2RecordResultEnum.Late.name;
    } else if (checkInResult == AttendanceV2RecordResultEnum.SeriousLate.key) {
      return AttendanceV2RecordResultEnum.SeriousLate.name;
    } else if (checkInResult == AttendanceV2RecordResultEnum.Absenteeism.key) {
      return AttendanceV2RecordResultEnum.Absenteeism.name;
    } else if (checkInResult == AttendanceV2RecordResultEnum.NotSigned.key) {
      return AttendanceV2RecordResultEnum.NotSigned.name;
    }
    return '';
  }

  factory AttendanceV2Record.fromJson(Map<String, dynamic> json) =>
      AttendanceV2Record(
        id: json["id"],
        userId: json["userId"],
        recordDateString: json["recordDateString"],
        recordDate: json["recordDate"],
        preDutyTime: json["preDutyTime"],
        preDutyTimeBeforeLimit: json["preDutyTimeBeforeLimit"],
        preDutyTimeAfterLimit: json["preDutyTimeAfterLimit"],
        sourceType: json["sourceType"],
        checkInResult: json["checkInResult"],
        checkInType: json["checkInType"],
        sourceDevice: json["sourceDevice"],
        description: json["description"],
        groupId: json["groupId"],
        groupName: json["groupName"],
        groupCheckType: json["groupCheckType"],
        shiftId: json["shiftId"],
        shiftName: json["shiftName"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        sequence: json["sequence"],
        fieldWork: json["fieldWork"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "recordDateString": recordDateString,
        "recordDate": recordDate,
        "preDutyTime": preDutyTime,
        "preDutyTimeBeforeLimit": preDutyTimeBeforeLimit,
        "preDutyTimeAfterLimit": preDutyTimeAfterLimit,
        "sourceType": sourceType,
        "checkInResult": checkInResult,
        "checkInType": checkInType,
        "sourceDevice": sourceDevice,
        "description": description,
        "groupId": groupId,
        "groupName": groupName,
        "groupCheckType": groupCheckType,
        "shiftId": shiftId,
        "shiftName": shiftName,
        "createTime": createTime,
        "updateTime": updateTime,
        "sequence": sequence,
        "fieldWork": fieldWork,
      };
}

class WorkPlaceList {
  WorkPlaceList({
    this.id,
    this.placeName,
    this.placeAlias,
    this.creator,
    this.longitude,
    this.latitude,
    this.gpsLng,
    this.gpsLat,
    this.errorRange,
    this.description,
    this.createTime,
    this.updateTime,
    this.sequence,
  });

  String? id;
  String? placeName;
  String? placeAlias;
  String? creator;
  String? longitude;
  String? latitude;
  String? gpsLng; // wgs84坐标
  String? gpsLat; // wgs84坐标
  int? errorRange;
  String? description;
  String? createTime;
  String? updateTime;
  String? sequence;

  factory WorkPlaceList.fromJson(Map<String, dynamic> json) => WorkPlaceList(
        id: json["id"],
        placeName: json["placeName"],
        placeAlias: json["placeAlias"],
        creator: json["creator"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        gpsLng: json["gpsLng"],
        gpsLat: json["gpsLat"],
        errorRange: json["errorRange"],
        description: json["description"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        sequence: json["sequence"],
      );

  /// 获取正确的坐标  gps [gpsLat] [gpsLng]
  /// 如果有 gps 坐标直接返回，没有就拿百度坐标通过计算返回
  List<double> getLngLat() {
    final lat = double.tryParse(gpsLat ?? '0') ?? 0;
    final lng = double.tryParse(gpsLng ?? '0') ?? 0;
    if (lat == 0 && lng == 0) {
      OLogger.i("没有 gps 坐标？？，开始转化！！！");
      final bdLat = double.tryParse(latitude ?? '0') ?? 0;
      final bdLng = double.tryParse(longitude ?? '0') ?? 0;
      return BaiduLocationTransformHelper.bd09towgs84(bdLng, bdLat);
    } else {
      return [lng, lat];
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "placeName": placeName,
        "placeAlias": placeAlias,
        "creator": creator,
        "longitude": longitude,
        "latitude": latitude,
        "gpsLng": gpsLng,
        "gpsLat": gpsLat,
        "errorRange": errorRange,
        "description": description,
        "createTime": createTime,
        "updateTime": updateTime,
        "sequence": sequence,
      };
}
