
class AttendanceV2Config {
    String? id;
    bool? appealEnable;
    int? appealMaxTimes;
    String? processId;
    String? processName;
    bool? onDutyFastCheckInEnable;
    bool? offDutyFastCheckInEnable;
    bool? checkInAlertEnable;
    bool? exceptionAlertEnable;
    String? exceptionAlertTime;
    String? exceptionAlertDate;
    String? createTime;
    String? updateTime;
    bool? closeOldAttendance; // 是否关闭旧版考勤

    AttendanceV2Config({
        this.id,
        this.appealEnable,
        this.appealMaxTimes,
        this.processId,
        this.processName,
        this.onDutyFastCheckInEnable,
        this.offDutyFastCheckInEnable,
        this.checkInAlertEnable,
        this.exceptionAlertEnable,
        this.exceptionAlertTime,
        this.exceptionAlertDate,
        this.createTime,
        this.updateTime,
        this.closeOldAttendance,
    });

    factory AttendanceV2Config.fromJson(Map<String, dynamic> json) => AttendanceV2Config(
        id: json["id"],
        appealEnable: json["appealEnable"],
        appealMaxTimes: json["appealMaxTimes"],
        processId: json["processId"],
        processName: json["processName"],
        onDutyFastCheckInEnable: json["onDutyFastCheckInEnable"],
        offDutyFastCheckInEnable: json["offDutyFastCheckInEnable"],
        checkInAlertEnable: json["checkInAlertEnable"],
        exceptionAlertEnable: json["exceptionAlertEnable"],
        exceptionAlertTime: json["exceptionAlertTime"],
        exceptionAlertDate: json["exceptionAlertDate"],
        createTime: json["createTime"] ,
        updateTime: json["updateTime"],
        closeOldAttendance: json["closeOldAttendance"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "appealEnable": appealEnable,
        "appealMaxTimes": appealMaxTimes,
        "processId": processId,
        "processName": processName,
        "onDutyFastCheckInEnable": onDutyFastCheckInEnable,
        "offDutyFastCheckInEnable": offDutyFastCheckInEnable,
        "checkInAlertEnable": checkInAlertEnable,
        "exceptionAlertEnable": exceptionAlertEnable,
        "exceptionAlertTime": exceptionAlertTime,
        "exceptionAlertDate": exceptionAlertDate,
        "createTime": createTime,
        "updateTime": updateTime,
        "closeOldAttendance": closeOldAttendance,
    };
}