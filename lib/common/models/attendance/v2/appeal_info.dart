

import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../enums/index.dart';
import 'pre_check_in_data.dart';

class AttendanceV2Appeal {
    AttendanceV2Record? record;
    String? id;
    String? recordId;
    String? userId;
    String? recordDateString;
    String? recordDate;
    String? startTime;
    int? status;
    String? jobId;
    bool? sendStatus;
    String? createTime;
    String? updateTime;
    String? updateStatusAdminPerson;

    AttendanceV2Appeal({
        this.record,
        this.id,
        this.recordId,
        this.userId,
        this.recordDateString,
        this.recordDate,
        this.startTime,
        this.status,
        this.jobId,
        this.sendStatus,
        this.createTime,
        this.updateTime,
        this.updateStatusAdminPerson
    });

    String statsText() {
      if (status == AttendanceV2AppealStatusEnum.StatusInit.key) {
        return AttendanceV2AppealStatusEnum.StatusInit.name;
      } else if (status == AttendanceV2AppealStatusEnum.StatusProcessing.key) {
        return AttendanceV2AppealStatusEnum.StatusProcessing.name;
      } else if (status == AttendanceV2AppealStatusEnum.StatusProcessAgree.key) {
        return AttendanceV2AppealStatusEnum.StatusProcessAgree.name;
      } else if (status == AttendanceV2AppealStatusEnum.StatusProcessDisagree.key) {
        return AttendanceV2AppealStatusEnum.StatusProcessDisagree.name;
      } else if (status == AttendanceV2AppealStatusEnum.StatusAdminDeal.key) {
        return '${AttendanceV2AppealStatusEnum.StatusAdminDeal.name} [${updateStatusAdminPerson?.o2NameCut()}]' ;
      }
      return '';
    }

    factory AttendanceV2Appeal.fromJson(Map<String, dynamic> json) => AttendanceV2Appeal(
        record: json["record"] == null ? null : AttendanceV2Record.fromJson(json["record"]),
        id: json["id"],
        recordId: json["recordId"],
        userId: json["userId"],
        recordDateString: json["recordDateString"],
        recordDate: json["recordDate"],
        startTime: json["startTime"] ,
        status: json["status"],
        jobId: json["jobId"],
        sendStatus: json["sendStatus"],
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
        updateStatusAdminPerson: json["updateStatusAdminPerson"] ,
    );

    Map<String, dynamic> toJson() => {
        "record": record?.toJson(),
        "id": id,
        "recordId": recordId,
        "userId": userId,
        "recordDateString": recordDateString,
        "recordDate": recordDate,
        "startTime": startTime,
        "status": status,
        "jobId": jobId,
        "sendStatus": sendStatus,
        "createTime": createTime,
        "updateTime": updateTime,
        "updateStatusAdminPerson": updateStatusAdminPerson,
    };
}
 