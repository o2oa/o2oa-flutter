class AttendanceV2Version {
  String? version;

  AttendanceV2Version({this.version});

  factory AttendanceV2Version.fromJson(Map<String, dynamic> json) =>
      AttendanceV2Version(
        version: json["version"],
      );
}
