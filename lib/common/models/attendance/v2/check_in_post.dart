
///
/// 手机打卡对象
/// 
class CheckPost {
        String? recordId; 
        String? checkInType;
        String? workPlaceId;
        bool? fieldWork; // 是否外勤打卡
        String? signDescription; //打卡说明:上班打卡，下班打卡, 可以为空.
        String? sourceDevice; //操作设备类别：Mac|Windows|IOS|Android|其他, 可以为空.
        String? description;
        String? recordAddress; //打卡地点描述, 可以为空.
        String? longitude; //经度
        String? latitude; //纬度
        String? sourceType; // "USER_CHECK", 



  CheckPost({
    this.recordId,
    this.checkInType,
    this.workPlaceId,
    this.fieldWork,
    this.signDescription,
    this.sourceDevice,
    this.description,
    this.recordAddress,
    this.longitude,
    this.latitude,
    this.sourceType
  });


Map<String, dynamic> toJson() => {
        "recordId": recordId,
        "checkInType": checkInType,
        "workPlaceId": workPlaceId,
        "fieldWork": fieldWork,
        "signDescription": signDescription,
        "sourceDevice": sourceDevice,
        "description": description,
        "recordAddress": recordAddress,
        "longitude": longitude,
        "latitude": latitude,
        "sourceType": sourceType,
    };
}


///
/// 手机打卡对象
/// 
class CheckInResponse {
        String? checkInRecordId; 
        String? checkInResult;
        String? recordDate;
       

 factory CheckInResponse.fromJson(Map<String, dynamic> json) => CheckInResponse(
        checkInRecordId: json["checkInRecordId"],
        checkInResult: json["checkInResult"],
        recordDate: json["recordDate"],
    );


  CheckInResponse({
    this.checkInRecordId,
    this.checkInResult,
    this.recordDate,
    
  });


Map<String, dynamic> toJson() => {
        "checkInRecordId": checkInRecordId,
        "checkInResult": checkInResult,
        "recordDate": recordDate,
         
    };
}