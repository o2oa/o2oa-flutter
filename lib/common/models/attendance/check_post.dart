
///
/// 手机打卡对象
/// 
class MobileCheckPost {
        String? id; 
        String? recordDateString; //打卡记录日期字符串：yyyy-mm-dd, 必须填写.
        String? signTime; //打卡时间: hh24:mi:ss, 必须填写.
        String? signDescription; //打卡说明:上班打卡，下班打卡, 可以为空.
        String? description;
        String? recordAddress; //打卡地点描述, 可以为空.
        String? longitude; //经度, 可以为空.
        String? latitude; //纬度, 可以为空.
        String? optMachineType; //操作设备类别：手机品牌|PAD|PC|其他, 可以为空.
        String? optSystemName; //操作设备类别：Mac|Windows|IOS|Android|其他, 可以为空.
        // ignore: non_constant_identifier_names
        String? checkin_type;
        bool? isExternal; // 是否外勤打卡
        String? workAddress; // 当前打卡的工作地点 外勤打卡为空


  MobileCheckPost({
    this.id,
    this.recordDateString,
    this.signTime,
    this.signDescription,
    this.description,
    this.recordAddress,
    this.longitude,
    this.latitude,
    this.optMachineType,
    this.optSystemName,
    this.checkin_type,
    this.isExternal,
    this.workAddress,
  });


Map<String, dynamic> toJson() => {
        "id": id,
        "recordDateString": recordDateString,
        "signTime": signTime,
        "signDescription": signDescription,
        "description": description,
        "recordAddress": recordAddress,
        "longitude": longitude,
        "latitude": latitude,
        "optMachineType": optMachineType,
        "optSystemName": optSystemName,
        "checkin_type": checkin_type,
        "isExternal": isExternal,
        "workAddress": workAddress,
    };



}