
class WorkplaceInfo {
    WorkplaceInfo({
        this.id,
        this.placeName,
        this.placeAlias,
        this.creator,
        this.longitude,
        this.latitude,
        this.errorRange,
        this.description,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? placeName;
    String? placeAlias;
    String? creator;
    String? longitude;
    String? latitude;
    int? errorRange;
    String? description;
    String? createTime;
    String? updateTime;

    factory WorkplaceInfo.fromJson(Map<String, dynamic> json) => WorkplaceInfo(
        id: json["id"],
        placeName: json["placeName"],
        placeAlias: json["placeAlias"],
        creator: json["creator"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        errorRange: json["errorRange"],
        description: json["description"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "placeName": placeName,
        "placeAlias": placeAlias,
        "creator": creator,
        "longitude": longitude,
        "latitude": latitude,
        "errorRange": errorRange,
        "description": description,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}