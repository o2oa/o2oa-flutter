
class IdData {
  String? id;

   IdData({this.id});
   IdData.fromJson(Map<String, dynamic> jsonMap) {
     id = jsonMap['id'];
   }
   Map<String, dynamic> toJson() => {
        "id": id,
      };
}