
import '../../values/index.dart';

class O2SearchV2Form {
    int? page;
    String? query;
    int? size;
  O2SearchV2Form({this.page, this.query, this.size = O2.o2DefaultPageSize});

  Map<String, dynamic> toJson() => {
    "page": page,
    "query": query,
    "size": size,
  };
}


class O2SearchV2PageModel {
  List<O2SearchV2Entry>? documentList;
  int? count;

  O2SearchV2PageModel({this.documentList, this.count});

  factory O2SearchV2PageModel.fromJson(Map<String, dynamic> map) =>
      O2SearchV2PageModel(
        count: map['count'],
        documentList: map['documentList'] == null
            ? []
            : List<O2SearchV2Entry>.from(
                map['documentList'].map((e) => O2SearchV2Entry.fromJson(e))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "documentList": documentList == null
            ? []
            : List<Map<String, dynamic>>.from(
                documentList!.map((e) => e.toJson()))
      };
}

class O2SearchV2Entry {
  String? id; // 业务id
  String? category; // cms processPlatform
  String? title;
  String? highlighting; // html
  String? summary; // 文字
  String? creatorPerson;
  String? creatorUnit;
  String? indexTime;
  String? createTime;
  String? updateTime;

  O2SearchV2Entry(
      {this.id,
      this.category,
      this.title,
      this.highlighting,
      this.summary,
      this.creatorPerson,
      this.creatorUnit,
      this.indexTime,
      this.createTime,
      this.updateTime});

  factory O2SearchV2Entry.fromJson(Map<String, dynamic> map) => O2SearchV2Entry(
        id: map['id'],
        category: map['category'],
        title: map['title'],
        highlighting: map['highlighting'],
        summary: map['summary'],
        creatorPerson: map['creatorPerson'],
        creatorUnit: map['creatorUnit'],
        indexTime: map['indexTime'],
        createTime: map['createTime'],
        updateTime: map['updateTime'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category": category,
        "title": title,
        "highlighting": highlighting,
        "summary": summary,
        "creatorPerson": creatorPerson,
        "creatorUnit": creatorUnit,
        "indexTime": indexTime,
        "createTime": createTime,
        "updateTime": updateTime,
      };
}
