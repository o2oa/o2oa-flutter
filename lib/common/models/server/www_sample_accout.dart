
/// 演示版本 获取演示服务器列表用的一些数据对象

class WwwGetSampleAccountPost {
  String? serverId;
  WwwGetSampleAccountPost({this.serverId});

  WwwGetSampleAccountPost.fromJson(Map<String, dynamic>? map) {
    if (map != null) {
      serverId = map['serverId'];
    }
  }

  Map<String, dynamic> toJson() => {
    "serverId": serverId
  };
}

class WwwGetSampleAccounts {
  String? password;
  List<WwwGetSampleAccount>? accountList;

  WwwGetSampleAccounts({this.password, this.accountList});

  WwwGetSampleAccounts.fromJson(Map<String, dynamic>? map) {
    if (map != null) {
      password = map['password'];
      accountList = map['accountList'] == null
          ? []
          : List<WwwGetSampleAccount>.from(
              map['accountList']!.map((e) => WwwGetSampleAccount.fromJson(e)));
    }
  }
}

class WwwGetSampleAccount {
  String? name;
  String? account;
  WwwGetSampleAccount({this.name, this.account});

  WwwGetSampleAccount.fromJson(Map<String, dynamic>? map) {
    if (map != null) {
      name = map['name'];
      account = map['account'];
    }
  }
}
