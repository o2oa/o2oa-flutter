import 'package:get/get.dart';

enum AttendanceV2RecordResultEnum {
    Normal, // ("Normal", "正常")
    Early, //("Early", "早退"),
    Late, //("Late", "迟到"),
    SeriousLate, //("SeriousLate", "严重迟到"),
    Absenteeism, //("Absenteeism", "旷工迟到"),
    NotSigned, //("NotSigned", "未打卡"),
    PreCheckIn, //("PreCheckIn", ""),
}

extension AttendanceV2RecordResultEnumExtension on AttendanceV2RecordResultEnum {
  String get name {
    switch(this) {
      case AttendanceV2RecordResultEnum.Normal:
        return 'attendance_result_Normal'.tr;
      case AttendanceV2RecordResultEnum.Early:
        return 'attendance_result_Early'.tr;
      case AttendanceV2RecordResultEnum.Late:
        return 'attendance_result_Late'.tr;
      case AttendanceV2RecordResultEnum.SeriousLate:
        return 'attendance_result_SeriousLate'.tr;
      case AttendanceV2RecordResultEnum.Absenteeism:
        return 'attendance_result_Absenteeism'.tr;
      case AttendanceV2RecordResultEnum.NotSigned:
        return 'attendance_result_NotSigned'.tr;
      case AttendanceV2RecordResultEnum.PreCheckIn:
        return 'attendance_result_PreCheckIn'.tr;
      
            
    }
  }

  String get key {
    switch(this) {
      case AttendanceV2RecordResultEnum.Normal:
        return 'Normal';
      case AttendanceV2RecordResultEnum.Early:
        return 'Early';
      case AttendanceV2RecordResultEnum.Late:
        return 'Late';
      case AttendanceV2RecordResultEnum.SeriousLate:
        return 'SeriousLate';
      case AttendanceV2RecordResultEnum.Absenteeism:
        return 'Absenteeism';
      case AttendanceV2RecordResultEnum.NotSigned:
        return 'NotSigned';
      case AttendanceV2RecordResultEnum.PreCheckIn:
        return 'PreCheckIn';
      
    }
  }
}