import 'package:get/get.dart';

enum CloudDiskFileType {
  image,
  office,
  movie,
  music,
  other,
}

extension CloudDiskFileTypeExt on CloudDiskFileType {

  String getKey() {
    switch(this) {
      case CloudDiskFileType.image:
        return 'image';
      case CloudDiskFileType.office:
        return 'office';
      case CloudDiskFileType.movie:
        return 'movie';
      case CloudDiskFileType.music:
        return 'music';
      case CloudDiskFileType.other:
        return 'other';
    }
  }

  String getName() {
    switch(this) {
      case CloudDiskFileType.image:
        return 'cloud_disk_type_image'.tr;
      case CloudDiskFileType.office:
        return 'cloud_disk_type_document'.tr;
      case CloudDiskFileType.movie:
        return 'cloud_disk_type_video'.tr;
      case CloudDiskFileType.music:
        return 'cloud_disk_type_music'.tr;
      case CloudDiskFileType.other:
        return 'cloud_disk_type_other'.tr;
    }
  }
}