import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../style/index.dart';

/// 
enum O2Skin {
  blue,
  red,
  orange,
  green,
  cyan,
  purple,
  darkgreen,
  tan,
  navy,
  gray,
}

extension O2SkinExtension on O2Skin {

  /// 皮肤颜色
  Color getColor() {
    switch(this) {
      case O2Skin.blue:return AppColor.o2SkinBlue;
      case O2Skin.red:return AppColor.o2SkinRed;
      case O2Skin.orange:return AppColor.o2SkinOrange;
      case O2Skin.green:return AppColor.o2SkinGreen;
      case O2Skin.cyan:return AppColor.o2SkinCyan;
      case O2Skin.purple:return AppColor.o2SkinPurple;
      case O2Skin.darkgreen:return AppColor.o2SkinDarkGreen;
      case O2Skin.tan:return AppColor.o2SkinTan;
      case O2Skin.navy:return AppColor.o2SkinNavy;
      case O2Skin.gray:return AppColor.o2SkinGray;
      default: return AppColor.o2SkinBlue;
    }
  }

  Color getSecondaryColor() {
    switch(this) {
      case O2Skin.blue:return AppColor.o2SkinBlueSecondary;
      case O2Skin.red:return AppColor.o2SkinRedSecondary;
      case O2Skin.orange:return AppColor.o2SkinOrangeSecondary;
      case O2Skin.green:return AppColor.o2SkinGreenSecondary;
      case O2Skin.cyan:return AppColor.o2SkinCyanSecondary;
      case O2Skin.purple:return AppColor.o2SkinPurpleSecondary;
      case O2Skin.darkgreen:return AppColor.o2SkinDarkGreenSecondary;
      case O2Skin.tan:return AppColor.o2SkinTanSecondary;
      case O2Skin.navy:return AppColor.o2SkinNavySecondary;
      case O2Skin.gray:return AppColor.o2SkinGraySecondary;
      default: return AppColor.o2SkinBlueSecondary;
    }
  }

  String get displayName {
    switch(this) {
      case O2Skin.blue:return 'settings_skin_color_blue'.tr;
      case O2Skin.red:return 'settings_skin_color_red'.tr;
      case O2Skin.orange:return 'settings_skin_color_orange'.tr;
      case O2Skin.green:return 'settings_skin_color_green'.tr;
      case O2Skin.cyan:return 'settings_skin_color_cyan'.tr;
      case O2Skin.purple:return 'settings_skin_color_purple'.tr;
      case O2Skin.darkgreen:return 'settings_skin_color_darkgreen'.tr;
      case O2Skin.tan:return 'settings_skin_color_tan'.tr;
      case O2Skin.navy:return 'settings_skin_color_navy'.tr;
      case O2Skin.gray:return 'settings_skin_color_gray'.tr;
      default: return 'settings_skin_color_blue'.tr;
    }
  }


}