

///
/// 字符串转 枚举类
/// 
MeetingRoomDevice toMeetingRoomDevice(String key) {
  switch(key) {
    case 'projector':
    return MeetingRoomDevice.PROJECTOR;
    case 'board':
    return MeetingRoomDevice.BOARD;
    case 'tvset':
    return MeetingRoomDevice.TVSET;
    case 'tv':
    return MeetingRoomDevice.TV;
    case 'camera':
    return MeetingRoomDevice.CAMERA;
    case 'wifi':
    return MeetingRoomDevice.WIFI;
    case 'phone':
    return MeetingRoomDevice.PHONE;
    default:
    return MeetingRoomDevice.PROJECTOR;
  }
}

enum MeetingRoomDevice {
    PROJECTOR, //("projector", "投影仪", R.mipmap.icon__projector),
    BOARD, //("board", "白板", R.mipmap.icon__board),
    TVSET, //("tvset", "电视", R.mipmap.icon__tv),
    TV, //("tv", "视频会议", R.mipmap.icon__video),
    CAMERA, //("camera", "摄像头", R.mipmap.icon__camera),
    WIFI, //("wifi", "WIFI", R.mipmap.icon__wifi),
    PHONE, //("phone", "电话会议", R.mipmap.icon__phone);
}


extension MeetingRoomDeviceExtension on MeetingRoomDevice {
  String getName() {
      switch(this) {
        case MeetingRoomDevice.PROJECTOR:
        return '投影仪';
        case MeetingRoomDevice.BOARD:
        return '白板';
        case MeetingRoomDevice.TVSET:
        return '电视';
        case MeetingRoomDevice.TV:
        return '视频会议';
        case MeetingRoomDevice.CAMERA:
        return '摄像头';
        case MeetingRoomDevice.WIFI:
        return 'WIFI';
        case MeetingRoomDevice.PHONE:
        return '电话会议';
      }
  }
  String getAssetName() {
    switch(this) {
        case MeetingRoomDevice.PROJECTOR:
        return 'meeting_room_projector.png';
        case MeetingRoomDevice.BOARD:
        return 'meeting_room_board.png';
        case MeetingRoomDevice.TVSET:
        return 'meeting_room_tv.png';
        case MeetingRoomDevice.TV:
        return 'meeting_room_video.png';
        case MeetingRoomDevice.CAMERA:
        return 'meeting_room_camera.png';
        case MeetingRoomDevice.WIFI:
        return 'meeting_room_wifi.png';
        case MeetingRoomDevice.PHONE:
        return 'meeting_room_phone.png';
      }
  }
}