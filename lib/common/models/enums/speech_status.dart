enum SpeechStatus {
  idle, // 空闲
  speaking, // 说话中
  thinking // 等待中
}