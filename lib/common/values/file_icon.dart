

class FileIcon {

  static String getFileIconAssetsNameByExtension(String extension) {
        if (extension.isEmpty) {
          return "icon_file_unkown.png";
        }
        extension = extension.toLowerCase();
        if (extension.startsWith(".")) {
          extension = extension.substring(1, extension.length);
        }
        switch (extension){
            case "jpg":
            case "jpeg":
                return "icon_file_jpeg.png";
            case "gif":
                return "icon_file_gif.png";
            case "png":
                return "icon_file_png.png";
            case "tiff":
                return "icon_file_tiff.png";
            case "bmp":
            case "webp":
                return "icon_file_img.png";
            case "ogg":
            case "mp3":
            case "wav":
            case "wma":
                return "icon_file_mp3.png";
            case "mp4":
                return "icon_file_mp4.png";
            case "avi":
                return "icon_file_avi.png";
            case "mov":
            case "rm":
            case "mkv":
                return "icon_file_rm.png";
            case "doc":
            case "docx":
                return "icon_file_word.png";
            case "xls":
            case "xlsx":
                return "icon_file_excel.png";
            case "ppt":
            case "pptx":
                return "icon_file_ppt.png";
            case "html":
                return "icon_file_html.png";
            case "pdf":
                return "icon_file_pdf.png";
            case "txt":
            case "json":
                return "icon_file_txt.png";
            case "zip":
                return "icon_file_zip.png";
            case "rar":
                return "icon_file_rar.png";
            case "7z":
                return "icon_file_arch.png";
            case "ai":
                return "icon_file_ai.png";
            case "att":
                return "icon_file_att.png";
            case "au":
                return "icon_file_au.png";
            case "cad":
                return "icon_file_cad.png";
            case "cdr":
                return "icon_file_cdr.png";
            case "eps":
                return "icon_file_eps.png";
            case "exe":
                return "icon_file_exe.png";
            case "iso":
                return "icon_file_iso.png";
            case "link":
                return "icon_file_link.png";
            case "swf":
                return "icon_file_flash.png";
            case "psd":
                return "icon_file_psd.png";
            case "tmp":
                return "icon_file_tmp.png";
            default:
                return "icon_file_unkown.png";
        }
  }
}