
import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';

import '../models/index.dart';
import '../services/index.dart';
import '../utils/index.dart';
import 'x_organization_assemble_personal.dart';

///
/// 会议管理服务
/// 
class MeetingAssembleService extends GetxService {
  
  static MeetingAssembleService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_meeting_assemble_control) ?? '';
  }

  /// 会议管理配置文件读取
  Future<MeetingConfigModel?> config() async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/config/system/config');
      MeetingConfigModel info = MeetingConfigModel.fromJson(response.data);
      // 存储配置 其他页面需要使用
      SharedPreferenceService.to.putString(SharedPreferenceService.meetingConfigKey, json.encode(info.toJson()));
      return info;
    } catch (err, stackTrace) {
      OLogger.e('新配置文件读取失败', err, stackTrace);
      return OrganizationPersonalService.to.meetingConfig();
    }
  }

  ///
  /// 会议保存
  ///
  Future<IdData?> saveMeeting(MeetingInfoModel meeting) async {
    try {
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/meeting', meeting.toJson());
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('会议保存失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 更新会议
  ///
  Future<IdData?> updateMeeting(String id, MeetingInfoModel meeting) async {
    try {
      ApiResponse response = await O2HttpClient.instance.put('${baseUrl()}jaxrs/meeting/$id', meeting.toJson());
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('更新会议失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 查询会议
  ///
  Future<MeetingInfoModel?> getMeeting(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/$id');
      return MeetingInfoModel.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e(' 查询会议失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 保存会议材料
  /// @param summary 是否会议纪要
  /// 
  Future<IdData?> uploadMeetingAttachment(String meetingId, File file, {bool summary = false}) async {
    try {
      ApiResponse response = await O2HttpClient.instance.postUploadFile('${baseUrl()}jaxrs/attachment/meeting/$meetingId/upload/$summary', file);
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('保存会议材料失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 删除会议材料
  ///
  Future<IdData?> deleteMeetingAttachment(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.delete('${baseUrl()}jaxrs/attachment/$id');
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('删除会议材料失败', err, stackTrace);
    }
    return null;
  } 
  ///
  /// 获取会议材料对象
  ///
  Future<MeetingAttachmentModel?> getMeetingAttachment(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/attachment/$id');
      MeetingAttachmentModel info = MeetingAttachmentModel.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('获取会议材料对象失败', err, stackTrace);
    }
    return null;
  } 
  ///
  /// 接收会议邀请
  ///
  Future<IdData?> acceptMeetingInvite(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/$id/accept');
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('接收会议邀请失败', err, stackTrace);
    }
    return null;
  }
  ///
  /// 拒绝会议邀请
  ///
  Future<IdData?> rejectMeetingInvite(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/$id/reject');
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('拒绝会议邀请失败', err, stackTrace);
    }
    return null;
  }
  ///
  /// 删除会议
  ///
  Future<IdData?> deleteMeeting(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.delete('${baseUrl()}jaxrs/meeting/$id');
      IdData info = IdData.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('删除会议失败', err, stackTrace);
    }
    return null;
  }


  ///
  /// 会议签到
  ///
  Future<MeetingCheckInModel?> meetingCheckIn(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/$id/checkin');
      MeetingCheckInModel info = MeetingCheckInModel.fromJson(response.data);
      return info;
    } catch (err, stackTrace) {
      OLogger.e('会议签到失败', err, stackTrace);
    }
    return null;
  }


  ///
  /// 列示我参与的指定日期当月的会议，或者被邀请，或者是申请人,或者是审核人，管理员可以看到所有.
  /// 
  Future<List<MeetingInfoModel>?> meetingListOnMonth(String year, String month) async {
     try {
       
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/list/year/$year/month/$month');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => MeetingInfoModel.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询月份会议列表失败', err, stackTrace);
    }
    return null;
  }



  ///
  /// 根据时间段查询会议室列表
  /// @param startTime 格式： yyyy-MM-dd HH:mm
  /// @param completedTime 格式： yyyy-MM-dd HH:mm
  /// 
  Future<List<MeetingRoomBuildModel>?> buildListWithStartCompleted(String startTime, String completedTime) async {
     try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/building/list/start/$startTime/completed/$completedTime');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => MeetingRoomBuildModel.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询会议室列表失败', err, stackTrace);
    }
    return null;
  }

  /// 会议邀请列表
  Future<List<MeetingInfoModel>?> meetingListByAccept() async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/meeting/list/wait/accept');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => MeetingInfoModel.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询会议邀请列表失败', err, stackTrace);
    }
    return null;
  }

}