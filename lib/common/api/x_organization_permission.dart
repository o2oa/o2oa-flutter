import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

/// 通讯录权限模块
/// 需要到应用市场下载安装
class OrganizationPermissionService extends GetxService {
  static OrganizationPermissionService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
            O2DistributeModuleEnum.x_organizationPermission) ??
        '';
  }

  ///  获取通讯录权限
  Future<ContactPermission?> getPermissionViewInfo(String view) async {
    try {
      ApiResponse response = await O2HttpClient.instance
          .get('${baseUrl()}jaxrs/permission/view/$view');
      ContactPermission c = ContactPermission.fromJson(response.data);
      return c;
    } catch (err, stackTrace) {
      OLogger.e('查询通讯录权限失败', err, stackTrace);
    }
    return null;
  }
}
