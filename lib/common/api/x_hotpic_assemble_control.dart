

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

class HotpicAssembleControlService extends GetxService {
  
  static HotpicAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_hotpic_assemble_control) ?? '';
  }

  ///
  /// 热点图片数据列表
  /// 
  Future<List<HotpicData>?> findHotPictureList() async {
     try {
      Map<String, dynamic> data = {};
      const page = 1; // 第一页
      const count = 5; // 5条数据
      ApiResponse response =
      await O2HttpClient.instance.put('${baseUrl()}jaxrs/user/hotpic/filter/list/page/$page/count/$count', data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => HotpicData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询热点图片列表失败', err, stackTrace);
    }
    return null;
  }
}