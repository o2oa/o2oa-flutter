

import 'dart:io';

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

class CmsAssembleControlService extends GetxService {
  
  static CmsAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_cms_assemble_control) ?? '';
  }

///
  /// 附件下载地址
  /// 
  String downloadAttachmentUrl(String attachmentId) {
    return "${baseUrl()}jaxrs/fileinfo/download/document/$attachmentId/stream";
  }

  ///
  ///  
  /// 根据分类id查询草稿列表
  /// @param categoryId 分类id 
  ///
  Future<List<DocumentData>?> listDocumentDraft(String categoryId, {int limit = O2.o2DefaultPageSize}) async {
    try {
      Map<String, dynamic> data = {
        "categoryIdList": [categoryId]
      };
      ApiResponse response =
      await O2HttpClient.instance.put('${baseUrl()}jaxrs/document/draft/list/${O2.o2DefaultPageFirstKey}/next/${O2.o2DefaultPageSize}', data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => DocumentData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据分类id查询草稿列表失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 分页查信息列表
  /// 首页使用
  /// @param page 
  ///
  Future<List<DocumentData>?> getCmsDocumentListByPage(int page, {int limit = O2.o2DefaultPageSize, Map<String, dynamic>? body}) async {
    try {
      Map<String, dynamic> data = {};
      if (body != null) {
        data = body;
      }
      data["justData"] = true; // 只返回数据 没有 count  这样速度会快点
      ApiResponse response = await O2HttpClient.instance.put('${baseUrl()}jaxrs/document/filter/list/$page/size/$limit', data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => DocumentData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询cms文档列表失败', err, stackTrace);
    }
    return null;
  }


  
  ///
  /// 查询当前用户能够查看的应用列表
  /// 
  Future<List<CmsAppData>?> listAppWithCategoryUserCanView() async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/appinfo/list/user/view');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => CmsAppData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询当前用户能够查看的应用列表失败', err, stackTrace);
    }
    return null;
  }
  
  ///
  /// 查询当前用户能够发布的应用列表
  /// 
  Future<List<CmsAppData>?> listAppWithCategoryUserCanPublish() async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/appinfo/list/user/publish');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => CmsAppData.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询当前用户能够发布的应用列表失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 查询应用对象
  /// 
  Future<CmsAppData?> getApp(String flag) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/appinfo/$flag');
      return CmsAppData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询应用对象失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 查询应用对象 
  /// 有返回可发布的分类列表 wrapOutCategoryList
  /// 
  Future<CmsAppData?> getAppCanPublishCategories(String appId) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/appinfo/get/user/publish/$appId');
      return CmsAppData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询应用对象失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 查询分类对象
  /// 
  Future<CmsCategoryData?> getCategory(String flag) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/categoryinfo/$flag');
      return CmsCategoryData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询应用对象失败', err, stackTrace);
    }
    return null;
  } 
  
  ///
  /// 创建文档
  /// 
  Future<IdData?> documentPost(Map<String, dynamic> body) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/document', body);
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('创建文档失败', err, stackTrace);
    }
    return null;
  }


  ///
  /// 上传附件
  /// 
  Future<IdData?> uploadAttachment(String docId, String site, File file) async {
    try {
      ApiResponse response = await O2HttpClient.instance.postUploadFile('${baseUrl()}jaxrs/fileinfo/upload/document/$docId', file, body: {'site': site});
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('上传附件失败', err, stackTrace);
    }
    return null;
  }
  ///
  /// 替换附件
  /// 
  Future<IdData?> replaceAttachment(String docId, String attachmentId, File file) async {
    try {
      ApiResponse response = await O2HttpClient.instance.postUploadFile('${baseUrl()}jaxrs/fileinfo/update/document/$docId/attachment/$attachmentId', file);
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('替换附件失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 根据docId查询附件对象
  /// 
  Future<DocAttachmentInfo?> getAttachmentInfoByDocId(String docId, String attId) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/fileinfo/$attId/document/$docId');
      return  DocAttachmentInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询附件对象失败 ， docId $docId, attId: $attId', err, stackTrace);
    }
    return null;
  }
}