 
import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
 


///
///组织人员服务
///
class OrganizationAssembleExpressService extends GetxService {
  
  static OrganizationAssembleExpressService get to => Get.find();
  //
  
  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_organization_assemble_express) ?? '';
  }
 


  ///
  /// 人员对象查询
  /// 
  Future<O2Person?> person(String id) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/person/$id');
      return O2Person.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('人员对象查询失败, id:$id ', err, stackTrace);
    }
    return null;
  }
 
    ///
    /// 批量获取person的DN
    /// @param body {"personList":["4b9df3c7-c0fa-4b64-9f98-034da8bb8f92"]}
    ///
    Future<SearchPersonList?> searchPersonDNList(List<String> personList) async {
    try {
      final body = {
        'personList': personList
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/person/list', body);
      return SearchPersonList.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('批量获取person的DN失败', err, stackTrace);
    }
    return null;
  }

 
    ///
    /// 根据职务、组织查询身份列表
    /// @param body {"nameList":["测试职务"],"unit":"人力资源部@2812170000@U"}
    ///
    Future<List<O2Identity>?> identityListByUnitAndDuty(List<String> dutyList, String parentUnit) async {
    try {
      final body = {
        'nameList': dutyList,
        'unit': parentUnit
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unitduty/list/identity/unit/name/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((e) => O2Identity.fromJson(e)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据职务、组织查询身份列表失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 根据身份查询组织
  /// 
  Future<List<O2Unit>?> unitListWithIdentities(List<String> identities) async {
    try {
      final body = {
        'identityList': identities
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/list/identity/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => O2Unit.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据身份查询组织失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 根据身份查询对应等级的组织
  /// 
  Future<O2Unit?> unitByIdAndLevel(String identity, int level) async {
    try {
      final body = {
        'identity': identity,
        'level': level
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/identity/level/object', body);
      return O2Unit.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('根据身份查询对应等级的组织失败, identity:$identity, level: $level', err, stackTrace);
    }
    return null;
  }

  ///
  /// 根据人员所属的组织
  /// 
  Future<List<O2Unit>?> listUnitByPerson(String person) async {
    try {
       final body = {
        'personList': [person],
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/list/person/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Unit.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据人员所属的组织织失败, person:$person,', err, stackTrace);
    }
    return null;
  }

  ///
  /// 身份换人员对象
  /// 
  Future<List<O2Person>?> listPersonWithIdentities(List<String> identities) async {
    	
      try {
      final body = {
        'identityList': identities
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/person/list/identity/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Person.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据身份换人员对象失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 人员换身份对象
  /// 
  Future<List<O2Identity>?> listIdentityWithPersons(List<String> persons) async {
      try {
      final body = {
        'personList': persons
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/identity/list/person/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Identity.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('根据人员换身份对象失败', err, stackTrace);
    }
    return null;
  }
  
   ///
  /// 获取身份对象
  /// 
  Future<List<O2Identity>?> listIdentityObjects(List<String> identities) async {
      try {
      final body = {
        'identityList': identities
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/identity/list/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Identity.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取身份对象失败', err, stackTrace);
    }
    return null;
  }
  ///
  /// 获取组织对象
  /// 
  Future<List<O2Unit>?> listUnitObjects(List<String> units) async {
      try {
      final body = {
        'unitList': units
      };
      ApiResponse response =
      await O2HttpClient.instance.post('${baseUrl()}jaxrs/unit/list/object', body);
      var list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Unit.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取组织对象失败', err, stackTrace);
    }
    return null;
  }
  /// 获取群组对象
  Future<List<O2Group>?> listGroupObjects(List<String> groupIds) async {
    try {
      final body = {
        'groupList':  groupIds
      };
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/group/list/object', body);
      final list = response.data == null ? [] : response.data as List;
      return list.map((p) => O2Group.fromJson(p)).toList();
    } catch (err, stackTrace) {
      OLogger.e('获取群组对象失败', err, stackTrace);
    }
    return null;
  }
}
