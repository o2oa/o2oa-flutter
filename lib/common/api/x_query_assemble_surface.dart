import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

/// 查询服务
class QueryAssembleSurfaceService extends GetxService {
  
  static QueryAssembleSurfaceService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_query_assemble_surface) ?? '';
  }

  /// 全文搜索接口 
  Future<O2SearchV2PageModel?> searchV2(O2SearchV2Form form) async {
    try {
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}jaxrs/search', form.toJson());
      return  O2SearchV2PageModel.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('全文搜索接口查询失败', err, stackTrace);
    }
    return null;
  }
}