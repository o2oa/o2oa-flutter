import 'dart:io';

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

///
/// 論壇服務
///
class BBSAssembleControlService extends GetxService {
  static BBSAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_bbs_assemble_control) ??
        '';
  }


  ///
  /// 论坛附件下载地址
  /// 
  String getBBSAttachmentURL( String attachmentId) {
    return "${baseUrl()}jaxrs/attachment/download/$attachmentId/stream/true";
  }
   
  /// 板块权限查询.
  Future<SectionPermissionInfo?>  sectionPermission(String sectionId) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/permission/section/$sectionId');
      return SectionPermissionInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询板块权限失败', err, stackTrace);
    }
    return null;
  }  

  /// 主题帖子权限查询.
  Future<SubjectPermissionInfo?> subjectPermission(String subjectId) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/permission/subject/$subjectId');
      return SubjectPermissionInfo.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询主题帖子权限失败', err, stackTrace);
    }
    return null;
  }

  /// 板块对象查询
  Future<SectionInfoList?> getSectionInfo(String sectionId) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/section/$sectionId');
      return SectionInfoList.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询板块失败', err, stackTrace);
    }
    return null;
  }

  /// 获取登录者可以访问到的所有ForumInfo的信息列表.
  Future<List<ForumInfo>?> mobileFormuInfoAllList() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/mobile/view/all');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => ForumInfo.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询ForumInfo列表失败', err, stackTrace);
    }
    return null;
  }
 

  ///
  /// 分页查询主题列表
  ///
  Future<List<SubjectInfo>?> subjectListByPage(String sectionId, int page, {int limit = O2.o2DefaultPageSize, bool withTopSubject = true}) async {
    try {
       Map<String, dynamic> data = {
        "withTopSubject": withTopSubject,
        "sectionId": sectionId
      };
      ApiResponse response = await O2HttpClient.instance
          .put('${baseUrl()}jaxrs/subject/filter/list/page/$page/count/$limit', data);
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => SubjectInfo.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询主题列表失败', err, stackTrace);
    }
    return null;
  }

/// 主题帖子附件列表
  Future<List<SubjectAttachment>?> subjectAttachmentList(String subjectId) async {
    try {
      ApiResponse response = await O2HttpClient.instance.get('${baseUrl()}jaxrs/subjectattach/list/subject/$subjectId');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => SubjectAttachment.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询帖子附件列表失败', err, stackTrace);
    }
    return null;
  }

  /// 附件下载地址
  String subjectAttachmentDownloadUrl(String id) {
    return "${baseUrl()}jaxrs/attachment/download/$id";
  }
   ///
  /// 上传文件.
  ///
  Future<IdData?> uploadAttachment(String subjectId, File file) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.postUploadFile('${baseUrl()}jaxrs/attachment/upload/subject/$subjectId', file, body: {'site': subjectId});
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('上传附件失败', err, stackTrace);
    }
    return null;
  }

  /// 发帖
  Future<IdData?> subjectPublish(SubjectInfo post) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/user/subject', post.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('发帖失败', err, stackTrace);
    }
    return null;
  }
  /// 删除帖子.
  Future<IdData?> deleteSubject(String subjectId) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.delete('${baseUrl()}jaxrs/user/subject/$subjectId');
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('删除帖子失败', err, stackTrace);
    }
    return null;
  }



///
/// 回复帖子
/// 
  Future<IdData?> replaySubject(ReplyFormJson post) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.post('${baseUrl()}jaxrs/user/reply', post.toJson());
      return IdData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('回复帖子失败', err, stackTrace);
    }
    return null;
  }

   ///
  /// 回复对象.
  ///
  Future<SubjectReplyInfoJson?> getReplyInfo(String replyId) async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/reply/$replyId');
      return SubjectReplyInfoJson.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('查询回复失败', err, stackTrace);
    }
    return null;
  }



}
