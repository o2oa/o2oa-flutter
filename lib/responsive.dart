
import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {

  final Widget? mobile;
  final Widget? tablet;
  final Widget? desktop;

  const Responsive({
    Key? key, 
    @required this.mobile, 
    this.tablet, 
    @required this.desktop}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    if (size.width >= 1100 && desktop != null) {
      return desktop!;
    } else if (size.width >= 850 && tablet != null) {
      return tablet!;
    } else if (mobile != null) {
      return mobile!;
    }
    return Container();
  }




  static bool isMobile(BuildContext context) => MediaQuery.of(context).size.width < 850;

  static bool isTablet(BuildContext context) => MediaQuery.of(context).size.width < 1100 && MediaQuery.of(context).size.width >= 850;

  static bool isDesktop(BuildContext context) => MediaQuery.of(context).size.width >= 1100;
  
  
}