import 'package:get/get.dart';

import 'controller.dart';

class BbsBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<BbsController>(() => BbsController())];
  }
}
