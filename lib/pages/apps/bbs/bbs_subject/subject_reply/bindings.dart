import 'package:get/get.dart';

import 'controller.dart';

class SubjectReplyBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SubjectReplyController>(() => SubjectReplyController())];
  }
}
