import 'package:get/get.dart';

import '../../../../../common/models/bbs/index.dart';

class SubjectReplyState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

 final _replyHintObs = "".obs;
  set replyHintObs(value) => _replyHintObs.value = value;
  get replyHintObs => _replyHintObs.value;
  // 上传图片列表
  RxMap<String, ReplyImageItem> replyImageMap = <String, ReplyImageItem>{}.obs;

}
