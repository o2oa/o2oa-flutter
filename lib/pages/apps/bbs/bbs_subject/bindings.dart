import 'package:get/get.dart';

import 'controller.dart';

class BbsSubjectBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<BbsSubjectController>(() => BbsSubjectController())];
  }
}
