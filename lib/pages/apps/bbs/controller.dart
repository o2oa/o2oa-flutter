import 'dart:convert';
import 'dart:typed_data';

import 'package:get/get.dart';
import '../../../common/api/index.dart';
import '../../../common/models/index.dart';
import '../../../common/utils/index.dart';
import '../bbs/bbs_section/view.dart';

import 'index.dart';

class BbsController extends GetxController {
  BbsController();

  final state = BbsState();
  
  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.title = Get.parameters['displayName'] ?? "app_name_bbs".tr;
    loadForumlist();
    super.onReady();
  }

  

 Future<void> loadForumlist() async {
    var list = await BBSAssembleControlService.to.mobileFormuInfoAllList();
    if (list != null && list.isNotEmpty) {
      state.forumList.clear();
      state.forumList.addAll(list);
    }
  }

  Future<void> clickSection(SectionInfoList section) async {
    OLogger.d("点击了section ${section.sectionName}");
    await BbsSectionPage.open(section);
    loadForumlist();
  }

 Uint8List? iconBase64(String? icon) {
    if (icon == null) {
      return null;
    }
    return base64Decode(icon);
  }

}
