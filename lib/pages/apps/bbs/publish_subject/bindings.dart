import 'package:get/get.dart';

import 'controller.dart';

class PublishSubjectBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<PublishSubjectController>(() => PublishSubjectController())];
  }
}
