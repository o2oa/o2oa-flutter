import 'package:get/get.dart';

import '../../../../common/models/bbs/index.dart';

class PublishSubjectState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final _category = "".obs;
  set category(value) => _category.value = value;
  get category => _category.value;


  final _subjectType = "".obs;
  set subjectType(value) => _subjectType.value = value;
  get subjectType => _subjectType.value;

  // 上传图片列表
  RxMap<String, ReplyImageItem> imageMap = <String, ReplyImageItem>{}.obs;

   /// 是否有更多属性填写
  final _openMore = false.obs;
  set openMore(bool value) => _openMore.value = value;
  bool get openMore => _openMore.value;

  /// 是否匿名发帖
  final _anonymousSubject = false.obs;
  set anonymousSubject(bool value) => _anonymousSubject.value = value;
  bool get anonymousSubject => _anonymousSubject.value;

}
