import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../cloud_disk_helpler.dart';
import 'index.dart';

class UploadHistoryListController extends GetxController {
  UploadHistoryListController();

  final state = UploadHistoryListState();
 
  
  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.uploadList.addAll(CloudDiskHelper().getUploadList());
    CloudDiskHelper().setCloudDiskUploadProgressListener((fileinfo) => updateUploadFileProgress(fileinfo));
    super.onReady();
  }

  @override
  void onClose() {
    CloudDiskHelper().clearCloudDiskUploadProgressListener();
    super.onClose();
  }
 
  updateUploadFileProgress(CloudDiskUploadFileInfo fileinfo) {
    final index = state.uploadList.indexWhere((element) => element.id == fileinfo.id);
    state.uploadList[index] = fileinfo;
  }
}
