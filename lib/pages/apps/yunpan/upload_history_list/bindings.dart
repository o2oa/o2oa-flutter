import 'package:get/get.dart';

import 'controller.dart';

class UploadHistoryListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<UploadHistoryListController>(() => UploadHistoryListController())];
  }
}
