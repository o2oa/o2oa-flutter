import 'package:get/get.dart';

import 'controller.dart';

class ShareListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ShareListController>(() => ShareListController())];
  }
}
