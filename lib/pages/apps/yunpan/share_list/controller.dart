import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/widgets/index.dart';
import '../folder_picker/index.dart';
import 'index.dart';

class ShareListController extends GetxController {
  ShareListController();

  final state = ShareListState();
 
 
  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    loadShareList();
    super.onReady();
  }

  loadShareList() async {
    state.shareList.clear();
    if (state.shareType.value == CloudDiskShareListType.shareTome) {
      final result = await FileAssembleService.to.listShareToMe();
      if (result != null) {
        state.shareList.addAll(result);
      }
    } else {
      final result = await FileAssembleService.to.listMyShare();
      if (result != null) {
        state.shareList.addAll(result);
      }
    }
  }
 
  void changeType(CloudDiskShareListType type) {
    state.shareType.value = type;
    loadShareList();
  }

  void onItemClick(ShareFileInfo info) {
    final context = Get.context;
    if (context == null) {
      return;
    }
    List<SheetMenuData> menus = [];
    if (state.shareType.value == CloudDiskShareListType.shareTome) {
      menus.add(SheetMenuData('cloud_disk_share_action_save_to_pan'.tr, () => saveToPan(info)));
      menus.add(SheetMenuData('cloud_disk_share_action_shield'.tr, () => shieldShareFile(info)));
    } else {
       menus.add(SheetMenuData('cloud_disk_share_action_cancel_share'.tr, () => deleteShareFile(info)));
    }
    O2UI.showBottomSheetWithMenuData(context, menus);
  }

  void saveToPan(ShareFileInfo info) async {
    OLogger.d('保存到网盘');
    if (info.id?.isNotEmpty == true && info.fileId?.isNotEmpty == true) {
      final folder = await FolderPickerPage.open();
      if (folder != null && folder is FolderInfo) {
        await FileAssembleService.to.shareSaveToPan(info.id!, info.fileId!, folder.id!);
        loadShareList();
      }
    }
  }

  void shieldShareFile(ShareFileInfo info) async {
    OLogger.d('屏蔽分享');
    if (info.id?.isNotEmpty == true){
      await FileAssembleService.to.shareFileShield(info.id!);
      loadShareList();
    }
    
  }
  void deleteShareFile(ShareFileInfo info) async {
    OLogger.d('取消分享');
    if (info.id?.isNotEmpty == true){
      await FileAssembleService.to.shareFileDelete(info.id!);
      loadShareList();
    }
  }
}
