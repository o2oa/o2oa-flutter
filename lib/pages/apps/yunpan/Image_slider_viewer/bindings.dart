import 'package:get/get.dart';

import 'controller.dart';

class ImageSliderViewerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ImageSliderViewerController>(() => ImageSliderViewerController())];
  }
}
