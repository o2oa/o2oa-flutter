import 'package:get/get.dart';

import 'controller.dart';

class EnterpriseFileListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<EnterpriseFileListController>(() => EnterpriseFileListController())];
  }
}
