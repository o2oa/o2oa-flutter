import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class FileTypeListState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  Rx<CloudDiskFileType> type = Rx<CloudDiskFileType>(CloudDiskFileType.image);

   RxList<FileInfo> fileList = <FileInfo>[].obs;

// 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;
}
