import 'package:get/get.dart';

import 'controller.dart';

class FileTypeListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<FileTypeListController>(() => FileTypeListController())];
  }
}
