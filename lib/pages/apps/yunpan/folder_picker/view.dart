import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';

import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class FolderPickerPage extends GetView<FolderPickerController> {
  const FolderPickerPage({Key? key}) : super(key: key);

  /// 个人网盘文件夹选择器
  static Future<dynamic> open() async {
    return await Get.toNamed(O2OARoutes.appCloudDiskFolderPicker);
  }

  /// 企业网盘 文件夹选择
  ///
  /// [zoneId] 文件共享区 id [zoneName] 文件共享区名称
  static Future<dynamic> openV3(String zoneId, String zoneName) async {
    return await Get.toNamed(O2OARoutes.appCloudDiskFolderPicker, arguments: {
      "type": FolderPickerType.zone,
      "topId": zoneId,
      "topName": zoneName
    });
  }

  // 主视图
  Widget _buildView(BuildContext context) {
    return Column(
      children: [
        breadcrumbView(context),
        const Divider(height: 1, color: AppColor.dividerColor),
        const SizedBox(height: 10),
        Obx(() => Visibility(
              visible: controller.state.breadcrumbBeans.length == 1,
              child: ListTile(
                onTap: () => controller.submitTopFolder(),
                leading: const AssetsImageView('icon_folder.png',
                    width: 40, height: 40),
                title: Text('cloud_disk_folder_picker_top'.tr),
              ),
            )),
        Expanded(
            flex: 1,
            child: Obx(() => ListView.builder(
                  itemCount: controller.state.cloudDiskItemList.length,
                  itemBuilder: ((context, index) {
                    var item = controller.state.cloudDiskItemList[index];
                    var icon = 'icon_folder.png'; // 文件夹
                    var updateTime = '';
                    if (!item.isFolder) {
                      icon = FileIcon.getFileIconAssetsNameByExtension(
                          item.file?.extension ?? '');
                      String len =
                          (item.file?.length ?? 0).friendlyFileLength();
                      updateTime = '${item.file?.updateTime ?? ''} $len';
                    } else {
                      updateTime = item.folder?.updateTime ?? '';
                    }
                    return ListTile(
                        onTap: () => controller.onItemClick(item),
                        leading: AssetsImageView(icon, width: 40, height: 40),
                        title: Text(item.displayName),
                        subtitle: Text(updateTime),
                        trailing: Obx(() {
                          var checkedList =
                              controller.state.cloudDiskItemCheckedList;
                          var checkedItem = checkedList
                              .firstWhereOrNull((element) => element == item);
                          return Checkbox(
                            value: checkedItem != null,
                            onChanged: (value) =>
                                controller.onItemCheckedChange(item, value),
                          );
                        }));
                  }),
                ))),
      ],
    );
  }

  Widget breadcrumbView(BuildContext context) {
    return SizedBox(
      height: 48,
      width: double.infinity,
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(
                children: controller.state.breadcrumbBeans.map((element) {
              var isLast = (element.folderId ==
                  controller.state.breadcrumbBeans.last.folderId);
              if (isLast) {
                return TextButton(
                    onPressed: () {},
                    child: Text(element.displayName,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                            color: Theme.of(context).colorScheme.primary)));
              }
              return Row(mainAxisSize: MainAxisSize.min, children: [
                TextButton(
                    onPressed: () => controller.clickBreadcrumb(element),
                    child: Text(element.displayName,
                        style: Theme.of(context).textTheme.bodyLarge)),
                Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child:
                        Text(">", style: Theme.of(context).textTheme.bodyLarge))
              ]);
            }).toList()),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FolderPickerController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text("cloud_disk_folder_picker_title".tr),
            actions: [
              TextButton(
                  onPressed: () => controller.submitPicker(),
                  child: Text('positive'.tr,
                      style: AppTheme.whitePrimaryTextStyle))
            ],
          ),
          body: SafeArea(
            child: _buildView(context),
          ),
        );
      },
    );
  }
}
