import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/values/index.dart';
import 'index.dart';

class FolderPickerController extends GetxController {
  FolderPickerController();

  final state = FolderPickerState();
  int folderLevel = 0; // 组织层级
  FolderPickerType pickerType = FolderPickerType.person; // 选择类型

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    _initBreadcrumb(); // 默认的
    final map = Get.arguments;
    if (map != null && map["type"] != null && map["type"] is FolderPickerType) {
      pickerType = map["type"] as FolderPickerType;
      if (pickerType == FolderPickerType.zone) {
        if (map["topId"] == null || map["topName"] == null) {
          OLogger.e('参数不正确！');
          Get.back();
          return;
        }
        final topId = map["topId"] as String;
        final topName = map["topName"] as String;
        folderLevel = 0;
        state.breadcrumbBeans.clear();
        state.breadcrumbBeans
            .add(FolderBreadcrumbBean(topName, topId, folderLevel));
      }
    }

    refreshData();
    super.onReady();
  }

  /// 初始化面包屑导航
  _initBreadcrumb() {
    folderLevel = 0;
    state.breadcrumbBeans.clear();
    state.breadcrumbBeans.add(FolderBreadcrumbBean(
        'cloud_disk_breadcrumb_all'.tr, O2.o2DefaultUnitParentId, folderLevel));
  }

  /// 刷新数据
  void refreshData() async {
    var lastBreadcrumb = state.breadcrumbBeans.last;
    // 获取列表数据
    var folderId = lastBreadcrumb.folderId;
    folderLevel = lastBreadcrumb.level;

    if (pickerType == FolderPickerType.zone) {
      _loadListByParentV3(folderId);
    } else {
      if (folderId == O2.o2DefaultUnitParentId) {
        _loadTopList();
      } else {
        _loadListByParent(folderId);
      }
    }
  }
  
  /// 查询数据
  void _loadListByParentV3(String parentId) async {
    Loading.show();
    state.cloudDiskItemList.clear();
    var listFolder =
        await FileAssembleService.to.listFolderByParentV3(parentId);
    if (listFolder != null) {
      for (var element in listFolder) {
        var item = CloudDiskItem.folder(element);
        state.cloudDiskItemList.add(item);
      }
    }
    Loading.dismiss();
  }

  /// 查询顶层数据
  void _loadTopList() async {
    state.cloudDiskItemList.clear();
    var listFolder = await FileAssembleService.to.listFolderTop();
    if (listFolder != null) {
      for (var element in listFolder) {
        var item = CloudDiskItem.folder(element);
        state.cloudDiskItemList.add(item);
      }
    }
  }

  /// 查询数据
  void _loadListByParent(String parentId) async {
    state.cloudDiskItemList.clear();
    var listFolder = await FileAssembleService.to.listFolderByParent(parentId);
    if (listFolder != null) {
      for (var element in listFolder) {
        var item = CloudDiskItem.folder(element);
        state.cloudDiskItemList.add(item);
      }
    }
  }

  /// 点击面包屑导航
  void clickBreadcrumb(FolderBreadcrumbBean bean) {
    var list = state.breadcrumbBeans.toList();
    var last = list.last;
    if (bean.folderId != last.folderId) {
      var clickIndex = 0;
      for (var i = 0; i < list.length; i++) {
        var item = list[i];
        if (item.folderId == bean.folderId) {
          clickIndex = i;
        }
      }
      var newList = clickIndex == 0 ? [list[0]] : list.sublist(0, clickIndex);
      state.breadcrumbBeans.clear();
      state.breadcrumbBeans.addAll(newList);
      refreshData();
    }
  }

  /// 点击列表对象
  void onItemClick(CloudDiskItem item) {
    if (item.isFolder) {
      OLogger.d('点击文件夹: ${item.folder?.name}');
      FolderBreadcrumbBean bean = FolderBreadcrumbBean(
          item.folder?.name ?? '', item.folder?.id ?? '', folderLevel + 1);
      state.breadcrumbBeans.add(bean);
      refreshData();
    }
  }

  /// 选中某一个 Item
  void onItemCheckedChange(CloudDiskItem item, bool? checked) {
    state.cloudDiskItemCheckedList.clear();
    if (checked == true) {
      state.cloudDiskItemCheckedList.add(item);
    }
  }

  void submitPicker() {
    if (state.cloudDiskItemCheckedList.isEmpty) {
      Loading.showError('cloud_disk_folder_picker_empty'.tr);
      return;
    }
    final folder = state.cloudDiskItemCheckedList.first;
    Get.back(result: folder.folder);
  }

  /// 选择顶层
  ///
  /// 顶层没有实际的 Folder 对象 创建一个空的
  void submitTopFolder() {
    if (pickerType == FolderPickerType.zone) {
      final zone = state.breadcrumbBeans.first;
      Get.back(result: FolderInfo(id: zone.folderId, name: zone.displayName));
    } else {
      Get.back(result: FolderInfo(id: ""));
    }
    
  }
}
