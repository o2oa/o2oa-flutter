import 'package:get/get.dart';

import 'controller.dart';

class FolderPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<FolderPickerController>(() => FolderPickerController())];
  }
}
