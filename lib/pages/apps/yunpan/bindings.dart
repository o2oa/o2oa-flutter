import 'package:get/get.dart';

import 'controller.dart';

class YunpanBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<YunpanController>(() => YunpanController())];
  }
}
