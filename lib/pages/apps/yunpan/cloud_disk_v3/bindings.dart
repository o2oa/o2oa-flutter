import 'package:get/get.dart';

import 'controller.dart';

class CloudDiskV3Binding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CloudDiskV3Controller>(() => CloudDiskV3Controller())];
  }
}
