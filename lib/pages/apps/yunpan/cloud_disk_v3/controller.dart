import 'package:get/get.dart';

import '../../../../common/routers/index.dart';
import 'index.dart';

class CloudDiskV3Controller extends GetxController {
  CloudDiskV3Controller();

  final state = CloudDiskV3State();
 
  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.title = Get.parameters['displayName'] ?? "app_name_yunpan".tr;
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  void gotoEnterprise() {
    Get.toNamed(O2OARoutes.appCloudDiskV3EnterpriseZone);
  }
  void gotoPerson() {
    Get.toNamed(O2OARoutes.appYunpan);
  }
}
