import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';

import '../../../../common/index.dart';
import '../../../../common/models/index.dart';
import 'index.dart';

/// 企业网盘 工作区
class EnterpriseZonePage extends GetView<EnterpriseZoneController> {
  const EnterpriseZonePage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return Obx(() {
      final list = controller.state.zoneList.toList();
      return GroupedListView<CloudDiskZoneInfo, String>(
          elements: list,
          groupBy: (element) => element.groupTag ?? '',
          groupSeparatorBuilder: ((value) => Padding(
              padding: const EdgeInsets.only(left: 8, top: 10, bottom: 10),
              child: Text(value))),
          itemBuilder: (context, element) {
            return Container(
                color: Theme.of(context).colorScheme.background,
                child: ListTile(
                  title: InkWell(
                      onTap: () => controller.clickZone(element),
                      child: Text(element.name ?? '',
                          style: Theme.of(context).textTheme.bodyLarge)),
                  trailing: IconButton(
                      icon: const Icon(Icons.more_vert_outlined),
                      onPressed: () => controller.clickZoneMenu(element)),
                ));
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EnterpriseZoneController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text("cloud_disk_v3_enterprise_file".tr),
            actions: [
              Obx(() => Visibility(
                  visible: controller.state.canCreate,
                  child: TextButton(
                      onPressed: () => controller.createZone(),
                      child: Text('create'.tr,
                          style: AppTheme.whitePrimaryTextStyle))))
            ],
          ),
          body: SafeArea(
            child: Obx(() => controller.state.zoneList.toList().isEmpty
                ? O2UI.noResultView(context)
                : _buildView()),
          ),
        );
      },
    );
  }
}
