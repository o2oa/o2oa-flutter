import 'package:get/get.dart';

import 'controller.dart';

class EnterpriseZoneBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<EnterpriseZoneController>(() => EnterpriseZoneController())];
  }
}
