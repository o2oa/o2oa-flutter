import 'package:get/get.dart';

import 'controller.dart';

class MainBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MainController>(() => MainController())];
  }
}
