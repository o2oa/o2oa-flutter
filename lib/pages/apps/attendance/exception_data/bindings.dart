import 'package:get/get.dart';

import 'controller.dart';

class ExceptionDataBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ExceptionDataController>(() => ExceptionDataController())];
  }
}
