import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/style/index.dart';
import 'index.dart';

class ExceptionDataPage extends GetView<ExceptionDataController> {
  const ExceptionDataPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return Obx(() => SmartRefresher(
        enablePullDown: true,
        enablePullUp: controller.state.hasMoreData,
        controller: controller.refreshController,
        onRefresh: () => controller.refreshMyAppealList(),
        onLoading: () => controller.loadMoreMyAppealList(),
        child: ListView.separated(
            itemBuilder: (context, index) {
              final info = controller.state.myAppealList[index];
              final time = info.recordDate ?? '';
              final duty = info.record?.checkInTypeText();
              final showText = "$time ($duty)";
              var result = info.record?.resultText();
              if (info.record?.fieldWork == true) {
                result = 'attendance_result_fieldWork'.tr;
              }
              return ListTile(
                onTap: () => controller.showOperationMenu(info),
                title: Text(result ?? '',
                    style: Theme.of(context).textTheme.bodyLarge),
                subtitle: Text(showText,
                    style: Theme.of(context).textTheme.bodyMedium),
                trailing: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: controller.state.enableProcess
                        ? [
                            Text(info.statsText(),
                                style: Theme.of(context).textTheme.bodySmall),
                            const Icon(
                              Icons.keyboard_arrow_right,
                              color: AppColor.hintText,
                              size: 22,
                            )
                          ]
                        : [
                            Text(info.statsText(),
                                style: Theme.of(context).textTheme.bodySmall)
                          ]),
              );
            },
            separatorBuilder: (context, index) {
              return const Divider(
                height: 1,
                color: Colors.transparent,
              );
            },
            itemCount: controller.state.myAppealList.length)));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ExceptionDataController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('attendance_exception_data_title'.tr)),
          body: SafeArea(
            child: _buildView(),
          ),
        );
      },
    );
  }
}
