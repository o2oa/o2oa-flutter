import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ExceptionDataState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  RxList<AttendanceV2Appeal> myAppealList = <AttendanceV2Appeal>[].obs;
  
  // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;

  // 是否启用申诉流程
  final _enableProcess = false.obs;
  set enableProcess(bool value) => _enableProcess.value = value;
  bool get enableProcess => _enableProcess.value;
  
}
