import 'package:get/get.dart';

import 'controller.dart';

class OldCheckInBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<OldCheckInController>(() => OldCheckInController())];
  }
}
