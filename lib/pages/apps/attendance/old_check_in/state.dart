import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class OldCheckInState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 当前时间
  final _currentTime = "".obs;
  set currentTime(value) => _currentTime.value = value;
  get currentTime => _currentTime.value;

   // 当前地址信息
  final _currentAddress = "".obs;
  set currentAddress(value) => _currentAddress.value = value;
  String get currentAddress => _currentAddress.value;
  
   // 是否在打卡地址范围内
  final _isInCheckInPositionRange = false.obs;
  set isInCheckInPositionRange(bool value) => _isInCheckInPositionRange.value = value;
  bool get isInCheckInPositionRange => _isInCheckInPositionRange.value;


   // 今天是否还需要打卡
  final _todayNeedCheck = false.obs;
  set todayNeedCheck(bool value) => _todayNeedCheck.value = value;
  bool get todayNeedCheck => _todayNeedCheck.value;

  // 打卡记录
  RxList<Feature> recordList = <Feature>[].obs;

}
