import 'package:get/get.dart';

import 'controller.dart';

class ProcessPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ProcessPickerController>(() => ProcessPickerController())];
  }
}
