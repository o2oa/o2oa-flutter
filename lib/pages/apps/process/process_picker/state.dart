import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ProcessPickerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 应用列表
  final RxList<ProcessApplicationData>  appList = <ProcessApplicationData>[].obs;
  // 流程列表
  final RxList<ProcessData> processList = <ProcessData>[].obs;

  // 选择器 模式 默认选择流程
  final Rx<ProcessPickerMode> mode = Rx<ProcessPickerMode>(ProcessPickerMode.process);

  // 选中的应用
  final Rx<ProcessApplicationData?> currentApp = Rx<ProcessApplicationData?>(null);

}
