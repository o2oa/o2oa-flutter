import 'package:get/get.dart';

import 'controller.dart';

class OfficeCenterBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<OfficeCenterController>(() => OfficeCenterController())];
  }
}
