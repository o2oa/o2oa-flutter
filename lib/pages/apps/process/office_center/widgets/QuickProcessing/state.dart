import 'package:get/get.dart';

import '../../../../../../common/models/process/index.dart';

class QuickprocessingState {
 
 
  final Rx<TaskData?> task = Rx<TaskData?>(null);

  /// 当前路由
  final _routeName = "".obs;
  set routeName(String value) => _routeName.value = value;
  String get routeName => _routeName.value;

  /// 常用意见
  final RxList<String> ideaList = <String>[].obs;
  /// 手写意见图片地址
  final Rx<String?> handwritingImagePath = Rx<String?>(null);

}
