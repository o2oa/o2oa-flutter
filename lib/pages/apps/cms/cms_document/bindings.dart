import 'package:get/get.dart';

import 'controller.dart';

class CmsDocumentBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CmsDocumentController>(() => CmsDocumentController())];
  }
}
