import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';

import '../../../../common/utils/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class CmsDocumentPage extends GetView<CmsDocumentController> {
  @override
  final String? tag;
  const CmsDocumentPage({Key? key, required this.tag}) : super(key: key);

  static Future<void> open(String documentId,
      {String title = '',
      Map<String, dynamic>? options,
      bool isCloseCurrentPage = false}) async {
    Get.lazyPut<CmsDocumentController>(() => CmsDocumentController(),
        tag: documentId);
    if (isCloseCurrentPage) {
      await Get.off(() => CmsDocumentPage(tag: documentId), arguments: {
        "documentId": documentId,
        "title": title,
        "options": options
      });
    } else {
      await Get.to(() => CmsDocumentPage(tag: documentId), arguments: {
        "documentId": documentId,
        "title": title,
        "options": options
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CmsDocumentController>(
      tag: tag,
      builder: (_) {
        return PopScope(
            canPop: false,
            onPopInvokedWithResult: (didPop, result) {
              OLogger.d('============>onPopInvokedWithResult $didPop');
              if (didPop) {
                return;
              }
              controller.tapBackBtn();
            },
            child: Scaffold(
              appBar: AppBar(
                  leading: IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () => {controller.tapBackBtn()},
                  ),
                  title: Obx(() => Text(controller.state.title)),
                  actions: [
                    O2UI.webviewPopupMenu((index) => controller.webviewMenuTap(index))
                  ]),
              body: SafeArea(
                child: Obx(() => controller.state.url.isEmpty
                    ? const Center(child: CircularProgressIndicator())
                    : _buildView()),
              ),
            ));
      },
    );
  }

  // 主视图
  Widget _buildView() {
    return InAppWebView(
        key: controller.webViewKey,
        initialUrlRequest: URLRequest(url: WebUri(controller.state.url)),
        initialSettings: InAppWebViewSettings(
            useShouldOverrideUrlLoading: true,
            useOnDownloadStart: true,
            javaScriptCanOpenWindowsAutomatically: true,
            mediaPlaybackRequiresUserGesture: false,
            applicationNameForUserAgent: O2.webviewUserAgent,
          ),
        onWebViewCreated: (c) {
          controller.setupWebviewJsHandler(c);
        },
        shouldOverrideUrlLoading: (c, navigationAction) async {
          var uri = navigationAction.request.url!;
          OLogger.d("shouldOverrideUrlLoading uri: $uri");
          return NavigationActionPolicy.ALLOW;
        },
        onProgressChanged: (c, p) {
          controller.progressChanged(c, p);
        },
        // h5下载文件
        onDownloadStartRequest: (c, request) async {
          controller.webviewHelper.onFileDownloadStart(request);
        },
        onTitleChanged: ((c, title) {
          OLogger.d('修改网页标题： $title');
          final host = O2ApiManager.instance.getWebHost();
          if (title != null && title.isNotEmpty && !title.contains(host)) {
            controller.state.title = title;
          }
        }),
        onConsoleMessage: (c, consoleMessage) {
          OLogger.i("console: $consoleMessage");
        });
  }
}
