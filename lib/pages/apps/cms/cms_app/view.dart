import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart';
import 'widgets/cms_category/index.dart';

class CmsAppPage extends GetView<CmsAppController> {
  const CmsAppPage({Key? key}) : super(key: key);

  static void open(CmsAppData appData, {String categoryId = ""}) {
    Get.toNamed(O2OARoutes.appCmsApplication, arguments: {"appData": appData, "categoryId": categoryId});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CmsAppController>(
      builder: (_) {
        return Obx(() => DefaultTabController(
            length: controller.state.categoryList.length,
            child: Scaffold(
              appBar: AppBar(
                title: Text(controller.state.app.value?.appName ?? ''),
                actions: [
                  controller.state.canPublish
                      ? TextButton(
                          onPressed: controller.clickPublish,
                          child: Text('cms_document_publish'.tr,
                              style: AppTheme.whitePrimaryTextStyle))
                      : Container()
                ],
                bottom: controller.state.categoryList.length == 1
                    ? null
                    : TabBar(
                      isScrollable: true,
                        //生成Tab菜单
                        tabs: controller.state.categoryList.isEmpty
                            ? []
                            : controller.state.categoryList
                                .map((e) => Tab(
                                    child: Text(e.categoryName ?? '',
                                        style: AppTheme.whitePrimaryTextStyle)))
                                .toList()),
              ),
              body: SafeArea(
                child: controller.state.categoryList.length == 1
                    ? CmsCategoryPage(controller.state.categoryList[0])
                    : TabBarView(
                        children: controller.state.categoryList.isEmpty
                            ? []
                            : controller.state.categoryList.map((m) => CmsCategoryPage(m)).toList(),
                      ),
              ),
            )));
      },
    );
  }
}
