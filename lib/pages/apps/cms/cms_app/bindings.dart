import 'package:get/get.dart';

import 'controller.dart';

class CmsAppBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CmsAppController>(() => CmsAppController())];
  }
}
