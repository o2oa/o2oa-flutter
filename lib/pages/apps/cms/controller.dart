import 'dart:convert';
import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import 'cms_app/index.dart';
import 'index.dart';

class CmsController extends GetxController {
  CmsController();

  final state = CmsState();
 

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.title = Get.parameters['displayName'] ?? "app_name_cms".tr;
    loadApplist();
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  Future<void> loadApplist() async {
    var list = await CmsAssembleControlService.to.listAppWithCategoryUserCanView();
    if (list != null && list.isNotEmpty) {
      state.appList.clear();
      state.appList.addAll(list);
    }
  }

  Uint8List? iconBase64(String? icon) {
    if (icon == null) {
      return null;
    }
    return base64Decode(icon);
  }

  void clickApp(CmsAppData app) {
    CmsAppPage.open(app);
  }
}
