import 'package:get/get.dart';

import 'controller.dart';

class CmsCategoryPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CmsCategoryPickerController>(() => CmsCategoryPickerController())];
  }
}
