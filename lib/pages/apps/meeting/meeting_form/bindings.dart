import 'package:get/get.dart';

import 'controller.dart';

class MeetingFormBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MeetingFormController>(() => MeetingFormController())];
  }
}
