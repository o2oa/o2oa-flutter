import 'package:get/get.dart';

import 'controller.dart';

class MeetingDetailBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MeetingDetailController>(() => MeetingDetailController())];
  }
}
