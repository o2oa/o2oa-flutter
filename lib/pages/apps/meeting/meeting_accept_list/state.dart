import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class MeetingAcceptListState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;



  final RxList<MeetingInfoModel> meetingList = <MeetingInfoModel>[].obs;
}
