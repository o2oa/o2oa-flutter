import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/utils/index.dart';
import '../meeting_detail/index.dart';
import 'index.dart';

class MeetingAcceptListController extends GetxController {
  MeetingAcceptListController();

  final state = MeetingAcceptListState();
 

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    loadAcceptList();
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  loadAcceptList() async {
    state.meetingList.clear();
    final list = await MeetingAssembleService.to.meetingListByAccept();
    if (list != null) {
      state.meetingList.addAll(list);
    }
  }

  void openMeetingView(MeetingInfoModel info) {
    OLogger.d('查看会议');
    if (info.id == null) {
      OLogger.e('会议id 为空');
      return;
    }
    MeetingDetailPage.openMeeting(info.id!);
  }

  void acceptInviteMeeting(MeetingInfoModel info) async {
    OLogger.d('接受会议邀请');
    final id = await MeetingAssembleService.to.acceptMeetingInvite(info.id!);
    if (id != null) {
      loadAcceptList();
    }
  }

  void rejectInviteMeeting(MeetingInfoModel info) async {
    OLogger.d('拒绝会议邀请');
    final id = await MeetingAssembleService.to.rejectMeetingInvite(info.id!);
    if (id != null) {
      loadAcceptList();
    }
  }
}
