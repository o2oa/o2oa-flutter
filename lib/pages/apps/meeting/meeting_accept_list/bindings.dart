import 'package:get/get.dart';

import 'controller.dart';

class MeetingAcceptListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MeetingAcceptListController>(() => MeetingAcceptListController())];
  }
}
