import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class MeetingRoomPickerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _startTime = "".obs;
  set startTime(String value) => _startTime.value = value;
  String get startTime => _startTime.value;

  final _endTime = "".obs;
  set endTime(String value) => _endTime.value = value;
  String get endTime => _endTime.value;

  final RxList<MeetingRoom> roomList = <MeetingRoom>[].obs;
}
