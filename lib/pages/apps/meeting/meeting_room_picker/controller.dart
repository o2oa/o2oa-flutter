import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/index.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import 'index.dart';

class MeetingRoomPickerController extends GetxController {
  MeetingRoomPickerController();

  final state = MeetingRoomPickerState();
 

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    final args = Get.arguments;
    if (args != null) {
      state.startTime = args['startTime'] ?? '';
      state.endTime = args['endTime'] ?? '';
    }
    loadRoomList();
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  Future<void> loadRoomList() async {
    if (state.startTime.isEmpty || state.endTime.isEmpty) {
      final now = DateTime.now();
      var startDate = now.addHours(1);
      final completeDate = now.addHours(2);
      state.startTime = startDate.ymdhms();
      state.endTime = completeDate.ymdhms();
    }
    final buildList = await MeetingAssembleService.to.buildListWithStartCompleted(state.startTime.substring(0, 16), state.endTime.substring(0, 16));
    if (buildList != null) {
      List<MeetingRoom> roomList = [];
      for (var build in buildList) {
        final rooms = build.roomList ?? [];
        for (var room in rooms) {
          room.buildName = build.name;
          roomList.add(room);
        }
      }
      state.roomList.clear();
      state.roomList.addAll(roomList);
    }
  }

  void chooseRoom(MeetingRoom room) {
    // if (room.idle != true || room.available != true) {
    //   Loading.toast('meeting_room_not_be_choose'.tr);
    //   return;
    // }
    Get.back(result: room);
  }
}
