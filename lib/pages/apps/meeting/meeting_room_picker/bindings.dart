import 'package:get/get.dart';

import 'controller.dart';

class MeetingRoomPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MeetingRoomPickerController>(() => MeetingRoomPickerController())];
  }
}
