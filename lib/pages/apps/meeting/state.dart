import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../common/models/index.dart';

class MeetingState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final _format = Rx<CalendarFormat>(CalendarFormat.week);
  set format(CalendarFormat value) => _format.value = value;
  CalendarFormat get format => _format.value;

  final _focusedDay = Rx<DateTime>(DateTime.now());
  set focusedDay(DateTime value) => _focusedDay.value = value;
  DateTime get focusedDay => _focusedDay.value;

   final _selectedDay = Rx<DateTime?>(null);
  set selectedDay(DateTime? value) => _selectedDay.value = value;
  DateTime? get selectedDay => _selectedDay.value;


  final RxList<MeetingInfoModel> meetingList = <MeetingInfoModel>[].obs;

  // 配置文件
  final Rx<MeetingConfigModel?> meetingConfig = Rx<MeetingConfigModel?>(null);
}
