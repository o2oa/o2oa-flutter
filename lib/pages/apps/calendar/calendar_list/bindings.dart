import 'package:get/get.dart';

import 'controller.dart';

class CalendarListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CalendarListController>(() => CalendarListController())];
  }
}
