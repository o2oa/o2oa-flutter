import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class CalendarListState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final RxList<CalendarInfo> calendarList = <CalendarInfo>[].obs;

    // 是否是 日历选择器
  final _isSelectMode = false.obs;
  set isSelectMode(bool value) => _isSelectMode.value = value;
  bool get isSelectMode => _isSelectMode.value;

}
