import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import '../calendar_const.dart';
import '../widgets/widgets.dart';
import 'index.dart';

class CreateCalendarPage extends GetView<CreateCalendarController> {
  const CreateCalendarPage({Key? key}) : super(key: key);

  /// 创建日历
  /// 
  /// 如果传入日历对象就是修改
  static Future<dynamic> open({CalendarInfo? calendar}) async {
    await Get.toNamed(O2OARoutes.appCalendarCreate, arguments: {"calendar": calendar});
  }

  // 主视图
  Widget _formView(BuildContext context) {
    return O2UI.sectionOutBox( Column(children: [
        // 标题
        TextField(
          controller: controller.titleController,
          decoration: InputDecoration(
              hintText: "calendar_event_form_title_hint".tr,
              label: Text('calendar_event_form_title'.tr)),
        ),
        // 颜色
        Obx(() {
          final eventColor = controller.state.eventColor;
          return ColorChooseView(
            color: eventColor,
            onChange: ((color) => controller.chooseEventColor(color)));
            }),
        const Divider(height: 1),
         // 日历类型
        Obx(()=>O2UI.lineWidget(
            'calendar_edit_form_type'.tr, Text(O2Calendar.calendarTypes[controller.state.calendarType] ?? ''),
            ontap: controller.state.isUpdate ? null : () => controller.selectCalendarType())),
        const Divider(height: 1),
        // 是否公开
        Obx(()=>Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  'calendar_edit_form_is_public'.tr,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(width: 10),
                Switch(
                  value: controller.state.isPublic,
                  onChanged: (bool value) {
                    controller.changePublic(value);
                  },
                )
              ],
            ))),
        const Divider(height: 1),

         // 所属组织
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: O2UI.lineWidget(
            'calendar_edit_form_target'.tr, Text(controller.state.target.o2NameCut()),
            ontap:  () => controller.chooseTarget()))),
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: const Divider(height: 1))),

         //  管理者
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: O2UI.lineWidget(
            'calendar_edit_form_manageable'.tr, Text(controller.state.manageablePersonName),
            ontap:  () => controller.chooseManageAble()))),
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: const Divider(height: 1))),
         //  可见范围
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: O2UI.lineWidget(
            'calendar_edit_form_viewable'.tr, Text(controller.state.viewableName),
            ontap:  () => controller.chooseViewAble()))),
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: const Divider(height: 1))),
         //  可新建范围
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: O2UI.lineWidget(
            'calendar_edit_form_publicable'.tr, Text(controller.state.publishableName),
            ontap:  () => controller.choosePublishAble()))),
        Obx(()=> Visibility(
            visible: controller.state.calendarType == 'UNIT',
            child: const Divider(height: 1))),

         //  备注
        TextField(
          controller: controller.summaryController,
          maxLines: 3,
          decoration: InputDecoration(
              hintText: "calendar_edit_form_summary_hint".tr,
              label: Text('calendar_edit_form_summary'.tr)),
        ),
        const SizedBox(height: 5),
        Obx(()=>Visibility(
            visible: controller.state.isUpdate,
            child:Padding(
            padding: const EdgeInsets.only(
                top: 40, bottom: 10, left: 15, right: 15),
            child: SizedBox(
                width: double.infinity,
                height: 44,
                child: O2ElevatedButton(() => controller.deleteCalendar(),
                    Text(
                      'calendar_action_calendar_delete'.tr,
                      style: const TextStyle(fontSize: 18),
                    ))),
          )))
    ]
    ), context);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateCalendarController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title:  Obx(() => Text(controller.state.isUpdate
                  ? 'calendar_action_calendar_update'.tr
                  : 'calendar_action_calendar_create'.tr)), actions: [
                    Obx(() => controller.state.isUpdate
                    ? TextButton(
                        onPressed: () => controller.updateCalendar(),
                        child: Text('save'.tr,
                            style: AppTheme.whitePrimaryTextStyle))
                    : TextButton(
                        onPressed: () => controller.saveCalendar(),
                        child: Text('save'.tr,
                            style: AppTheme.whitePrimaryTextStyle)))
                  ],),
          body: SafeArea(
            child: ListView(
                children: [const SizedBox(height: 8), _formView(context)]),
          ),
        );
      },
    );
  }
}
