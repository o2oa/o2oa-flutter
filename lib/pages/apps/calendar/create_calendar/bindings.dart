import 'package:get/get.dart';

import 'controller.dart';

class CreateCalendarBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CreateCalendarController>(() => CreateCalendarController())];
  }
}
