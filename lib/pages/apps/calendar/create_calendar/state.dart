import 'package:get/get.dart';

class CreateCalendarState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 是否更新
  final _isUpdate = false.obs;
  set isUpdate(bool value) => _isUpdate.value = value;
  bool get isUpdate => _isUpdate.value;

  // 颜色
  final _eventColor = "".obs;
  set eventColor(String value) => _eventColor.value = value;
  String get eventColor => _eventColor.value;

  // 是否公开
  final _isPublic = false.obs;
  set isPublic(bool value) => _isPublic.value = value;
  bool get isPublic => _isPublic.value;

  // 日历类型
  final _calendarType = "".obs;
  set calendarType(String value) => _calendarType.value = value;
  String get calendarType => _calendarType.value;


  // 所属组织
  final _target = "".obs;
  set target(String value) => _target.value = value;
  String get target => _target.value;

  // 管理人员 显示
  final _manageablePersonName = "".obs;
  set manageablePersonName(String value) => _manageablePersonName.value = value;
  String get manageablePersonName => _manageablePersonName.value;

  // 可新建
  final _publishableName = "".obs;
  set publishableName(String value) => _publishableName.value = value;
  String get publishableName => _publishableName.value;

  // 可见范围
  final _viewableName = "".obs;
  set viewableName(String value) => _viewableName.value = value;
  String get viewableName => _viewableName.value;

}
