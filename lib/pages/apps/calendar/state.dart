import 'package:get/get.dart';

import '../../../common/models/index.dart';

class CalendarState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;




  final _focusedDay = Rx<DateTime>(DateTime.now());
  set focusedDay(DateTime value) => _focusedDay.value = value;
  DateTime get focusedDay => _focusedDay.value;

   final _selectedDay = Rx<DateTime?>(null);
  set selectedDay(DateTime? value) => _selectedDay.value = value;
  DateTime? get selectedDay => _selectedDay.value;



  final RxList<EventInfo> eventList = <EventInfo>[].obs;

}
