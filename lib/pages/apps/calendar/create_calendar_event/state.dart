import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class CreateCalendarEventState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 是否更新
  final _isUpdate = false.obs;
  set isUpdate(bool value) => _isUpdate.value = value;
  bool get isUpdate => _isUpdate.value;

  // 选择日历
  Rx<CalendarInfo?> calendar = Rx<CalendarInfo?>(null);

  // 全天
  final _allDay = false.obs;
  set allDay(bool value) => _allDay.value = value;
  bool get allDay => _allDay.value;

  final _startDate = "".obs;
  set startDate(String value) => _startDate.value = value;
  String get startDate => _startDate.value;

  final _endDate = "".obs;
  set endDate(String value) => _endDate.value = value;
  String get endDate => _endDate.value;

  final _startTime = "".obs;
  set startTime(String value) => _startTime.value = value;
  String get startTime => _startTime.value;

  final _endTime = "".obs;
  set endTime(String value) => _endTime.value = value;
  String get endTime => _endTime.value;

  // 颜色
  final _eventColor = "".obs;
  set eventColor(String value) => _eventColor.value = value;
  String get eventColor => _eventColor.value;

    // repeat
  final _repeat = "NONE".obs;
  set repeat(String value) => _repeat.value = value;
  String get repeat => _repeat.value;
 // remind
  final _remind = "NONE".obs;
  set remind(String value) => _remind.value = value;
  String get remind => _remind.value;


  final _repeatEndDate = "".obs;
  set repeatEndDate(String value) => _repeatEndDate.value = value;
  String get repeatEndDate => _repeatEndDate.value;

  final RxList<String> weekDayKeys = <String>[].obs;
   

  


}
