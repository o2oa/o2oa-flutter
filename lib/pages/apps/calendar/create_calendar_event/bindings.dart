import 'package:get/get.dart';

import 'controller.dart';

class CreateCalendarEventBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CreateCalendarEventController>(() => CreateCalendarEventController())];
  }
}
