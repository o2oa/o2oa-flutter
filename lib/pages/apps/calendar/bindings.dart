import 'package:get/get.dart';

import 'controller.dart';

class CalendarBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CalendarController>(() => CalendarController())];
  }
}
