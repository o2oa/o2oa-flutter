import 'package:get/get.dart';

class InnerWebviewState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _openUri = "".obs;
  set openUri(String value) => _openUri.value = value;
  String get openUri => _openUri.value;
}
