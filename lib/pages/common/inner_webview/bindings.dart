import 'package:get/get.dart';

import 'controller.dart';

class InnerWebviewBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<InnerWebviewController>(() => InnerWebviewController())];
  }
}
