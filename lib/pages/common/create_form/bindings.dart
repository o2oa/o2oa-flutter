import 'package:get/get.dart';

import 'controller.dart';

class CreateFormBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<CreateFormController>(() => CreateFormController())];
  }
}
