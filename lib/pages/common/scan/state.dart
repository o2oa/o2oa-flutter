import 'package:get/get.dart';

class ScanState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _torchLight = false.obs;
  set torchLight(value) => _torchLight.value = value;
  get torchLight => _torchLight.value;

}
