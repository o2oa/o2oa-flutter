import 'package:get/get.dart';

import 'controller.dart';

class ScanBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ScanController>(() => ScanController())];
  }
}
