import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/routers/index.dart';

import '../../../common/style/index.dart';
import 'index.dart';

class InputFormPage extends GetView<InputFormController> {
  const InputFormPage({Key? key}) : super(key: key);

  static Future<dynamic> openInputForm(
      String title, String defaultValue) async {
    return await Get.toNamed(O2OARoutes.commonInput,
        arguments: {'title': title, 'defaultValue': defaultValue});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<InputFormController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
              title: Obx(() => Text(controller.state.title)),
              actions: [
                TextButton(
                    onPressed: () => controller.clickSave(),
                    child: Text('positive'.tr,
                        style: AppTheme.whitePrimaryTextStyle))
              ],
            ),
            body: SafeArea(
              child: Container(
                color: Theme.of(context).colorScheme.background,
                child: Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: Obx(() => 
                    TextField(
                      maxLines: 1,
                      style: Theme.of(context).textTheme.bodyMedium,
                      autofocus: true,
                      controller: controller.inputController,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        labelText: 'input_form_placeholder'
                            .trArgs([controller.state.title]),
                      ),
                    ))),
              ),
            ));
      },
    );
  }
}
