import 'package:get/get.dart';

import 'controller.dart';

class InputFormBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<InputFormController>(() => InputFormController())];
  }
}
