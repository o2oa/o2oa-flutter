import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';

class InputFormController extends GetxController {
  InputFormController();

  final state = InputFormState();


  // 登录用户名的控制器
  final TextEditingController inputController = TextEditingController();

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    var map = Get.arguments;
    if (map != null) {
      state.title = map['title'] ?? '';
      inputController.text = map['defaultValue'] ?? '';
    }
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  void clickSave() {
    String text = inputController.text;
    Get.back(result: text);
  }
}
