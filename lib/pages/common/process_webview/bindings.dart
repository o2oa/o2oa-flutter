import 'package:get/get.dart';

import 'controller.dart';

class ProcessWebviewBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ProcessWebviewController>(() => ProcessWebviewController())];
  }
}
