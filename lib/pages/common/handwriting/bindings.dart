import 'package:get/get.dart';

import 'controller.dart';

class HandwritingBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<HandwritingController>(() => HandwritingController())];
  }
}
