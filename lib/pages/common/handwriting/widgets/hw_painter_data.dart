import 'dart:ui';


/// 画笔数据
class HWPainterData {
  Offset point; // 点
  Color color; // 颜色
  double strokeWidth; // 画笔粗细

  HWPainterData(this.point, this.color, this.strokeWidth);

}