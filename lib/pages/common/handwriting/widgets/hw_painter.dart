import 'package:flutter/material.dart';

import 'hw_painter_data.dart';

class HWPainter extends CustomPainter {
  HWPainter(this.points);

  final List<HWPainterData?> points;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..strokeCap = StrokeCap.round;
    for (var i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        var wp = points[i];
        var nextWp = points[i + 1];
        paint.color = wp!.color;
        paint.strokeWidth = wp.strokeWidth;
        canvas.drawLine(wp.point, nextWp!.point, paint);
      }
    }
  }

  @override
  bool shouldRepaint(HWPainter oldDelegate) {
    return oldDelegate.points != points;
  }
}