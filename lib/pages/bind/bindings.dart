import 'package:get/get.dart';

import 'controller.dart';

class BindBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<BindController>(() => BindController())];
  }
}
