import 'package:get/get.dart';

import 'controller.dart';

class DemoLoginBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<DemoLoginController>(() => DemoLoginController())];
  }
}
