import 'package:get/get.dart';


class DemoLoginState {
  
   // 当前年份
  final _year = "2022".obs;
  set year(value) => _year.value = value;
  get year => _year.value;
 

  /// 密码登录模式
  final _isPasswordLogin = true.obs;
  set isPasswordLogin(value) => _isPasswordLogin.value = value;
  get isPasswordLogin => _isPasswordLogin.value;

  /// 图片验证码是否启用
  final _isCaptcha = false.obs;
  set isCaptcha(value) => _isCaptcha.value = value;
  get isCaptcha => _isCaptcha.value;

  final _captchaBase64String =  ''.obs;
  set captchaBase64String(value) => _captchaBase64String.value = value;
  get captchaBase64String => _captchaBase64String.value;

}
