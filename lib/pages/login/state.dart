import 'package:get/get.dart';

class LoginState {
  // title
  final _title = "登录".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

   // 当前年份
  final _year = "2022".obs;
  set year(value) => _year.value = value;
  get year => _year.value;
 
  /// 是否能注册
  final _registerEnable = false.obs;
  set registerEnable(value) => _registerEnable.value = value;
  get registerEnable => _registerEnable.value;

  /// 密码登录模式
  final _isPasswordLogin = true.obs;
  set isPasswordLogin(value) => _isPasswordLogin.value = value;
  get isPasswordLogin => _isPasswordLogin.value;

  /// 图片验证码是否启用
  final _isCaptcha = false.obs;
  set isCaptcha(value) => _isCaptcha.value = value;
  get isCaptcha => _isCaptcha.value;

  final _captchaBase64String =  ''.obs;
  set captchaBase64String(value) => _captchaBase64String.value = value;
  get captchaBase64String => _captchaBase64String.value;

  /// 是否显示登录切换按钮
  final _isShowChangeLoginModeButton = false.obs;
  set isShowChangeLoginModeButton(value) => _isShowChangeLoginModeButton.value = value;
  get isShowChangeLoginModeButton => _isShowChangeLoginModeButton.value;

  ///  当前状态是否是双因素认证第二步
  final _twoFactorLogin = false.obs;
  set twoFactorLogin(value) => _twoFactorLogin.value = value;
  get twoFactorLogin => _twoFactorLogin.value;


  // 是否显示 同意协议
  final _isVisibleAgree = false.obs;
  set isVisibleAgree(bool value) => _isVisibleAgree.value = value;
  bool get isVisibleAgree => _isVisibleAgree.value;

  // 是否同意协议
  final _isAgree = false.obs;
  set isAgree(bool value) => _isAgree.value = value;
  bool get isAgree => _isAgree.value;
  
}
