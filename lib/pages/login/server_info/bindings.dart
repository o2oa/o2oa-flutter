import 'package:get/get.dart';

import 'controller.dart';

class ServerInfoBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ServerInfoController>(() => ServerInfoController())];
  }
}
