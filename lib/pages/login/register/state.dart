import 'package:get/get.dart';

class RegisterState {
   // title
  final _title = "注册".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

   // 当前年份
  final _year = "2024".obs;
  set year(value) => _year.value = value;
  get year => _year.value;
  

  /// 图片验证码是否启用
  final _isCaptcha = false.obs;
  set isCaptcha(value) => _isCaptcha.value = value;
  get isCaptcha => _isCaptcha.value;

  final _captchaBase64String =  ''.obs;
  set captchaBase64String(value) => _captchaBase64String.value = value;
  get captchaBase64String => _captchaBase64String.value;


  // 是否显示 同意协议
  final _isVisibleAgree = false.obs;
  set isVisibleAgree(bool value) => _isVisibleAgree.value = value;
  bool get isVisibleAgree => _isVisibleAgree.value;

  // 是否同意协议
  final _isAgree = false.obs;
  set isAgree(bool value) => _isAgree.value = value;
  bool get isAgree => _isAgree.value;
  
}
