import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/widgets/index.dart';
import 'index.dart';

class RegisterPage extends GetView<RegisterController> {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
      builder: (_) {
        return Scaffold(
            body: PageScrollContentWidget(
          contentWidget: Column(
            children: <Widget>[
              _buildInputForm(context),
              const Spacer(),
              _buildBottom(),
            ],
          ),
        ));
      },
    );
  }

  // 底部copyright
  Widget _buildBottom() {
    return Obx(() => Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Align(
            alignment: Alignment.center,
            child: Text(
                'copy_right'.trArgs(['appName'.tr, controller.state.year])),
          ),
        ));
  }

  // 表单
  Widget _buildInputForm(BuildContext context) {
    return Obx(() => Container(
          width: 295,
          margin: const EdgeInsets.only(top: 49),
          child: Column(children: [
            // 账号输入框
            TextField(
              maxLines: 1,
              autofocus: true,
              style: Theme.of(context).textTheme.bodyMedium,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              controller: controller.userNameController,
              onEditingComplete: () {
                FocusScope.of(context).requestFocus(controller.passwordNode);
              },
              decoration: InputDecoration(
                labelText: 'login_form_register_user_name'.tr,
                prefixIcon: Icon(Icons.person,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              maxLines: 1,
              obscureText: true,
              style: Theme.of(context).textTheme.bodyMedium,
              focusNode: controller.passwordNode,
              controller: controller.passwordController,
              textInputAction: TextInputAction.next,
              onEditingComplete: () {
                FocusScope.of(context)
                    .requestFocus(controller.repeatPasswordNode);
              },
              decoration: InputDecoration(
                labelText: 'login_form_user_password'.tr,
                prefixIcon: Icon(Icons.lock,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              maxLines: 1,
              obscureText: true,
              style: Theme.of(context).textTheme.bodyMedium,
              focusNode: controller.repeatPasswordNode,
              controller: controller.repeatPasswordController,
              textInputAction: TextInputAction.next,
              onEditingComplete: () {
                FocusScope.of(context).requestFocus(controller.phoneNode);
              },
              decoration: InputDecoration(
                labelText: 'login_form_user_password_repeat'.tr,
                prefixIcon: Icon(Icons.lock,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyMedium,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              focusNode: controller.phoneNode,
              controller: controller.phoneController,
              onEditingComplete: () {
                FocusScope.of(context).requestFocus(controller.emailNode);
              },
              decoration: InputDecoration(
                labelText: 'login_form_register_phone'.tr,
                prefixIcon: Icon(Icons.phone_android,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyMedium,
              textInputAction: TextInputAction.next,
              focusNode: controller.emailNode,
              controller: controller.emailController,
              onEditingComplete: () {
                if (controller.state.isCaptcha) {
                  FocusScope.of(context).requestFocus(controller.captchaNode);
                } else {
                  FocusScope.of(context).requestFocus(controller.codeNode);
                }
              },
              decoration: InputDecoration(
                labelText: 'login_form_register_email'.tr,
                prefixIcon: Icon(Icons.email,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            // 密码或短信验证码 输入框
            controller.state.isCaptcha
                ? _captchaInput(context)
                : _codeModeTextFormField(context),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              width: double.infinity,
              height: 42,
              child: O2ElevatedButton(
                () {
                  controller.register();
                },
                Text('register'.tr),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Visibility(
                visible: controller.state.isVisibleAgree,
                child: Row(
                  children: [
                    Obx(() => Checkbox(
                          value: controller.state.isAgree,
                          onChanged: (value) => controller.changeAgree(value),
                        )),
                    Expanded(
                        flex: 1,
                        child: RichText(
                          maxLines: 2,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          text: TextSpan(
                              text: 'privacy_policy_bind_content_part1'.tr,
                              style: Theme.of(context).textTheme.bodySmall,
                              children: [
                                TextSpan(
                                    text: 'user_agreement_book_mark'.tr,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        fontSize: 12),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap =
                                          () => controller.openUserAgreement()),
                                TextSpan(
                                  text:
                                      'privacy_policy_dialog_content_part2'.tr,
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                                TextSpan(
                                    text: 'privacy_policy_book_mark'.tr,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        fontSize: 12),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap =
                                          () => controller.openPrivacyPolicy()),
                              ]),
                        ))
                  ],
                )),
            const SizedBox(
              height: 16,
            ),
            TextButton(
                onPressed: () => controller.gotoLogin(),
                child: Text('register_to_login'.tr,
                    style: Theme.of(context).textTheme.bodySmall)),
          ]),
        ));
  }

  Widget _captchaInput(BuildContext context) {
    return Row(
      children: [
        Expanded(
            flex: 1,
            child: TextField(
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyMedium,
              keyboardType: TextInputType.number,
              focusNode: controller.captchaNode,
              controller: controller.captchaController,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                labelText: 'login_form_user_code'.tr,
                prefixIcon: Icon(Icons.lock,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            )),
        controller.state.captchaBase64String.isEmpty
            ? TextButton(
                onPressed: () {
                  controller.getCaptchaImageData();
                },
                child: const Text('点击刷新'))
            : InkWell(
                onTap: () {
                  controller.getCaptchaImageData();
                },
                child: Image.memory(
                  controller.parseCaptchaImg(),
                  width: 100,
                  height: 50,
                  fit: BoxFit.cover,
                ),
              )
      ],
    );
  }

  ///
  /// 短信验证码登录
  ///
  Widget _codeModeTextFormField(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: TextField(
            maxLines: 1,
            obscureText: true,
            style: Theme.of(context).textTheme.bodyMedium,
            keyboardType: TextInputType.text,
            controller: controller.codeController,
            focusNode: controller.codeNode,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              labelText: 'login_form_user_code'.tr,
              prefixIcon: Icon(Icons.lock,
                  color: Theme.of(context).colorScheme.secondary),
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: CountDownButton(() {
              return controller.onCheckMobile();
            }, () {
              controller.sendSmsCode();
            }))
      ],
    );
  }
}
