import 'package:get/get.dart';

import 'controller.dart';

class RegisterBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<RegisterController>(() => RegisterController())];
  }
}
