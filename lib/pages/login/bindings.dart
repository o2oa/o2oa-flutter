import 'package:get/get.dart';

import 'controller.dart';

class LoginBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<LoginController>(() => LoginController())];
  }
}
