import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class AccountSafePage extends GetView<AccountSafeController> {
  const AccountSafePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountSafeController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_account_safe".tr)),
          body: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget(
                          'account_safe_server_name'.tr,
                          Text(
                              O2ApiManager.instance.o2CloudServer?.centerHost ??
                                  '')),
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget('account_safe_login_name'.tr,
                          Text(O2ApiManager.instance.o2User?.name ?? '')),
                      const Divider(height: 1),
                      O2UI.lineWidget('account_safe_login_pwd_label'.tr,
                          Text('account_safe_login_pwd_update'.tr),
                          ontap: controller.goUpdatePassword),
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Obx(() => Column(children: [
                       // 生物识别锁
                        Visibility(
                            visible: !controller.state.gestureUnlockAuthSwitch,
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(controller.state.title),
                              Switch(
                                value: controller.state.biometricAuthSwitch,
                                onChanged: (bool value) {
                                  controller.switchLocalAuth(value);
                                },
                              )
                            ],
                          )),
                          const Divider(height: 1),
                          Visibility(
                            visible: !controller.state.biometricAuthSwitch,
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text('account_safe_gesture_unlock_title'.tr),
                              Switch(
                                value: controller.state.gestureUnlockAuthSwitch,
                                onChanged: (bool value) {
                                  controller.switchGestureUnlock(value);
                                },
                              )
                            ],
                          ))
                      // 手势密码锁
                    ])),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget(
                          'account_safe_empower'.tr, const SizedBox(),
                          ontap: controller.goEmpower),
                    ]),
                    context),
              ],
            ),
          ),
        );
      },
    );
  }
}
