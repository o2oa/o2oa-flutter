import 'package:get/get.dart';

import 'controller.dart';

class AccountSafeBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<AccountSafeController>(() => AccountSafeController())];
  }
}
