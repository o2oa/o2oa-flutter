import 'package:get/get.dart';

import 'controller.dart';

class EmpowerCreateBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<EmpowerCreateController>(() => EmpowerCreateController())];
  }
}
