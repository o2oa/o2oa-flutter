import 'package:get/get.dart';

import '../../../../../common/models/index.dart';

class EmpowerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 委托列表类型
  Rx<EmpowerType> listType = Rx<EmpowerType>(EmpowerType.myEmpower);

  RxList<EmpowerData> empowerList =  <EmpowerData>[].obs;
}
