import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../common/models/index.dart';

class GestureUnlockState {
 
  /// 九宫格点的列表  
  final RxList<GesturePassWord> ninePointList = <GesturePassWord>[].obs;

  /// 手指位置
  final Rx<Offset?> currentOffset = Rx<Offset?>(null);

  /// 连过的点对象集合
  final RxList<GesturePassWord> gesturePassWords = <GesturePassWord>[].obs;
  
  /// 点的颜色 默认Colors.blue 错误的时候 Colors.red
  final _color = Colors.blue.obs;
  set color(value) => _color.value = value;
  get color => _color.value;
  

  final _titleText= "".obs;
  set titleText(value) => _titleText.value = value; 
  get titleText => _titleText.value;

  final Rx<Color?> titleTextColor = Rx<Color?>(null);


  final _closeBtnVisible = false.obs;
  set closeBtnVisible(value) => _closeBtnVisible.value = value;
  get closeBtnVisible => _closeBtnVisible.value;
  

}
