import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/routers/index.dart';
import 'gesture_type.dart';
import 'index.dart';

class GestureUnlockPage extends GetView<GestureUnlockController> {
  const GestureUnlockPage({Key? key}) : super(key: key);

  static Future<dynamic> setupGesturePassword() async {
    return await Get.toNamed(O2OARoutes.homeSettingsAccountSafeGestureUnlock,
        arguments: {"type": GestureType.setup});
  }

  static void openGestureUnlock() {
     Get.offNamed(O2OARoutes.homeSettingsAccountSafeGestureUnlock, arguments: {"type": GestureType.unlock});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GestureUnlockController>(
      builder: (_) {
        return Scaffold(
          body: Container(
              color: Theme.of(context).colorScheme.background,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AnimatedBuilder(
                      animation: controller.animation,
                      builder: (ctx, child) {
                        return Container(
                          margin: EdgeInsetsDirectional.only(
                              bottom: 20,
                              start: controller.errorPwdAnimationValue(),
                              end: controller.animation.value),
                          child: Obx(() => Text(
                                controller.state.titleText,
                                style: TextStyle(fontSize: 22, color: controller.state.titleTextColor.value),
                              )),
                        );
                      }),
                  GestureDetector(
                    onPanDown: (d) => controller.judgeZone(d.localPosition),
                    onPanUpdate: (d) => controller.onPanUpdate(d.localPosition),
                    onPanEnd: (d) => controller.onPanEnd(),
                    child: Obx(() => CustomPaint(
                          size: Size(controller.gestureBoxSize,
                              controller.gestureBoxSize),
                          painter: _GesturesUnlockPainter(
                              controller.state.ninePointList,
                              controller.state.gesturePassWords,
                              controller.state.color,
                              controller.state.currentOffset.value),
                        )),
                  ),
                  Obx(() => Visibility(
                      visible: controller.state.closeBtnVisible,
                      child: TextButton(
                          onPressed: () => controller.closePage(),
                          child: Text('close'.tr))))
                ],
              )),
        );
      },
    );
  }
}

class _GesturesUnlockPainter extends CustomPainter {
  final List<GesturePassWord> centerOffset;
  final List<GesturePassWord> gesturePassWords;
  final double defaultStrokeWidth = 1;
  final Color painterColor;
  final Offset? currentOffset;

  _GesturesUnlockPainter(this.centerOffset, this.gesturePassWords,
      this.painterColor, this.currentOffset);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.translate(size.width / 2, size.height / 2);
    Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = defaultStrokeWidth
      ..color = painterColor;
    // 绘制圆
    _drawCirCle(canvas, size, paint);

    var offsets = gesturePassWords.map((e) => e.offset).toList();
    // 绘制按压点
    canvas.drawPoints(
        PointMode.points,
        offsets,
        paint
          ..strokeWidth = 20
          ..strokeCap = StrokeCap.round
          ..color = painterColor);

    // 绘制密码
    Path path = Path();
    if (gesturePassWords.isNotEmpty) {
      path.moveTo(gesturePassWords[0].offset.dx, gesturePassWords[0].offset.dy);
      if (currentOffset != null) {
        canvas.drawLine(gesturePassWords.last.offset, currentOffset!,
            paint..strokeWidth = defaultStrokeWidth);
      }
    }
    for (int i = 1; i < gesturePassWords.length; i++) {
      path.lineTo(gesturePassWords[i].offset.dx, gesturePassWords[i].offset.dy);
    }
    canvas.drawPath(path, paint..strokeWidth = defaultStrokeWidth);
  }

  @override
  bool shouldRepaint(_GesturesUnlockPainter oldDelegate) {
    return oldDelegate.centerOffset != centerOffset ||
        oldDelegate.gesturePassWords != gesturePassWords ||
        oldDelegate.painterColor != painterColor ||
        oldDelegate.currentOffset != currentOffset;
  }

  void _drawHelpRect(Canvas canvas, Size size, Paint paint) {
    for (int i = 0; i < centerOffset.length; i++) {
      canvas.drawRect(
          Rect.fromCenter(
              center: centerOffset[i].offset, width: 100, height: 100),
          paint);
    }
  }

  void _drawCirCle(Canvas canvas, Size size, Paint paint) {
    for (int i = 0; i < centerOffset.length; i++) {
      canvas.drawCircle(
          centerOffset[i].offset, 30, paint..color = painterColor);
    }
    for (int j = 0; j < gesturePassWords.length; j++) {
      canvas.drawCircle(
          gesturePassWords[j].offset, 30, paint..color = painterColor);
    }
  }
}
