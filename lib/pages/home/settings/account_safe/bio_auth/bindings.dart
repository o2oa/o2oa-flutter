import 'package:get/get.dart';

import 'controller.dart';

class BioAuthBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<BioAuthController>(() => BioAuthController())];
  }
}
