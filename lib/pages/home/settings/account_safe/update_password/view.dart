import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../common/style/index.dart';
import 'index.dart';

class UpdatePasswordPage extends GetView<UpdatePasswordController> {
  const UpdatePasswordPage({Key? key}) : super(key: key);
 

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UpdatePasswordController>(
      init: UpdatePasswordController(),
      id: "update_password",
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text("account_safe_login_pwd_update".tr),
            actions: [
              TextButton(
                  onPressed: () => controller.updatePwd(),
                  child: Text('save'.tr, style: AppTheme.whitePrimaryTextStyle))
            ],
          ),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).colorScheme.background,
                child: Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: ListView(
                      children: [
                        TextField(
                          maxLines: 1,
                          obscureText: true,
                          style: Theme.of(context).textTheme.bodyMedium,
                          autofocus: true,
                          controller: controller.oldPwdController,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                              labelText: 'account_safe_old_pwd'.tr),
                        ),
                        const SizedBox(height: 10),
                        TextField(
                          maxLines: 1,
                          obscureText: true,
                          style: Theme.of(context).textTheme.bodyMedium,
                          autofocus: true,
                          controller: controller.newPwdController,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                              labelText: 'account_safe_new_pwd'.tr),
                        ),
                        const SizedBox(height: 10),
                        TextField(
                          maxLines: 1,
                          obscureText: true,
                          style: Theme.of(context).textTheme.bodyMedium,
                          autofocus: true,
                          controller: controller.repeatNewPwdController,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                              labelText: 'account_safe_new_pwd_repeat'.tr),
                        ),
                      ],
                    ))),
          ),
        );
      },
    );
  }
}
