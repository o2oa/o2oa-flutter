import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';

import '../../../../common/api/x_organization_assemble_control.dart';
import '../../../../common/api/x_organization_assemble_personal.dart';
import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/services/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class SettingsCommonController extends GetxController {
  SettingsCommonController();

  final state = SettingsCommonState();
 

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    loadCacheSize();
    state.languageName = LanguageListExt.getCurrentLanguage().getDisplayName();
    super.onReady();
  }
 
  void openLogs() {
    Get.toNamed(O2OARoutes.homeSettingsLogs);
  }

  /// 
  /// 清除缓存
  /// 
  void clickClearCache() {
    O2UI.showConfirm(Get.context, 'settings_clear_cache_clear_confirm_msg'.trArgs([(state.cacheSize)]) , okPressed: () {
      _clearCache();
    });
  }

  void _clearCache() async {
    await O2FilePathUtil.clearCache();
    await O2FlutterMethodChannelUtils().clearCacheNative();
    OrganizationControlService.to.clearCache();
    Loading.toast('settings_clear_cache_clear_success'.tr);
    loadCacheSize();
  }

  ///
  /// 获取缓存目录大小
  /// 
  Future<void> loadCacheSize() async {
    int size = await O2FilePathUtil.cacheSizeTotal();
    state.cacheSize = size.friendlyFileLength();
  }
 
  // 切换语言
  changeLanguage() {
    final context = Get.context;
    if (context == null) {
      return ;
    }
     O2UI.showBottomSheetWithCancel(context, LanguageList.values.map((e) => ListTile(
      title: Text(e.getDisplayName()),
      onTap: () {
         Navigator.pop(context);
         updateLanguage(e);
      },
     )).toList());
  }
  updateLanguage(LanguageList lan) async {
    Get.updateLocale(lan.getLocale());
    SharedPreferenceService.to.putString(SharedPreferenceService.languageSaveKey, lan.getKey());
    state.languageName = lan.getDisplayName();
    _updateLanguageRemote(lan);
  }

  Future<void> _updateLanguageRemote(LanguageList lan) async {
    var user = O2ApiManager.instance.o2User;
    if (user == null) {
      return;
    }
    final key = LanguageListExt.getPcKeyFromLanguage(lan);
    user.language = key;
    final s = await OrganizationPersonalService.to.personEdit(user);
    OLogger.d('更新结果：${s?.id}');
  }
}
