import 'package:get/get.dart';

import 'controller.dart';

class SettingsCommonBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SettingsCommonController>(() => SettingsCommonController())];
  }
}
