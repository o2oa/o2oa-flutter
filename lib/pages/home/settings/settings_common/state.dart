import 'package:get/get.dart';

class SettingsCommonState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;



  final _cacheSize = "0".obs;
  set cacheSize(String value) => _cacheSize.value = value;
  String get cacheSize => _cacheSize.value;

  final _languageName = "".obs;
  set languageName(String value) => _languageName.value = value;
  String get languageName => _languageName.value;
}
