import 'package:get/get.dart';

import 'controller.dart';

class SettingsBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SettingsController>(() => SettingsController())];
  }
}
