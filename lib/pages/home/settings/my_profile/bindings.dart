import 'package:get/get.dart';

import 'controller.dart';

class MyProfileBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<MyProfileController>(() => MyProfileController())];
  }
}
