
import 'package:get/get.dart';

import '../../../common/utils/index.dart';

class SettingsState {
 

  final _personName = "".obs;
  set personName(value) => _personName.value = value;
  String get personName => _personName.value;

  final _avatarUrl = "".obs;
  set avatarUrl(value) => _avatarUrl.value = value;
  String get avatarUrl => _avatarUrl.value;

  final _personSign = "".obs;
  set personSign(value) => _personSign.value = value;
  String get personSign => _personSign.value;
 

  /// 刷新头像
  final _dn = (O2ApiManager.instance.o2User?.distinguishedName ?? '').obs;
  set dn(String value) => _dn.value = value;
  String get dn => _dn.value;

   final _cacheSize = "0".obs;
  set cacheSize(value) => _cacheSize.value = value;
  String get cacheSize => _cacheSize.value;
}
