import 'package:get/get.dart';

import 'controller.dart';

class LogListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<LogListController>(() => LogListController())];
  }
}
