import 'package:get/get.dart';

import 'controller.dart';

class AboutBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<AboutController>(() => AboutController())];
  }
}
