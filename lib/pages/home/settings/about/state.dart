
import 'package:get/get.dart';

class AboutState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;
 

  final _versionName = "".obs;
  set versionName(value) => _versionName.value = value;
  get versionName => _versionName.value;

  final _isInner = false.obs;
  set isInner(bool value) => _isInner.value = value;
  bool get isInner => _isInner.value;

  final _isAndroid = false.obs;
  set isAndroid(bool value) => _isAndroid.value = value;
  bool get isAndroid => _isAndroid.value;

  final _isOpenUpdate = true.obs;
  set isOpenUpdate(bool value) => _isOpenUpdate.value = value;
  bool get isOpenUpdate => _isOpenUpdate.value;
}
