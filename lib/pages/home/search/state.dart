import 'package:get/get.dart';

import '../../../common/models/index.dart';

class SearchState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  /// 搜索关键字
  final _searchKey = "".obs;
  set searchKey(String value) => _searchKey.value = value;
  String get searchKey => _searchKey.value;

  // 历史搜索关键字
  RxList<String> historyList = <String>[].obs;
  

   // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;


  // 搜索结果列表
  final RxList<O2SearchV2Entry> resultList = <O2SearchV2Entry>[].obs; 
}
