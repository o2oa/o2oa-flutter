import 'package:get/get.dart';

import 'controller.dart';

class SearchBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SearchController>(() => SearchController())];
  }
}
