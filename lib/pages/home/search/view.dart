import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart' as search;
import 'widgets/widgets.dart';

class SearchPage extends GetView<search.SearchController> {
  const SearchPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return Obx(() => controller.state.searchKey.isEmpty
        ? const SearchHistory()
        : const SearchListView());
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<search.SearchController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
              title: Container(
            height: 36,
            decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: const BorderRadius.all( Radius.circular(18))),
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  SizedBox(
                    width: 22,
                    height: 22,
                    child: Icon(Icons.search,
                        color: Theme.of(context).colorScheme.primary),
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: TextField(
                        autofocus: true,
                        focusNode: controller.searchNode,
                        controller: controller.searchController,
                        decoration: InputDecoration(
                          isDense: true,
                          border: InputBorder.none,
                          hintText: 'home_index_search_placeholder'.tr,
                          hintStyle: Theme.of(context).textTheme.bodySmall,
                        ),
                        style: Theme.of(context).textTheme.bodyMedium,
                        textInputAction: TextInputAction.search,
                        onSubmitted: controller.onSearch,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
          body: SafeArea(
            child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child:  _buildView(),
            )
          ),
        );
      },
    );
  }
}
