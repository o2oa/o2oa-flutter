import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/widgets/index.dart';
import '../controller.dart' as search;

class SearchHistory extends GetView<search.SearchController> {
  const SearchHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 10, left: 14, right: 14),
        child: Column(
          children: [
            // 删除按钮
            InkWell(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: TextButton.icon(
                  icon: Icon(Icons.delete,
                      size: 24, color: Theme.of(context).hintColor),
                  label: Text('search_delete_all_history'.tr,
                      style: TextStyle(color: Theme.of(context).hintColor)),
                  onPressed: controller.cleanHistory,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: historyGridView(context),
            )
          ],
        ));
  }

  Widget historyGridView(BuildContext context) {
    return Obx(() => Wrap(
          spacing: 8.0, // 水平间距
          runSpacing: 4.0, // 垂直间距
          children: controller.state.historyList
              .map((tag) => InkWell(
                  onTap: () => controller.onSearch(tag),
                  child: EllipticalText(tag,
                      backgroundColor: Theme.of(context).highlightColor)))
              .toList(),
        ));
  }
}
