import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../index.dart' as search;

/// hello
class HelloWidget extends GetView<search.SearchController> {
  const HelloWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Obx(() => Text(controller.state.title)),
    );
  }
}
