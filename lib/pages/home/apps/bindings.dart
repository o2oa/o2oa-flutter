import 'package:get/get.dart';

import 'controller.dart';

class AppsBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<AppsController>(() => AppsController())];
  }
}
