import 'package:get/get.dart';

import 'controller.dart';

class GroupMembersBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<GroupMembersController>(() => GroupMembersController())];
  }
}
