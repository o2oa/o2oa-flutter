import 'package:get/get.dart';

import 'controller.dart';

class ImBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ImController>(() => ImController())];
  }
}
