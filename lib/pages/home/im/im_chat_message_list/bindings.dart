import 'package:get/get.dart';

import 'controller.dart';

class ImChatMessageListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ImChatMessageListController>(() => ImChatMessageListController())];
  }
}
