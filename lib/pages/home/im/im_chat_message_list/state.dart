import 'package:get/get.dart';

import '../../../../common/models/im/im_message.dart';

class ImChatMessageListState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

    // 消息列表
  final RxList<IMMessage> msgList = <IMMessage>[].obs;
}
