import 'dart:typed_data';

import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/values/index.dart';

class ImChatState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 消息列表
  final RxList<IMMessage> msgList = <IMMessage>[].obs;
  // 选中的消息列表
  final RxList<IMMessage> selectedMsgList = <IMMessage>[].obs;

  /// 是否选择模式
  final _isSelectMode = false.obs;
  set isSelectMode(bool value) => _isSelectMode.value = value;
  bool get isSelectMode => _isSelectMode.value;

  /// 引用消息
  final Rx<IMMessage?> quoteMessage = Rx<IMMessage?>(null);

  // 是否正在播放语音
  final _playingVoiceId = "".obs;
  set playingVoiceId(String value) => _playingVoiceId.value = value;
  String get playingVoiceId => _playingVoiceId.value;
  

  /// 底部工具栏显示类型 0:不显示 1:语音 2:表情 3:更多工具
  final _toolBoxType = 0.obs;
  set toolBoxType(int type) => _toolBoxType.value = type;
  int get toolBoxType => _toolBoxType.value;

  /// 输入法输入状态下，如果有内容就显示发送按钮 否则显示更多按钮
  final _showMoreTools = true.obs;
  set showMoreTools(bool value) => _showMoreTools.value = value;
  bool get showMoreTools => _showMoreTools.value;

  /// 流程图标缓存
  final RxMap<String, Uint8List> processAppIconMap =  <String, Uint8List>{}.obs;

  /// 是否有更多数据
  final _hasMore = true.obs;
  set hasMore(bool value) => _hasMore.value = value;
  bool get hasMore => _hasMore.value;

  /// 顶部菜单
  final RxList<IMChatMenu> menuList = <IMChatMenu>[IMChatMenu('im_chat_action_menu_report'.tr, IMChatMenuType.report)].obs;

  /// 表情列表
  final RxList<String> emojiTextList = <String>[].obs;

  final Rx<EmojiType> emojiType = Rx(EmojiType.smileOrPerson);
  
  final Rx<IMConfigData?> imConfig = Rx<IMConfigData?>(null);

}
