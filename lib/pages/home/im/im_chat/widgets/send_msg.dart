
import 'package:flutter/material.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/widgets/index.dart';
import 'msg_body.dart';

class SendMessageWidget extends StatelessWidget {
  final IMMessageBody msgBody;
  final String msgId;
  const SendMessageWidget({
    Key? key,
    required this.msgBody,
    required this.msgId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final messageTextGroup = Flexible(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: Container(
                padding: const EdgeInsets.all(14),
                decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 0,127,255),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(18),
                    bottomLeft: Radius.circular(18),
                    bottomRight: Radius.circular(18),
                  ),
                ),
                child: MsgBodyWidget(msgBody: msgBody, isSender: true, msgId: msgId),
              ),
            ),
            CustomPaint(painter: ImBubbleShape(const Color.fromARGB(255, 0,127,255))),
          ],
        ));

    return Padding(
      padding: const EdgeInsets.only(right: 10.0, left: 0, top: 15, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          const SizedBox(height: 30),
          messageTextGroup,
        ],
      ),
    );
  }
}