import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import '../controller.dart';

///
/// 表情列表
///
class EmojiListWidget extends GetView<ImChatController> {
  const EmojiListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: GridView.count(
          shrinkWrap: true,
          crossAxisCount: 10,
          mainAxisSpacing: 5.0, //设置上下之间的间距
          crossAxisSpacing: 2.0, //设置左右之间的间距
          childAspectRatio: 1, //设置宽高比
          children: O2Emoji.emojiList.keys.map((element) {
            return InkWell(
              onTap: () => controller.onSendEmoji(element),
              child: Image.asset(O2Emoji.getEmojiPath(element)),
            );
          }).toList()),
    );
  }
}

///
/// 文本表情列表
///
class EmojiTextListWidget extends GetView<ImChatController> {
  const EmojiTextListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
      child: Obx(() => Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 5),
              emojiTypeListView(context),
              const SizedBox(height: 5),
              const Divider(height: 1),
              const SizedBox(height: 5),
              Expanded(
                  flex: 1,
                  child: GridView.count(
                      shrinkWrap: true,
                      crossAxisCount: 8,
                      mainAxisSpacing: 2.0, //设置上下之间的间距
                      crossAxisSpacing: 2.0, //设置左右之间的间距
                      childAspectRatio: 1, //设置宽高比
                      children: controller.state.emojiTextList.map((element) {
                        return InkWell(
                          onTap: () => controller.onSendEmojiText(element),
                          child:
                              Text(element, style: const TextStyle(fontSize: 24)),
                        );
                      }).toList()))
            ],
          )),
    );
  }

  Widget emojiTypeListView(
    BuildContext context,
  ) {
    return Obx(() => Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.smileOrPerson),
                  "smile",
                  controller.state.emojiType.value == EmojiType.smileOrPerson),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.animals),
                  "animals",
                  controller.state.emojiType.value == EmojiType.animals),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.food),
                  "food",
                  controller.state.emojiType.value == EmojiType.food),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.activities),
                  "activities",
                  controller.state.emojiType.value == EmojiType.activities),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.travel),
                  "travel",
                  controller.state.emojiType.value == EmojiType.travel),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.objects),
                  "objects",
                  controller.state.emojiType.value == EmojiType.objects),
              _emojiTypeImageButton(
                  context,
                  () => controller.clickEmojiTypeBtn(EmojiType.symbol),
                  "symbol",
                  controller.state.emojiType.value == EmojiType.symbol),
            ]));
  }

  Widget _emojiTypeButton(BuildContext context, VoidCallback onPressed,
      IconData icon, bool selected) {
    return Container(
        decoration: selected
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              )
            : null,
        child: IconButton(
          onPressed: onPressed,
          icon: Icon(icon, size: 32),
          // color: selected ? Colors.white : null,
        ));
  }

  Widget _emojiTypeImageButton(BuildContext context, VoidCallback onPressed,
    String emojiType, bool selected) {
  return Container(
      decoration: selected
          ? BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            )
          : null,
      child: IconButton(
        onPressed: onPressed,
        icon:  AssetsImageView("emoji_type_$emojiType.png", width: 32, height: 32),
        // color: selected ? Colors.white : null,
      ));
}
}
