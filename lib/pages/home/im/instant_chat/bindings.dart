import 'package:get/get.dart';

import 'controller.dart';

class InstantChatBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<InstantChatController>(() => InstantChatController())];
  }
}
