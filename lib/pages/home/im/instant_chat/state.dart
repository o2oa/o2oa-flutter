import 'dart:typed_data';

import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class InstantChatState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 消息列表
  final RxList<InstantMsg> instantMsgList = <InstantMsg>[].obs;


  /// 流程图标缓存
  final RxMap<String, Uint8List> processAppIconMap =  <String, Uint8List>{}.obs;

}
