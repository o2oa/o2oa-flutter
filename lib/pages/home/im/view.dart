import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/date_extension.dart';

import '../../../common/models/im/im_instant_message.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/conversation_list_view.dart';

class ImPage extends GetView<ImController> {
  const ImPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Obx(() => RefreshIndicator(
            onRefresh: () => controller.loadConversationList(),
            child: Column(
              children: [
                Visibility(
                    visible: controller.state.showInstantMsg,
                    child: lastInstantMsgItemView(context)),
                Visibility(
                    visible: controller.state.showInstantMsg,
                    child: const SizedBox(height: 10)),
                Expanded(
                    flex: 1,
                    child: ConversationListView(
                      controller.state.conversationList.toList(),
                      onTapConversation: (info) =>
                          controller.tapOpenConversation(info),
                    ))
              ],
            ))));
  }

  ///
  /// 系统消息
  ///
  Widget lastInstantMsgItemView(BuildContext context) {
    InstantMsg? msg = controller.instantMsg;
    String lastTime = '';
    String subTitle = '';
    if (msg?.createTime != null) {
      DateTime? time = DateTime.tryParse(msg?.createTime ?? "");
      if (time != null) {
        lastTime = time.friendlyTime();
      }
      subTitle = msg?.title ?? '';
    }
    return Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Theme.of(context).colorScheme.background,
        ),
        child: ListTile(
          leading: const SizedBox(
            width: 50,
            height: 50,
            child: AssetsImageView("icon_msg.png"),
          ),
          title: Text(
            'im_instant_msg_title'.tr,
            softWrap: true,
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          subtitle: Text(
            subTitle,
            softWrap: true,
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
          trailing:
              Text(lastTime, style: Theme.of(context).textTheme.bodySmall),
          onTap: () => controller.clickInstant(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('home_tab_im'.tr),
            actions: [menu()],
          ),
          body: SafeArea(
              child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: _buildView(context),
          )),
          floatingActionButton: Obx(() => Visibility(
                visible: controller.state.showSpeechAssistant,
                child: FloatingActionButton(
                    onPressed: () => controller.startSpeechAssistant(),
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    child: const AssetsImageView('icon_zhinengyuyin.png',
                        width: 48, height: 48)
                    // const Icon(
                    //   Icons.record_voice_over,
                    //   color: Colors.white,
                    //   size: 36,
                    // )
                    ),
              )),
        );
      },
    );
  }

  Widget menu() {
    return Obx(() {
      final list = controller.state.menuList.toList();
      return PopupMenuButton<int>(
        icon: const Icon(Icons.add_circle_outline, color: Colors.white),
        itemBuilder: (context) => list,
        onSelected: (value) {
          switch (value) {
            case 0:
              controller.startSingleChat();
              break;
            case 1:
              controller.startGroupChat();
              break;
            case 3:
              controller.tap2Collection();
              break;
            default:
              break;
          }
        },
      );
    });
  }
}
