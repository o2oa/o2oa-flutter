import 'package:get/get.dart';

import 'controller.dart';

class ImMessageCollectionBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ImMessageCollectionController>(() => ImMessageCollectionController())];
  }
}
