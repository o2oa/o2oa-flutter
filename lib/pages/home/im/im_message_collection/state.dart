import 'package:get/get.dart';

import '../../../../common/models/im/im_message.dart';

class ImMessageCollectionState {
  // 消息列表
  final RxList<IMMessageCollection> msgList = <IMMessageCollection>[].obs;

  // 消息列表
  final RxList<IMMessageCollection> selectedList = <IMMessageCollection>[].obs;


  // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;

  // 选择模式
  final _selectMode = false.obs;
  set selectMode(bool value) => _selectMode.value = value;
  bool get selectMode => _selectMode.value;
}
