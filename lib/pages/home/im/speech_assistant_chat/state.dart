import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class SpeechAssistantChatState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _btnTitle = "".obs;
  set btnTitle(String value)=> _btnTitle.value = value;
  String get btnTitle => _btnTitle.value;

  // 当前状态
  Rx<SpeechStatus> status = Rx<SpeechStatus>(SpeechStatus.idle);

  // 随机展示命令列表
  final RxList<String> initCommandList = <String>[].obs;

  // 对话列表
  final RxList<ImSpeechAssistantResponse> chatList = <ImSpeechAssistantResponse>[].obs;

  
}
