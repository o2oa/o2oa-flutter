import 'package:get/get.dart';

import 'controller.dart';

class SpeechAssistantChatBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SpeechAssistantChatController>(() => SpeechAssistantChatController())];
  }
}
