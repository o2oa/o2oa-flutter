import 'package:get/get.dart';

import '../../../../common/models/im/im_conversation.dart';
import '../../../../common/models/orgs/o2_person.dart';

class ConversationPickerState {
  final RxList<IMConversationInfo> conversationList =
      <IMConversationInfo>[].obs;

  // 搜索结果是否展现
  final _showSearchResult = false.obs;
  set showSearchResult(bool value) => _showSearchResult.value = value;
  bool get showSearchResult => _showSearchResult.value;

  final RxList<IMConversationInfo> searchConversationList =
      <IMConversationInfo>[].obs;

  // 人员列表
  final RxList<O2Person> searchPersonList = <O2Person>[].obs;
}
