import 'package:get/get.dart';

import 'controller.dart';

class ConversationPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ConversationPickerController>(() => ConversationPickerController())];
  }
}
