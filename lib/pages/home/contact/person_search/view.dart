import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/widgets/index.dart';
import 'index.dart';

class PersonSearchPage extends GetView<PersonSearchController> {
  const PersonSearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonSearchController>(
      builder: (_) {
        return O2UI.basePopScope(
            child: Scaffold(
            appBar: AppBar(
                title: Container(
              height: 36,
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: const BorderRadius.all(Radius.circular(18))),
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  children: [
                    SizedBox(
                      width: 22,
                      height: 22,
                      child: Icon(Icons.search,
                          color: Theme.of(context).colorScheme.primary),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: TextField(
                          autofocus: true,
                          focusNode: controller.searchNode,
                          controller: controller.searchController,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            hintText: 'contact_person_search_placeholder'.tr,
                            hintStyle: Theme.of(context).textTheme.bodySmall,
                          ),
                          style: Theme.of(context).textTheme.bodyMedium,
                          textInputAction: TextInputAction.search,
                          onSubmitted: controller.onSearch,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )),
            body: SafeArea(
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Obx(
                      () => controller.state.personList.isNotEmpty
                          ? Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  color:
                                      Theme.of(context).colorScheme.background),
                              child: ListView(
                                children:
                                    controller.state.personList.map((person) {
                                  return ListTile(
                                    onTap: () =>
                                        controller.clickEnterPerson(person),
                                    leading: SizedBox(
                                      width: 50,
                                      height: 50,
                                      child: O2UI.personAvatar(
                                          person.distinguishedName!, 25),
                                    ),
                                    title: Text(person.name ?? ''),
                                    trailing: O2UI.rightArrow(),
                                  );
                                }).toList(),
                              ))
                          : Center(
                              child: Text('empty_data'.tr,
                                  style:
                                      Theme.of(context).textTheme.bodySmall)),
                    )))));
      },
    );
  }
}
