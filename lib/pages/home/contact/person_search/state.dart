import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class PersonSearchState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 人员列表
  final RxList<O2Person> personList = <O2Person>[].obs;
}
