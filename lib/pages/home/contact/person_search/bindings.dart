import 'package:get/get.dart';

import 'controller.dart';

class PersonSearchBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<PersonSearchController>(() => PersonSearchController())];
  }
}
