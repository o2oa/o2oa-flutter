import 'package:get/get.dart';

import 'controller.dart';

class ContactBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ContactController>(() => ContactController())];
  }
}
