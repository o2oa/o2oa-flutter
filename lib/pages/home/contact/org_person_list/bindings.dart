import 'package:get/get.dart';

import 'controller.dart';

class OrgPersonListBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<OrgPersonListController>(() => OrgPersonListController())];
  }
}
