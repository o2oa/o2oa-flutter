import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class OrgPersonListPage extends GetView<OrgPersonListController> {
  const OrgPersonListPage({Key? key}) : super(key: key);

  static open({O2Unit? top}) {
    Get.toNamed(O2OARoutes.homeContactList, arguments: top);
  }


  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrgPersonListController>(
      builder: (_) {
        return O2UI.basePopScope(
            child: Scaffold(
          appBar: AppBar(title:  Text('home_tab_contact'.tr)),
          body: SafeArea(
            child: Padding(
            padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: ListView(
              children: [
                breadcrumbView(context),
                const SizedBox(height: 10),
                contactListView(context)
              ],
            )),
          ),
        ));
      },
    );
  }


  Widget contactListView(BuildContext context) {
    return Column(
      children: [
        Obx(() => listBoxView(context, Column(
              children: controller.state.orgList.map((element) {
                var orgName = element.name ?? '';
                var oneName = orgName;
                if (orgName.length > 1) {
                  oneName = oneName.substring(0, 1);
                }
                return ListTile(
                  leading: SizedBox(
                    width: 32,
                    height: 32,
                    child: CircleAvatar(
                      radius: 32,
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      child: Text(oneName,
                          style: const TextStyle(color: Colors.white)),
                    ),
                  ),
                  title: Text(orgName),
                  trailing: O2UI.rightArrow(),
                  onTap: () => controller.clickEnterOrg(element),
                );
              }).toList(),
            ))),
        const SizedBox(height: 10),
        Obx(() => listBoxView(context, Column(
              children: controller.state.personList.map((element) {
                return  ListTile(
                  onTap: () => controller.clickEnterPerson(element),
                  leading: SizedBox(
                width: 50,
                height: 50,
                child: O2UI.personAvatar(element.distinguishedName!, 25),
              ),
                  title: Text(element.name ?? ''),
                  trailing: O2UI.rightArrow(),
                );
              }).toList(),
            )))
      ],
    );
  }

  Widget listBoxView(BuildContext context, Widget body) {
    return Container(
        width: double.infinity,
        decoration:  BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.background),
        child: body);
  }

  Widget breadcrumbView(BuildContext context) {
    return Container(
      height: 48,
      width: double.infinity,
      decoration:  BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Theme.of(context).colorScheme.background),
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(
                children: controller.state.breadcrumbBeans.map((element) {
              var isLast = (element.key == controller.state.breadcrumbBeans.last.key);
              if (isLast) {
                return TextButton(
                    onPressed: () {},
                    child: Text(element.name,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(color: Theme.of(context).colorScheme.primary)
                             ));
              }
              return Row(children: [
                TextButton(
                    onPressed: () => controller.clickBreadcrumb(element),
                    child: Text(element.name,
                        style: Theme.of(context).textTheme.bodyLarge)),
                Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(">",
                        style: Theme.of(context).textTheme.bodyLarge))
              ]);
            }).toList()),
          )),
    );
  }

}
