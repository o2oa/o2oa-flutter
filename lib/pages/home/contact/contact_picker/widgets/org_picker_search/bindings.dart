import 'package:get/get.dart';

import 'controller.dart';

class OrgPickerSearchBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<OrgPickerSearchController>(() => OrgPickerSearchController())];
  }
}
