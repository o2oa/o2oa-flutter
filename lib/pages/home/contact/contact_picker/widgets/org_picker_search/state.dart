import 'package:get/get.dart';

import '../../../../../../common/models/orgs/index.dart';

class OrgPickerSearchState {
 
 // 人员列表
  final RxList<O2Unit> unitList = <O2Unit>[].obs;

  /// 已经选中的身份列表
  final RxList<O2Unit> unitPickedList = <O2Unit>[].obs;
}
