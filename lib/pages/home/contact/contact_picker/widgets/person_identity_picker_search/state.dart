import 'package:get/get.dart';

import '../../../../../../common/models/orgs/index.dart';

class PersonIdentityPickerSearchState {


  // 人员列表
  final RxList<O2Identity> personList = <O2Identity>[].obs;

  /// 已经选中的身份列表
  final RxList<O2Identity> identityPickedList = <O2Identity>[].obs;
}
