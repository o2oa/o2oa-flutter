import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../common/models/orgs/index.dart';
import '../../../../../../common/routers/index.dart';
import '../../../../../../common/style/index.dart';
import '../../../../../../common/widgets/index.dart';
import 'index.dart';

class PersonIdentityPickerSearchPage
    extends GetView<PersonIdentityPickerSearchController> {
  const PersonIdentityPickerSearchPage({Key? key}) : super(key: key);

  static Future<dynamic> openSearch(
      ContactPickerArguments args, List<O2Identity> identityPickedList) async {
    return await Get.toNamed(O2OARoutes.homeContactPickerSearchPerson,
        arguments: {
          'arguments': args,
          'identityPickedList': identityPickedList
        });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonIdentityPickerSearchController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Container(
                          height: 36,
                          decoration: BoxDecoration(
                              color: Theme.of(context).scaffoldBackgroundColor,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(18))),
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 22,
                                  height: 22,
                                  child: Icon(Icons.search,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 5),
                                    child: TextField(
                                      autofocus: true,
                                      focusNode: controller.searchNode,
                                      controller: controller.searchController,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        hintText: 'contact_picker_search'.tr,
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .bodySmall,
                                      ),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                      textInputAction: TextInputAction.search,
                                      onChanged: (value) => controller.onEditingComplete(),
                                      onSubmitted: (value) => controller.onSubmit(value),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),
                    TextButton(
                        onPressed: () => controller.clickCancel(),
                        child: Text('cancel'.tr,
                            style: AppTheme.whitePrimaryTextStyle))
                  ],
                )),
            body: SafeArea(
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Obx(
                      () => controller.state.personList.isNotEmpty
                          ? ListView(
              children: [
                              Container(
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      color: Theme.of(context)
                                          .colorScheme
                                          .background),
                                  child: Column(
                                    children: controller.state.personList
                                        .map((element) {
                                      return CheckboxListTile(
                                        secondary: SizedBox(
                                          width: 32,
                                          height: 32,
                                          child: O2UI.personAvatar(
                                              element.person!, 16),
                                        ),
                                        title: Text(element.name ?? ''),
                                        value:
                                            controller.isItemChecked(element),
                                        onChanged: (v) => controller
                                            .onItemCheckedChange(element, v),
                                      );
                                    }).toList(),
                                  ))
                            ])
                          : Center(
                              child: Text('empty_data'.tr,
                                  style:
                                      Theme.of(context).textTheme.bodySmall)),
                    ))));
      },
    );
  }
}
