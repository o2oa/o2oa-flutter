import 'package:get/get.dart';

import 'controller.dart';

class PersonIdentityPickerSearchBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<PersonIdentityPickerSearchController>(() => PersonIdentityPickerSearchController())];
  }
}
