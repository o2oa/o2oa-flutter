import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../common/api/index.dart';
import '../../../../../../common/models/orgs/index.dart';
import '../../../../../../common/utils/index.dart';
import 'index.dart';

class PersonIdentityPickerSearchController extends GetxController {
  PersonIdentityPickerSearchController();

  final state = PersonIdentityPickerSearchState();

  final TextEditingController searchController = TextEditingController();

  final FocusNode searchNode = FocusNode();

  ContactPickerArguments? pickerArgs;

  @override
  void onReady() {
    final args = Get.arguments;
    if (args != null ) {
      pickerArgs = args['arguments'];
      final identityList = args['identityPickedList'];
      state.identityPickedList.clear();
      if (identityList != null && identityList.isNotEmpty) {
        state.identityPickedList.addAll(identityList);
      }
    }
     super.onReady();
  }


  
  void onEditingComplete() {
    final key = searchController.text.trim();
    OLogger.d("搜索关键字 ：$key  onEditingComplete");
    onSearch(key);
  }
  void onSubmit(String v) {
    final key = searchController.text.trim();
    OLogger.d("搜索关键字 ：$key  onSubmit");
    onSearch(key);
  }

  /// 搜索
  Future<void> onSearch(String key) async {
    OLogger.d("搜索关键字：$key");
    if (key.isNotEmpty) {
      var list = await OrganizationControlService.to.personSearch(key);
      state.personList.clear();
      if (list != null && list.isNotEmpty) {
        List<String> persons = [];
        for (var element in list) {
          persons.add(element.distinguishedName!);
        }
        final identityList = await OrganizationAssembleExpressService.to
            .listIdentityWithPersons(persons);
        if (identityList != null && identityList.isNotEmpty) {
          state.personList.addAll(identityList);
        }
      }
    } else {
      state.personList.clear();
    }
  }

  bool isItemChecked(O2Identity o2identity) {
    final isCheced = state.identityPickedList.firstWhereOrNull(
        (element) => element.distinguishedName == o2identity.distinguishedName);
    return isCheced != null;
  }

  void onItemCheckedChange(O2Identity o2identity, bool? checked) {
    OLogger.d(
        'checked change id：${o2identity.distinguishedName} checked: $checked');
    if (checked == true) {
      if (pickerArgs?.multiple == true) {
        if ((pickerArgs?.maxNumber ?? 0) > 0 &&
            state.identityPickedList.length >= (pickerArgs?.maxNumber)! ) {
          Loading.toast('contact_picker_msg_more_than_max'.tr);
          return;
        }
      } else {
        if (state.identityPickedList.isNotEmpty) {
          Loading.toast('contact_picker_msg_more_than_max'.tr);
          return;
        }
      }
      state.identityPickedList.add(o2identity);
    } else {
      state.identityPickedList.remove(o2identity);
    }
  }

  void clickCancel() {
    Get.back(result: {
      'identityList': state.identityPickedList,
    });
  }
}
