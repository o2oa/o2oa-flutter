import 'package:get/get.dart';

import 'controller.dart';

class ContactPickerBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ContactPickerController>(() => ContactPickerController())];
  }
}
