import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/style/index.dart';
import '../index.dart';

/// 首页顶部的工具栏
class TopBarWidget extends GetView<DefaultIndexController> {
  const TopBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 56,
        decoration: BoxDecoration(color: Theme.of(context).primaryColor),
        child: Row(
          children: [
            IconButton(
              onPressed: controller.clickScan,
              icon: const Icon(Icons.qr_code_scanner),
              iconSize: 28,
              color: Theme.of(context).appBarTheme.iconTheme?.color ??
                  Colors.white,
            ),
            Expanded(
                flex: 1,
                child: GestureDetector(
                    onTap: controller.clickSearch,
                    child: Container(
                        height: 36,
                        decoration: BoxDecoration(
                            color: Theme.of(context).scaffoldBackgroundColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(18))),
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 22,
                                height: 22,
                                child: Icon(O2IconFont.search,
                                    color: Theme.of(context).colorScheme.primary),
                              ),
                              Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    "home_index_search_placeholder".tr,
                                    style: Theme.of(context).textTheme.bodySmall,
                                  ))
                            ],
                          ),
                        )))),
            IconButton(
              onPressed: controller.clickAddWork,
              icon: const Icon(Icons.add),
              iconSize: 36,
              color: Theme.of(context).appBarTheme.iconTheme?.color ??
                  Colors.white,
            ),
          ],
        ));
  }
}
