import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import 'index.dart';

class ScanLoginController extends GetxController {
  ScanLoginController();

  final state = ScanLoginState();

  String? meta; // 扫码登录凭证

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    final map = Get.arguments;
    if (map != null) {
      meta = map['meta'];
    }
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  void clickLoginPc() async {
    if (meta == null || meta?.isEmpty == true) {
      Loading.toast('args_error'.tr);
      return;
    }
    OLogger.d('扫码登录meta：$meta');
    final p = await OrgAuthenticationService.to.login2Pc(meta!);
    if (p != null) {
      alert();
    }
  }

  void alert() {
    O2UI.showAlert(Get.context, 'scan_login_to_pc_alert_msg'.tr,
        okPressed: (() => Get.back()));
  }
}
