import 'package:get/get.dart';

import 'controller.dart';

class ScanLoginBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<ScanLoginController>(() => ScanLoginController())];
  }
}
