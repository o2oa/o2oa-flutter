import 'package:get/get.dart';

import 'controller.dart';

class DefaultIndexBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<DefaultIndexController>(() => DefaultIndexController())];
  }
}
