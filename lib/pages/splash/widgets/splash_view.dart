import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/api/index.dart';
import '../../../common/style/index.dart';
import '../../../common/widgets/assets_image_view.dart';
import '../index.dart';

/// splash view
class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(
          child: AssetsImageView(
            'launch_background.png',
            fit: BoxFit.cover,
          ),
        ),
        Positioned.fill(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 100),
            Text(
              'appName'.tr,
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(color: AppColor.primaryText),
            ),
            const SizedBox(height: 24),
            Obx(() => controller.state.showLaunchLogo
                ? ProgramCenterService.to.launchLogoImageView()
                : const SizedBox(height: 128))
          ],
        )),
        Positioned(
            right: 20,
            bottom: 64,
            child: Obx(() {
              return Text(
                '${controller.state.tips}',
                style: TextStyle(color: Theme.of(context).colorScheme.primary),
              );
            })),
        Positioned(
            bottom: 32,
            left: 0,
            right: 0,
            child: Obx(() => Center(
                    child: Text(
                  controller.state.copyright,
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: AppColor.primaryText),
                ))))
      ],
    );
  }
}
