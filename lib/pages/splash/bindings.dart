import 'package:get/get.dart';

import 'controller.dart';

class SplashBinding implements Binding {
  @override
  List<Bind> dependencies() {
    return [Bind.lazyPut<SplashController>(() => SplashController())];
  }
}
